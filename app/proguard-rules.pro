# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\sdk new\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-ignorewarnings
 -keep class * {
     public private default *;
 }

 -keep class * {
     public private *;
 }
-optimizationpasses 30
-mergeinterfacesaggressively
-dontpreverify
-optimizations !code/simplification/arithmetic

-repackageclasses 'vidstatus.com'
-allowaccessmodification
-useuniqueclassmembernames
-keeppackagenames doNotKeepAThing

-keep class !vidstatus.com.**{ *; }
-keep public class vidstatus.com.support{
    *;
}
-keep public class vidstatus.com.support.API{
    *;
}

-keep public class vidstatus.com.support.RequestHandlerUpdate4
-keepclasseswithmembernames public class com.sdk.ui.activity.ExampleActivity{
    public <methods>;
    protected <methods>;
}
-keepclasseswithmembernames public class com.example.sdk.ExampleReciever{
    public <methods>;
}

-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}
-dontwarn javax.servlet.**
-dontwarn org.joda.time.**
-dontwarn org.w3c.dom.**
-keep class butterknife.** { *; }

# keep everything in this package from being renamed only
-keepnames class butterknife.** { *; }
# keep everything in this package from being removed or renamed
-keep class retrofit2.** { *; }

# keep everything in this package from being renamed only
-keepnames class retrofit2.** { *; }
-keepattributes Signature
# keep everything in this package from being removed or renamed
-keep class okhttp3.** { *; }

# keep everything in this package from being renamed only
-keepnames class okhttp3.** { *; }
# keep everything in this package from being removed or renamed
-keep class libcore.** { *; }

# keep everything in this package from being renamed only
-keepnames class libcore.** { *; }
# keep everything in this package from being removed or renamed
-keep class dalvik.** { *; }

# keep everything in this package from being renamed only
-keepnames class dalvik.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.google.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.google.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.bumptech.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.bumptech.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.devbrackets.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.devbrackets.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.squareup.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.squareup.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.loopj.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.loopj.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.thin.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.thin.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.amitshekhar.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.amitshekhar.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.daimajia.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.daimajia.** { *; }
# keep everything in this package from being removed or renamed
-keep class android.** { *; }

# keep everything in this package from being renamed only
-keepnames class android.** { *; }
# keep everything in this package from being removed or renamed
-keep class cz.** { *; }

# keep everything in this package from being renamed only
-keepnames class cz.** { *; }
# keep everything in this package from being removed or renamed
-keep class org.** { *; }

# keep everything in this package from being renamed only
-keepnames class org.** { *; }
# keep everything in this package from being removed or renamed
-keep class org.** { *; }

# keep everything in this package from being renamed only
-keepnames class org.** { *; }
# keep everything in this package from being removed or renamed
-keep class javax.** { *; }

# keep everything in this package from being renamed only
-keepnames class javax.** { *; }
# keep everything in this package from being removed or renamed
-keep class de.** { *; }

# keep everything in this package from being renamed only
-keepnames class de.** { *; }
# keep everything in this package from being removed or renamed
-keep class dalvik.** { *; }

# keep everything in this package from being renamed only
-keepnames class dalvik.** { *; }

-dontwarn com.squareup.okhttp.**
#### -- Picasso --
 -dontwarn com.squareup.picasso.**

 #### -- OkHttp --

 -dontwarn com.squareup.okhttp.internal.**

 #### -- Apache Commons --

 -dontwarn org.apache.commons.logging.**

-libraryjars <java.home>\lib\rt.jar

-dontwarn org.apache.commons.**
-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okhttp3.**
-dontwarn okio.**

