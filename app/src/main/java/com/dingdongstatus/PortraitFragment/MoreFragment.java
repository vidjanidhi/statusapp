package com.dingdongstatus.PortraitFragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dingdongstatus.DownloadedListActivity;
import com.dingdongstatus.LoginActivity;
import com.dingdongstatus.PrivacyPolicyActivity;
import com.dingdongstatus.ProfileActivity;
import com.dingdongstatus.R;
import com.dingdongstatus.UploadVideoActivity;
import com.dingdongstatus.netutils.MyPreferences;
import com.dingdongstatus.support.AppConstant;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MoreFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.img_profile)
    de.hdodenhof.circleimageview.CircleImageView imgProfile;
    @BindView(R.id.ll_profile)
    LinearLayout llProfile;
    @BindView(R.id.ll_downloads)
    LinearLayout llDownloads;
    @BindView(R.id.ll_favourite)
    LinearLayout llFavourite;
    @BindView(R.id.ll_upload)
    LinearLayout llUpload;
    @BindView(R.id.ll_rate)
    LinearLayout llRate;
    @BindView(R.id.ll_share)
    LinearLayout llShare;
    @BindView(R.id.ll_privacy_policy)
    LinearLayout llPrivacyPolicy;
    Unbinder unbinder;
    MyPreferences myPreferences;
    @BindView(R.id.tv_profile_name)
    TextView tvProfileName;

    String ori_type = "";

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ori_type = getArguments().getString("ori_type");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        unbinder = ButterKnife.bind(this, view);

        myPreferences = new MyPreferences(getContext());
        llProfile.setOnClickListener(this);
        llDownloads.setOnClickListener(this);
        llFavourite.setOnClickListener(this);
        llUpload.setOnClickListener(this);
        llRate.setOnClickListener(this);
        llShare.setOnClickListener(this);
        llPrivacyPolicy.setOnClickListener(this);
        setUserProfile();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.ll_profile:
                if (myPreferences.getPreferences(MyPreferences.id).equals("")) {
                    intent = new Intent(getContext(), LoginActivity.class);
                    startActivityForResult(intent, 101);
                } else {
                    intent = new Intent(getContext(), ProfileActivity.class);
                    intent.putExtra("user_type", AppConstant.me);
                    intent.putExtra("ori_type", ori_type);
                    startActivity(intent);
                }
                break;

            case R.id.ll_downloads:
                intent = new Intent(getContext(), DownloadedListActivity.class);
                intent.putExtra("from", "1");
                intent.putExtra("type", "1");
                intent.putExtra("ori_type", ori_type);
                getActivity().startActivity(intent);
                break;

            case R.id.ll_favourite:
                intent = new Intent(getContext(), DownloadedListActivity.class);
                intent.putExtra("from", "2");
                intent.putExtra("type", "2");
                intent.putExtra("ori_type", ori_type);
                getActivity().startActivity(intent);
                break;

            case R.id.ll_upload:
                if (myPreferences.getPreferences(MyPreferences.id).equals("")) {
                    intent = new Intent(getContext(), LoginActivity.class);
                    intent.putExtra("ori_type", ori_type);
                    startActivityForResult(intent, 102);
                } else {
                    intent = new Intent(getContext(), UploadVideoActivity.class);
                    intent.putExtra("ori_type", ori_type);
                    startActivity(intent);
                }
                break;

            case R.id.ll_share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra("android.intent.extra.TEXT",
                        "Hey, I just found this awesome app on play store. " +
                                "You can download thousands of video status in this app.\n" +
                                " https://play.google.com/store/apps/details?id=" + getActivity().getPackageName());
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;

            case R.id.ll_rate:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("market://details?id=" + getActivity().getPackageName()));
                startActivity(i);
                break;

            case R.id.ll_privacy_policy:
                Intent inte = new Intent(getContext(), PrivacyPolicyActivity.class);
                startActivity(inte);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUserProfile();
    }

    public void setUserProfile() {
        if (!myPreferences.getPreferences(MyPreferences.id).equals("")) {
            if (myPreferences.getPreferences(MyPreferences.login_type).equals("1")) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                tvProfileName.setText(user.getDisplayName());
                Glide.with(getContext()).load(user.getPhotoUrl().toString())
                        .thumbnail(0.5f)
                        .crossFade()
                        .placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgProfile);

//                imgProfile.setImageURI(user.getPhotoUrl());
            } else if (myPreferences.getPreferences(MyPreferences.login_type).equals("3")) {
                tvProfileName.setText(myPreferences.getPreferences(MyPreferences.name));
                Glide.with(getContext()).load(myPreferences.getPreferences(MyPreferences.profile_image))
                        .thumbnail(0.5f)
                        .crossFade().placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgProfile);

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setUserProfile();
    }
}
