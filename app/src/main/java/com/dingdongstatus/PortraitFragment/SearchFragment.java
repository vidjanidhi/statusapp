package com.dingdongstatus.PortraitFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.dingdongstatus.R;
import com.dingdongstatus.adapter.HomeAdapter;
import com.dingdongstatus.model.Category;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.EndlessRecyclerOnScrollListenerForStrageredView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    @BindView(R.id.search_view)
    SearchView searchview;
    @BindView(R.id.recycler_view)
    RecyclerView search_recycler_view;
    Unbinder unbinder;
    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;
    @BindView(R.id.rel_screen_loder)
    RelativeLayout relScreenLoder;
    private GridLayoutManager mLayoutManager;
    private HomeAdapter videoListAdapter;
    private ArrayList<VideoModel> data = new ArrayList<>();

    private int page = 1, total_rec = 0, limit = 0;
    String search_text = "";

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        unbinder = ButterKnife.bind(this, view);
        search_recycler_view.setHasFixedSize(false);
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 1;
                } else if ((position + 1) % 7 == 0) {
                    return 2;
                } else {
                    return 1;
                }

            }
        });
        search_recycler_view.setLayoutManager(mLayoutManager);
        videoListAdapter = new HomeAdapter(getContext(), data, AppConstant.PORTRAIT);
        search_recycler_view.setAdapter(videoListAdapter);
        search_recycler_view.addOnScrollListener(new EndlessRecyclerOnScrollListenerForStrageredView(
                (GridLayoutManager) mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (total_rec > (limit * page)) {
                    page += 1;
                    getCategory();
                }
            }

            @Override
            public void onFirstVisibleItem(int pos) {
               /* if (pos >= 3) {
                    fabGoToTop.setVisibility(fabGoToTop.VISIBLE);
                } else {
                    fabGoToTop.setVisibility(fabGoToTop.GONE);
                }*/
            }
        });

        searchview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchview.onActionViewExpanded();
                searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        if (!searchview.isIconified()) {
//                            searchview.setIconified(true);
                            search_text = query;
                            clearData();
                            getCategory();
//                            videoListAdapter.notifyDataSetChanged();
                           /* Intent i = new Intent(MainActivity.this, SearchViewActivity.class);
                            i.putExtra("searchtext", query.toString());
                            i.putExtra("mode", "search");
                            startActivity(i);*/
                        }
//                        item.collapseActionView();
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        return false;
                    }
                });
                //do what you want when search view expended
            }
        });
        searchview.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                //do what you want  searchview is not expanded
                clearData();
                return false;
            }
        });
        return view;
    }

    public void clearData() {
        data.clear();
        videoListAdapter.notifyDataSetChanged();
        page = 1;
        total_rec = 0;
        limit = 0;
    }

    private void getCategory() {
        if (data.size() == 0) {
            relScreenLoder.setVisibility(View.VISIBLE);
        }
       /* RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*/
        Call<ResponseBody> call = AppConstant.request.getSearchStatus(
                "searchstatus",
                page + "",
                "" + AppConstant.PORTRAIT,
                search_text

        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                relScreenLoder.setVisibility(View.GONE);
                if (data.size() == 0) {
                    relScreenLoder.setVisibility(View.GONE);
                    clearData();
                } else {
                    data.remove(data.size() - 1);
                }
                try {
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    page = Integer.parseInt(json.getString("page"));
                    total_rec = Integer.parseInt(json.getString("total_rec"));
                    limit = Integer.parseInt(json.getString("limit"));
                    Category da = new Category();


                    if (json.getInt("success") == 1) {
                        JSONArray result = json.getJSONArray("data");
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject c = result.getJSONObject(i);
                            VideoModel modelOffers = new VideoModel();
                            modelOffers.setId("" + c.getString("id"));
                            modelOffers.setName("" + c.getString("name"));
                            modelOffers.setImgurl("" + c.getString("imgurl"));
                            modelOffers.setVideourl("" + c.getString("videourl"));
                            modelOffers.setTag("" + c.getString("tag"));
                            modelOffers.setDownloads("" + c.getString("downloads"));
                            modelOffers.setView("" + c.getString("view"));
                            modelOffers.setShare("" + c.getString("share"));
                            modelOffers.setStatus("" + c.getString("status"));
                            modelOffers.setDatetime("" + c.getString("datetime"));
                            modelOffers.setUser_id("" + c.getString("user_id"));
                            modelOffers.setUser_name("" + c.getString("user_name"));
                            modelOffers.setUser_imgurl("" + c.getString("user_imgurl"));
                            data.add(modelOffers);
                        }
                        videoListAdapter.notifyDataSetChanged();
                    } else {
//                        Toaster.show(getActivity(), "" + json.getString("ack_msg"), false, Toaster.DANGER);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                relScreenLoder.setVisibility(View.GONE);
                System.out.print("error" + t.getMessage());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
