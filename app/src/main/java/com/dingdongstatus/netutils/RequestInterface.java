package com.dingdongstatus.netutils;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RequestInterface {

    String secure_field = "key";
    String secure_value = "SrT13_appdesdrivnjhutQpQ";
    String param_key = "param_key";
    String param_value = "46419246";

    // TODO: 25-10-2018 nidhi login
    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getCategory(@Query("req") String req);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getArtist(@Query("req") String req);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getLatestStatus(@Query("req") String req,
                                       @Query("video_type") String video_type,
                                       @Query("page") String page);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getLatestStatus(@Query("req") String req,
                                       @Query("video_type") String video_type,
                                       @Query("user_id") String user_id,
                                       @Query("page") String page);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getStatusListFromCat(@Query("req") String req,
                                            @Query("category_id") String category_id,
                                            @Query("video_type") String video_type,
                                            @Query("page") String page);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getStatusListFromArtist(@Query("req") String req,
                                               @Query("artist_id") String artist_id,
                                               @Query("video_type") String video_type,
                                               @Query("page") String page);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getRelatedStatus(@Query("req") String req,
                                        @Query("id") String id,
                                        @Query("tag") String page);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getSearchStatus(@Query("req") String req,
                                       @Query("page") String page,
                                       @Query("video_type") String video_type,
                                       @Query("tag") String tag);

    @POST("ApplicationData.php?" + "reuestfor=savedata" + "&notification_app_id=1")
    Call<ResponseBody> sendToken(@Query("token") String token,
                                 @Query("deviceid") String deviceid);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getMoreApp(@Query("req") String req);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getAdvertise(@Query("req") String req);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> login(@Query("email") String email, @Query("req") String req,
                             @Query("password") String password);

    @Multipart
    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> register(@Query("email") String email,
                                @Query("name") String name,
                                @Query("password") String password,
                                @Query("req") String req,
                                @Query("google_id") String google_id,
                                @Query("login_type") String login_type,
                                @Query("facebook_id") String facebook_id, @Part MultipartBody.Part file);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> register(@Query("email") String email,
                                @Query("name") String name,
                                @Query("password") String password,
                                @Query("req") String req,
                                @Query("google_id") String google_id,
                                @Query("login_type") String login_type,
                                @Query("image_url") String photo,
                                @Query("facebook_id") String facebook_id);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> report(@Query("req") String req,
                              @Query("report_type") String report_type,
                              @Query("email") String email,
                              @Query("status_id") String status_id,
                              @Query("description") String description,
                              @Query("name") String name);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> updateProfile(@Query("req") String req,
                                     @Query("video_type") String video_type,
                                     @Query("user_id") String user_id,
                                     @Query("name") String name);

    @Multipart
    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> uploadVideo(@Query("req") String req,
                                   @Query("video_type") String video_type,
                                   @Query("user_id") String user_id,
                                   @Query("videoname") String videoname, @Part MultipartBody.Part file);

    @POST("db.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> view(@Query("req") String req,
                              @Query("id") String id);

}
