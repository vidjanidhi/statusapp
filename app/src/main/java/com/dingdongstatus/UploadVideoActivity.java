package com.dingdongstatus;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dingdongstatus.netutils.MyPreferences;
import com.dingdongstatus.netutils.RequestInterface;
import com.dingdongstatus.netutils.RetrofitClient;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.FbAds;
import com.dingdongstatus.support.GoogleAds;
import com.facebook.ads.NativeAdLayout;

import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dingdongstatus.support.FbAds.showNativeAd;

public class UploadVideoActivity extends AppCompatActivity implements View.OnClickListener {
    private com.google.android.gms.ads.AdView mAdView;
    private FbAds googleAds;
    private RelativeLayout RRchoosevideo;
    private Button buttonUpload;
    private TextView textView;
    private static final int SELECT_VIDEO = 3;
    private ProgressDialog pDialog;
    private String UPLOAD_SERVER_URI = "http://statuscool.in/admin_statuscool/api/deenew_api.php?req=fileUpload";
    long imageSize = 0;
    File sourceFile;
    String imagePath;
    EditText editvideotitle;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 4;
    private String type = "1", ori_type = "1";
    MyPreferences myPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_video);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Upload Video");
        //todo fb ads
        NativeAdLayout native_banner_ad_container = (NativeAdLayout) findViewById(R.id.native_banner_ad_container);
        showNativeAd(this, native_banner_ad_container);

        myPreferences = new MyPreferences(this);
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("ori_type")) {
            ori_type = getIntent().getExtras().getString("ori_type");
        }

        mAdView = (com.google.android.gms.ads.AdView) findViewById(R.id.adView);
        googleAds = new FbAds(getApplicationContext());


        pDialog = new ProgressDialog(UploadVideoActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCanceledOnTouchOutside(false);

        RRchoosevideo = (RelativeLayout) findViewById(R.id.RRchoosevideo);
        buttonUpload = (Button) findViewById(R.id.buttonUpload);
        textView = (TextView) findViewById(R.id.textView);
        editvideotitle = (EditText) findViewById(R.id.editvideotitle);
        RRchoosevideo.setOnClickListener(this);
        buttonUpload.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void chooseVideo() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select a Video "), SELECT_VIDEO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_VIDEO) {
                System.out.println("SELECT_VIDEO");
                Uri selectedImageUri = data.getData();
                imagePath = getPath(selectedImageUri);

                textView.setText(imagePath);
                if (!checkIfLessThen5MB(this.imagePath)) {
                    Toast.makeText(this, "Max file size allowed 5.0MB", Toast.LENGTH_SHORT).show();
                    chooseVideo();
                    return;
                } /*else if (!checkType(selectedImageUri)) {
                    Toast.makeText(this, "orientation wrong", Toast.LENGTH_SHORT).show();
                    chooseVideo();
                    return;
                }*/
            }
        }
    }

    private boolean checkType(Uri selectedImageUri) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(this, selectedImageUri);
        int width = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        int height = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
        retriever.release();
        if (ori_type.equals(AppConstant.PORTRAIT)) {
            if (height > width) {
                return true;
            } else {
                return false;
            }
        } else if (ori_type.equals(AppConstant.LANDSCAPE)) {
            if (height < width) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private boolean checkIfLessThen5MB(String pathFile) {
        if ((new File(pathFile).length() / 1024) / 1024 > 5) {

            return false;
        }

        return true;
    }


    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
        cursor.close();

        return path;
    }

    @Override
    public void onClick(View v) {
        if (v == RRchoosevideo) {
            if (ContextCompat.checkSelfPermission(UploadVideoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(UploadVideoActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            } else {
                chooseVideo();
            }

        }
        if (v == buttonUpload) {
            if (validationSuccess()) {
//                new UploadFileToServer().execute();
                if (myPreferences.getPreferences(MyPreferences.id).equals("")) {
                    Toast.makeText(this, "Login First", Toast.LENGTH_SHORT).show();
                } else {
                    UploadVideo();

                }
            }
        }
    }

    private Boolean validationSuccess() {
        if (editvideotitle.getText().toString().equals("")) {
            editvideotitle.setError("Plz enter Video Title");
            return false;
        } else if (imagePath == null) {
            Toast.makeText(UploadVideoActivity.this, "Please Select Video", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void UploadVideo() {
        try {
            final ProgressDialog pd = new ProgressDialog(UploadVideoActivity.this);
            pd.setTitle("Please Wait");
            pd.setMessage("Loading");
            pd.setCancelable(true);
            pd.show();
            RequestInterface request = RetrofitClient.getClient().create(RequestInterface.class);

            RequestBody requestfile_image;
            MultipartBody.Part body_image;

            Call<ResponseBody> call = null;
            sourceFile = new File(imagePath);
            requestfile_image = RequestBody.create(MediaType.parse("*/*"), sourceFile);
            body_image = MultipartBody.Part.createFormData("image", sourceFile.getName(), requestfile_image);
            String id = myPreferences.getPreferences(MyPreferences.id);
            call = request.uploadVideo(
                    "" + "fileUpload",
                    "" + ori_type,
                    "" + id,
                    "" + editvideotitle.getText().toString(),
                    body_image
            );

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        pd.dismiss();
                        String json_response = response.body().string();
                        JSONObject json = new JSONObject(json_response);
                        if (json.getString("error").equals("false")) {
                            Toast.makeText(UploadVideoActivity.this, "Uploaded Successfully", Toast.LENGTH_SHORT).show();
                            editvideotitle.setText("");
                            textView.setText("Choose File Max Allow 5.0 MB");
                            /*Intent intent = new Intent();
                            setResult(11, intent);
                            finish();*/

                        } else {
//                                    Toaster.show(AddContactCompanyActivity.this, "" + json.getString("ack_msg"), false, Toaster.DANGER);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    pd.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            pDialog.dismiss();
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setMax(100);
            pDialog.setProgress(0);
            pDialog.show();
            pDialog.setProgress(progress[0]);
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(UPLOAD_SERVER_URI);
            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) imageSize) * 100));
                            }
                        });
                sourceFile = new File(imagePath);
                if (!(imagePath == null || imagePath == "")) {
                    entity.addPart("image", new FileBody(sourceFile));
                    Log.d("image", sourceFile.toString());
                }
                String videoname = editvideotitle.getText().toString();
                entity.addPart("videoname", new StringBody(videoname));
                entity.addPart("video_type", new StringBody(ori_type));
                entity.addPart("user_id", new StringBody(""));
                imageSize = entity.getContentLength();
                httppost.setEntity(entity);
                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }


            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            } catch (Exception e) {

            }
            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            Log.e("SANDIIP", "Response from server: " + result);

            pDialog.cancel();
            // imgPreview.setVisibility(imgPreview.GONE);
            imagePath = null;
            editvideotitle.setText("");
            textView.setText("Choose File Max Allow 5.0 MB");
            Log.d("UPLOAD_SERVER_URI", "" + UPLOAD_SERVER_URI);


            try {
                String decoded = new String(result);

                decoded = android.text.Html.fromHtml(decoded).toString();


                JSONObject obj = new JSONObject(decoded);
                String message = obj.getString("message");

                Toast.makeText(UploadVideoActivity.this, "" + message, Toast.LENGTH_SHORT).show();


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(UploadVideoActivity.this, "We Need Permission to this action", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

}
