package com.dingdongstatus;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dingdongstatus.netutils.GlobalElements;
import com.dingdongstatus.netutils.MyPreferences;
import com.dingdongstatus.netutils.RequestInterface;
import com.dingdongstatus.netutils.RetrofitClient;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.MyTextWatcher;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.name_edt)
    EditText nameEdt;
    @BindView(R.id.name_layout)
    TextInputLayout nameLayout;
    @BindView(R.id.mobile_edt)
    EditText mobileEdt;
    @BindView(R.id.mobile_layout)
    TextInputLayout mobileLayout;
    @BindView(R.id.password_edt)
    EditText passwordEdt;
    @BindView(R.id.password_layout)
    TextInputLayout passwordLayout;
    @BindView(R.id.btn_login)
    Button btnLogin;
    MyPreferences myPreferences;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    private int RESULT_OK = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        myPreferences = new MyPreferences(this);

        mobileEdt.addTextChangedListener(new MyTextWatcher(mobileLayout));
        passwordEdt.addTextChangedListener(new MyTextWatcher(passwordLayout));
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPickImageDialog();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalElements.isConnectingToInternet(RegisterActivity.this)) {
                    if (mobileEdt.getText().toString().equals("")) {
                        mobileLayout.setError("Enter Email");
                    } else if (passwordEdt.getText().toString().equals("")) {
                        passwordLayout.setError("Enter Password");
                    } else {
                        LoginUser();
                    }
                } else {
                    GlobalElements.showDialog(RegisterActivity.this);
                }

            }
        });
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    private void OpenCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = FileProvider.getUriForFile(this,
                getApplicationContext().getPackageName() + ".provider",
                getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, 0);
        optionDialog.dismiss();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean mayRequestStoragePermission() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_MULTIPLE);
            return false;
        }
        return true;

    }

    public static final int REQUEST_MULTIPLE = 1;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_MULTIPLE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                OpenCamera();
            }
        }
    }

    Uri selectedImage;
    File f;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    selectedImage = file;
//                    f = new File(getRealPathFromURI(selectedImage));
                    try {
                        f = FileUtil.from(this, selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imgProfile.setImageURI(selectedImage);
//                    (selectedImage);
                }
                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    selectedImage = data.getData();
//                    f = new File(selectedImage.getPath());
                    try {
                        f = FileUtil.from(this, selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imgProfile.setImageURI(selectedImage);
//                    ivProfile.setImageURI(selectedImage);
//                    uploadImage(selectedImage);
                }
                break;
            case 2:
                selectedImage = file;
//                f = new File(selectedImage.getPath());
                try {
                    f = FileUtil.from(this, selectedImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                imgProfile.setImageURI(selectedImage);
//                    ivProfile.setImageURI(selectedImage);
//                uploadImage(selectedImage);
                break;
        }
    }


    Uri file;
    Dialog optionDialog = null;
    String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private void showPickImageDialog() {
        optionDialog = new Dialog(this);
        optionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        optionDialog.setCancelable(true);
        optionDialog.setContentView(R.layout.dialog_choose_picture);

        ImageView iv_camera = optionDialog.findViewById(R.id.iv_camera);
        ImageView iv_galary = optionDialog.findViewById(R.id.iv_galary);
        iv_camera.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (mayRequestStoragePermission()) {
                    OpenCamera();
                }
            }
        });
        iv_galary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
                optionDialog.dismiss();
            }
        });
        optionDialog.show();
    }


    private void LoginUser() {
        try {
            final ProgressDialog pd = new ProgressDialog(RegisterActivity.this);
            pd.setTitle("Please Wait");
            pd.setMessage("Loading");
            pd.setCancelable(true);
            pd.show();
            RequestBody requestfile_image;
            MultipartBody.Part body_image;
            requestfile_image = RequestBody.create(MediaType.parse("*/*"), f);
            body_image = MultipartBody.Part.createFormData("photo", f.getName(), requestfile_image);
           /* RequestInterface request = RetrofitClient.getClient().create(RequestInterface.class);*/
            Call<ResponseBody> call = AppConstant.request.register("" + mobileEdt.getText().toString(),
                    "" + nameEdt.getText().toString(),
                    "" + passwordEdt.getText().toString(),
                    "register", "", "", "",
                    body_image
            );

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        pd.dismiss();
                        String jsonst = response.body().string();
                        JSONObject json = new JSONObject(jsonst);
                        if (json.getInt("success") == 1) {
                            JSONArray array = json.getJSONArray("data");
                            JSONObject result = array.getJSONObject(0);
                            String name = result.getString("name");
                            String email = result.getString("email");
                            String image_url = result.getString("image");
                            String id = result.getString("id");
                            myPreferences.setPreferences(MyPreferences.email, email);
                            myPreferences.setPreferences(MyPreferences.id, id);
                            myPreferences.setPreferences(MyPreferences.name, name);
                            myPreferences.setPreferences(MyPreferences.login_type, "3");
                            myPreferences.setPreferences(MyPreferences.profile_image, image_url);
                            Intent myIntent = new Intent(RegisterActivity.this,
                                    MainActivity.class);
                            startActivity(myIntent);
//                            setResult(101);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "" + json.getString("data"), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    pd.dismiss();
                    System.out.print("error" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
