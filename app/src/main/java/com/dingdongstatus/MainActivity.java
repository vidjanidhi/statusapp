package com.dingdongstatus;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dingdongstatus.Apprate.AppRate;
import com.dingdongstatus.Apprate.Utils;
import com.dingdongstatus.Interfaces.RvOnscrollListner;
import com.dingdongstatus.adapter.MoreappAdapter;
import com.dingdongstatus.model.GeneralModel;
import com.dingdongstatus.model.Moreapp;
import com.dingdongstatus.netutils.MyPreferences;
import com.dingdongstatus.netutils.RetrofitClient;
import com.dingdongstatus.pageadapter.MainPagerAdapter;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.FbAds;
import com.facebook.CallbackManager;
import com.google.android.gms.ads.AdView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements RvOnscrollListner {

    ArrayList<GeneralModel> data = new ArrayList<>();
    MainPagerAdapter pagerAdapter;
    String type;
    @BindView(R.id.view_pager_main)
    ViewPager viewPager;
    @BindView(R.id.tab_layout_main)
    TabLayout tabLayout;
    @BindView(R.id.adView)
    AdView adView;
    @BindView(R.id.ad_container)
    LinearLayout adContainer;
    @BindView(R.id.drawer_layout)
    LinearLayout drawerLayout;
    @BindView(R.id.fab_upload)
    FloatingActionButton fabUpload;

    private FbAds googleAds;
    MyPreferences myPreferences;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    RelativeLayout LLapp;
    ProgressBar progress;
    ArrayList<Moreapp> arrayMoreApps;
    RecyclerView recyclerViewMoreApp;
    RecyclerView.Adapter adapter;
    private CallbackManager callbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

       new  Utils.generatePictureStyleNotification(this,"test", "test", "https://lh3.googleusercontent.com/SDFJkbeUozArC28icmsfW7QVqV1vYd29_N-aujNYxpeYhbF1e9r-a7Px14g49MPoKYQ=s180-rw").execute();

        /*Utils.generatePictureStyleNotification("test", "test", "https://lh3.googleusercontent.com/SDFJkbeUozArC28icmsfW7QVqV1vYd29_N-aujNYxpeYhbF1e9r-a7Px14g49MPoKYQ=s180-rw", this).e;*/
        //todo fb ads
        setRateSettings();
        checkUpdate();
        /*NativeAdLayout native_banner_ad_container = (NativeAdLayout) findViewById(R.id.native_banner_ad_container);
        showNativeAd(this, native_banner_ad_container);*/
        googleAds = new FbAds(MainActivity.this);

        /*try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.makewellvideostatus",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();

        }*/
//        setSupportActionBar(toolbar);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(RetrofitClient.REGISTRATION_COMPLETE)) {
                    sendFirebaseRegId();
                }
            }
        };

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
//            versionTxt.setText("Version " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        myPreferences = new MyPreferences(this);
        fabUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myPreferences.getPreferences(MyPreferences.id).equals("")) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivityForResult(intent, 101);
                } else {
                    Intent intent = new Intent(MainActivity.this, UploadVideoActivity.class);
                    startActivity(intent);
                }

            }
        });

//        LoadTiles();

    }

    private void setRateSettings() {
        AppRate.with(this)
                .setInstallDays(0) // default 10, 0 means install day.
                .setLaunchTimes(1) // default 10
                .setRemindInterval(1) // default 1
                .setShowLaterButton(true) // default true
                .setDebug(false) // default false
                .monitor();

        AppRate.showRateDialogIfMeetsConditions(this);
        AppRate.with(this).clearAgreeShowDialog();
    }

    private void checkUpdate() {
//        RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);
        Call<ResponseBody> call = AppConstant.request.getMoreApp(
                "app_update"
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
//                    arrayMoreApps.clear();
                    try {
                        JSONArray contacts = json.getJSONArray("data");
                        for (int i = 0; i < contacts.length(); i++) {
                            JSONObject c = contacts.getJSONObject(i);
                            String id = c.getString("id");
                            String icon = c.getString("icon");
                            String message = c.getString("message");
                            final String link = c.getString("link");
                            String status = c.getString("status");
                            String order = c.getString("order");

                            if (status.equals("1")) {
                                Dialog optionDialog = null;
                                optionDialog = new Dialog(MainActivity.this);
                                optionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                optionDialog.setCancelable(false);
                                optionDialog.setContentView(R.layout.dialog_update);
                                TextView btn_exit = optionDialog.findViewById(R.id.btn_exit);
                                TextView btn_update = optionDialog.findViewById(R.id.btn_update);
                                TextView tv_msg = optionDialog.findViewById(R.id.tv_msg);
                                tv_msg.setText(message);
                                btn_exit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                });
                                btn_update.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent i = new Intent(Intent.ACTION_VIEW);
                                        i.setData(Uri.parse(link));
                                        startActivity(i);
                                    }
                                });
                                optionDialog.show();
                            } else {
                                LoadTiles();
                            }
                        }
                    } catch (JSONException e) {
                        Log.d("From Method", "");
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.print("error" + t.getMessage());
            }
        });
    }

    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(RetrofitClient.REGISTRATION_COMPLETE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(RetrofitClient.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void sendFirebaseRegId() {
        String regId = myPreferences.getPreferences(MyPreferences.regId);
        if (!regId.equals("")) {
            String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            sendToken(regId, android_id);
        }
    }

    private void sendToken(String reg_id, String android_id) {
        /*RequestInterface req = RetrofitClient.getClient1().create(RequestInterface.class);*/
        Call<ResponseBody> call = AppConstant.request.sendToken(
                reg_id + "",
                android_id + ""
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("", " ");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.print("error" + t.getMessage());
            }
        });
    }

    private void LoadTiles() {
        GeneralModel da = new GeneralModel();
        da = new GeneralModel();
        da.setId("1");
        da.setName("Portrait");
        da.setSlug(AppConstant.PORTRAIT);
        da.setIcon(R.drawable.ic_stay_primary_portrait_black_24dp);
        data.add(da);
        da = new GeneralModel();
        da.setId("2");
        da.setName("Landscape");
        da.setSlug(AppConstant.LANDSCAPE);
        da.setIcon(R.drawable.ic_stay_primary_landscape_black_24dp);
        data.add(da);

        pagerAdapter = new MainPagerAdapter(this.getSupportFragmentManager(), data, this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(1);
        pagerAdapter.notifyDataSetChanged();

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                googleAds.caclulateIntersialAds();
                Drawable drawable = ContextCompat.getDrawable(MainActivity.this, Integer.parseInt(tab.getTag().toString()));
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable, getResources().getColor(R.color.darkOrchid));

                ImageView img = (ImageView) tab.getCustomView().findViewById(R.id.tab_icon);
                img.setImageDrawable(drawable);
               /* if (tab.getTag().toString().equals(AppConstant.PORTRAIT)) {

                }*/
//                tvTitle.setText(tab.getTag().toString());
//                getSupportActionBar().setTitle(tab.getText());
//                tab.getIcon().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Drawable drawable = ContextCompat.getDrawable(MainActivity.this, Integer.parseInt(tab.getTag().toString()));
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorPrimaryDark));

                ImageView img = (ImageView) tab.getCustomView().findViewById(R.id.tab_icon);
                img.setImageDrawable(drawable);
//                tab.getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setupTabIcons() {
        for (int i = 0; i < data.size(); i++) {
            tabLayout.getTabAt(i).setCustomView(R.layout.tab_custom_layout_main);
            tabLayout.getTabAt(i).setTag(data.get(i).getIcon());
            TextView tab_name = (TextView) tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tv_tab);
            tab_name.setText("" + data.get(i).getName());

            Drawable drawable = ContextCompat.getDrawable(this, data.get(i).getIcon());
            drawable = DrawableCompat.wrap(drawable);
            if (i == 0) {
                DrawableCompat.setTint(drawable, getResources().getColor(R.color.darkOrchid));
            } else {
                DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorPrimaryDark));
            }
//            tabLayout.getTabAt(i).setIcon(drawable);

            ImageView img = (ImageView) tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_icon);
            img.setImageDrawable(drawable);
        }
    }

  /*  @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_search) {

            final SearchView searchView;
            if (dataArrives)
                googleAds.caclulateIntersialAds();
            searchView = (SearchView) item.getActionView();
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    if (!searchView.isIconified()) {
                        searchView.setIconified(true);

                        Intent i = new Intent(MainActivity.this, SearchViewActivity.class);
                        i.putExtra("searchtext", query.toString());
                        i.putExtra("mode", "search");
                        startActivity(i);
                    }
                    item.collapseActionView();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {

                    return false;
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onBackPressed() {
        googleAds.caclulateIntersialAds();
       /* AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder
                .setMessage("Are you sure you want to Exit?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();*/
//            super.onBackPressed();
        exitDialog();
    }

   /* @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_favourite) {
            Intent intent = new Intent(MainActivity.this, DownloadedListActivity.class);
            intent.putExtra("from", "2");
            startActivity(intent);
        } else if (id == R.id.nav_download) {
            Intent intent = new Intent(MainActivity.this, DownloadedListActivity.class);
            intent.putExtra("from", "1");
            startActivity(intent);
        } *//*else if (id == R.id.nav_upload_video) {
            Intent intent = new Intent(MainActivity.this, UploadVideoActivity.class);
            startActivity(intent);
        }*//* else if (id == R.id.nav_share_app) {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra("android.intent.extra.TEXT",
                    "Hey, I just found this awesome app on play store. " +
                            "You can download thousands of video status in this app.\n" +
                            " https://play.google.com/store/apps/details?id=" + getPackageName());
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } else if (id == R.id.nav_rate_app) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("market://details?id=" + getPackageName()));
            startActivity(i);
        } *//*else if (id == R.id.nav_more_app) {
            if (GlobalElements.isConnectingToInternet(MainActivity.this)) {
                getMoreApp();
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://search?q=pub:Viral360"));
                startActivity(intent);
            }
        } *//* else if (id == R.id.nav_privacy_policy) {
            Intent intent = new Intent(MainActivity.this, PrivacyPolicyActivity.class);
            startActivity(intent);
        } *//*else if (id == R.id.nav_update_app) {
            new GetVersionCode().execute();


        }*//* else if (id == R.id.nav_exit) {
            if (dataArrives)
                googleAds.caclulateIntersialAds();
            finish();
//            exitDialog();
//            onBackPressed();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (!myPreferences.getPreferences(MyPreferences.id).equals("")) {
                Intent in = new Intent(MainActivity.this, UploadVideoActivity.class);
                startActivity(in);
            }
        }
    }

    void exitDialog() {
        Rect displayRectangle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_layout_exit, null);

        dialogBuilder.setView(dialogView);

        TextView tv_title = (TextView) dialogView.findViewById(R.id.tv_title);
        TextView tv_more_App_title = (TextView) dialogView.findViewById(R.id.tv_more_App_title);
        TextView tv_rate_app = (TextView) dialogView.findViewById(R.id.tv_rate_app);
        TextView txtcancel = (TextView) dialogView.findViewById(R.id.txtcancel);
        TextView txtexit = (TextView) dialogView.findViewById(R.id.txtexit);


        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/app/Montserrat-Regular.ttf");
        Typeface typeface_bold = Typeface.createFromAsset(getAssets(), "fonts/app/Montserrat-Bold.ttf");
        tv_title.setTypeface(typeface_bold);
        tv_more_App_title.setTypeface(typeface);
        tv_rate_app.setTypeface(typeface);
        txtcancel.setTypeface(typeface);
        txtexit.setTypeface(typeface);

        progress = (ProgressBar) dialogView.findViewById(R.id.progressBar);
        recyclerViewMoreApp = (RecyclerView) dialogView.findViewById(R.id.recyclerViewMoreApp);

//        LLapp = (RelativeLayout) dialogView.findViewById(R.id.LLapp);

        recyclerViewMoreApp.setVisibility(View.VISIBLE);
        recyclerViewMoreApp.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerViewMoreApp.setLayoutManager(layoutManager);
        arrayMoreApps = new ArrayList<>();
        final AlertDialog alertDialog = dialogBuilder.create();

        adapter = new DataAdapter(arrayMoreApps);
        recyclerViewMoreApp.setAdapter(adapter);
//

        /*RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*/
        Call<ResponseBody> call = AppConstant.request.getMoreApp(
                "moreapp"
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    arrayMoreApps.clear();
                    try {
                        JSONArray contacts = json.getJSONArray("data");
                        for (int i = 0; i < contacts.length(); i++) {
                            JSONObject c = contacts.getJSONObject(i);
                            String id = c.getString("id");
                            String icon = c.getString("icon");
                            String message = c.getString("imgurl");
                            String url = c.getString("url");

                            Moreapp gsMoreapp = new Moreapp();
                            gsMoreapp.setId(id);
                            gsMoreapp.setDevname("url");
                            gsMoreapp.setIcon(icon);
                            gsMoreapp.setName(message);
                            gsMoreapp.setUrl(url);
                            arrayMoreApps.add(gsMoreapp);
                        }
                        adapter.notifyDataSetChanged();
                        progress.setVisibility(View.GONE);

                    } catch (JSONException e) {
                        Log.d("From Method", "");
                        e.printStackTrace();
                    }
//                    showAlert();


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.print("error" + t.getMessage());
            }
        });


        txtexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();

            }
        });

        tv_rate_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("market://details?id=" + getPackageName()));
                startActivity(i);

            }
        });

        txtcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

            }
        });

        alertDialog.show();

    }

    private List<Moreapp> moreappList = new ArrayList<>();
    private MoreappAdapter moreappAdapter;
    AlertDialog alertDialog;
    private RecyclerView recyclerViewmoreapp;

    private void getMoreApp() {
        /*RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*/
        Call<ResponseBody> call = AppConstant.request.getMoreApp(
                "moreapp"
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    moreappList.clear();
                    try {
                        JSONArray contacts = json.getJSONArray("data");
                        for (int i = 0; i < contacts.length(); i++) {
                            JSONObject c = contacts.getJSONObject(i);
                            String id = c.getString("id");
                            String icon = c.getString("icon");
                            String message = c.getString("imgurl");
                            String url = c.getString("url");

                            Moreapp gsMoreapp = new Moreapp();
                            gsMoreapp.setId(id);
                            gsMoreapp.setIcon(icon);
                            gsMoreapp.setImgurl(message);
                            gsMoreapp.setUrl(url);
                            moreappList.add(gsMoreapp);
                        }

                    } catch (JSONException e) {
                        Log.d("From Method", "");
                        e.printStackTrace();
                    }
                    showAlert();

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertDialogBuilder.setTitle("More Apps");

                    final FrameLayout frameView = new FrameLayout(MainActivity.this);
                    alertDialogBuilder.setView(frameView);
                    alertDialog = alertDialogBuilder.create();

                    LayoutInflater inflater = alertDialog.getLayoutInflater();
                    View dialoglayout = inflater.inflate(R.layout.dialog_layout, frameView);
                    recyclerViewmoreapp = (RecyclerView) dialoglayout.findViewById(R.id.recycler_viewmoreapp);
                    RecyclerView.LayoutManager mLayoutManagerlist = new LinearLayoutManager(MainActivity.this);
                    recyclerViewmoreapp.setLayoutManager(mLayoutManagerlist);
                    recyclerViewmoreapp.setItemAnimator(new DefaultItemAnimator());
                    moreappAdapter = new MoreappAdapter(MainActivity.this, moreappList, alertDialog);
                    recyclerViewmoreapp.setAdapter(moreappAdapter);
                    moreappAdapter.notifyDataSetChanged();
                    alertDialog.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.print("error" + t.getMessage());
            }
        });
    }

    @Override
    public void show() {
        fabUpload.setVisibility(View.VISIBLE);
        tabLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hide() {
        fabUpload.setVisibility(View.GONE);
        tabLayout.setVisibility(View.GONE);
    }

    public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
        private ArrayList<Moreapp> moreApp;

        public DataAdapter(ArrayList<Moreapp> moreApp) {
            this.moreApp = moreApp;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_more_app_list_exit, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, int i) {

            final Moreapp gsMoreApp = moreApp.get(i);
            viewHolder.txtAppName.setText(gsMoreApp.getName());
            viewHolder.txtCompanyName.setText(gsMoreApp.getDevname());
            viewHolder.txtSizeAndRate.setText(gsMoreApp.getSize() + " · " + gsMoreApp.getRate() + " ⋆");

            Picasso.with(getApplicationContext())
                    .load(gsMoreApp.getIcon().trim())
                    .fit()
                    .placeholder(R.color.cardview_dark_background)
                    .error(R.color.black)
                    .into(viewHolder.imgIcon);

            viewHolder.llClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(gsMoreApp.getUrl())));
                }
            });
        }

        @Override
        public int getItemCount() {
            return moreApp.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView txtAppName, txtCompanyName, txtSizeAndRate;
            private ImageView imgIcon;
            private LinearLayout llClick;

            public ViewHolder(View view) {
                super(view);

                imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
                txtAppName = (TextView) view.findViewById(R.id.txtAppName);
                txtCompanyName = (TextView) view.findViewById(R.id.txtCompanyName);
                txtSizeAndRate = (TextView) view.findViewById(R.id.txtSizeAndRate);
                llClick = (LinearLayout) view.findViewById(R.id.llClick);

            }
        }

    }

    private void showAlert() {

    }

}
