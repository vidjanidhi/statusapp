package com.dingdongstatus.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dingdongstatus.model.VideoModel;

import java.util.ArrayList;

/**
 * Created by Matrix 10 on 16-08-2016.
 */
public class Database extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 8;

    private static final String DATABASE_NAME = "PandeVideoStatus";
    private static final String TABLE_BOOKMARK = "bookmark";
    //Language
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_IMAGE_URL = "imgurl";
    private static final String KEY_VIDEO_URL = "videourl";
    private static final String KEY_TAG = "tag";
    private static final String KEY_DOWNLOAD = "downloads";
    private static final String KEY_VIEW = "view";
    private static final String KEY_SHARE = "share";
    private static final String KEY_STATUS = "status";
    private static final String KEY_DATE_TIME = "datetime";

    private static final String KEY_IS_FAVOURITE = "is_fav";

    private static final String TABLE_download = "download";
    //Language

    // Table Create Language
    private static final String CREATE_TABLE_BOOKMARK = "CREATE TABLE "
            + TABLE_BOOKMARK + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_IMAGE_URL + " TEXT,"
            + KEY_VIDEO_URL + " TEXT,"
            + KEY_TAG + " TEXT,"
            + KEY_DOWNLOAD + " TEXT,"
            + KEY_VIEW + " TEXT,"
            + KEY_SHARE + " TEXT,"
            + KEY_STATUS + " TEXT,"
            + KEY_DATE_TIME + " TEXT,"
            + KEY_IS_FAVOURITE + " TEXT" + ")";

    private static final String CREATE_TABLE_download = "CREATE TABLE "
            + TABLE_download + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_IMAGE_URL + " TEXT,"
            + KEY_VIDEO_URL + " TEXT,"
            + KEY_TAG + " TEXT,"
            + KEY_DOWNLOAD + " TEXT,"
            + KEY_VIEW + " TEXT,"
            + KEY_SHARE + " TEXT,"
            + KEY_STATUS + " TEXT,"
            + KEY_DATE_TIME + " TEXT,"
            + KEY_IS_FAVOURITE + " TEXT" + ")";


    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_BOOKMARK);
        db.execSQL(CREATE_TABLE_download);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKMARK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_download);
        onCreate(db);
    }

    public void addDownloadedStatus(VideoModel videoModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, videoModel.getId());
        values.put(KEY_NAME, videoModel.getName());
        values.put(KEY_IMAGE_URL, videoModel.getImgurl());
        values.put(KEY_VIDEO_URL, videoModel.getVideourl());
        values.put(KEY_TAG, videoModel.getTag());
        values.put(KEY_DOWNLOAD, videoModel.getDownloads());
        values.put(KEY_VIEW, videoModel.getView());
        values.put(KEY_SHARE, videoModel.getShare());
        values.put(KEY_STATUS, "1");
        values.put(KEY_DATE_TIME, videoModel.getDatetime());
        values.put(KEY_IS_FAVOURITE, "0");

        db.insert(TABLE_download, null, values);
        db.close();
    }

    public void addFavStatus(VideoModel videoModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, videoModel.getId());
        values.put(KEY_NAME, videoModel.getName());
        values.put(KEY_IMAGE_URL, videoModel.getImgurl());
        values.put(KEY_VIDEO_URL, videoModel.getVideourl());
        values.put(KEY_TAG, videoModel.getTag());
        values.put(KEY_DOWNLOAD, videoModel.getDownloads());
        values.put(KEY_VIEW, videoModel.getView());
        values.put(KEY_SHARE, videoModel.getShare());
        values.put(KEY_STATUS, "0");
        values.put(KEY_DATE_TIME, videoModel.getDatetime());
        values.put(KEY_IS_FAVOURITE, "1");

        db.insert(TABLE_BOOKMARK, null, values);
        db.close();
    }


    public ArrayList<VideoModel> getAllDownloadedStatus() {
        ArrayList<VideoModel> arrayStatuses = new ArrayList<VideoModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_download + " where " + KEY_STATUS + " = 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                VideoModel videoModel = new VideoModel();
                videoModel.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                videoModel.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                videoModel.setImgurl(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_URL)));
                videoModel.setVideourl(cursor.getString(cursor.getColumnIndex(KEY_VIDEO_URL)));
                videoModel.setTag(cursor.getString(cursor.getColumnIndex(KEY_TAG)));
                videoModel.setDownloads(cursor.getString(cursor.getColumnIndex(KEY_DOWNLOAD)));
                videoModel.setView(cursor.getString(cursor.getColumnIndex(KEY_VIEW)));
                videoModel.setShare(cursor.getString(cursor.getColumnIndex(KEY_SHARE)));
                videoModel.setStatus(cursor.getString(cursor.getColumnIndex(KEY_STATUS)));
                videoModel.setDatetime(cursor.getString(cursor.getColumnIndex(KEY_DATE_TIME)));
                arrayStatuses.add(videoModel);

            } while (cursor.moveToNext());
        }
        // return contact list
        return arrayStatuses;
    }

    public ArrayList<VideoModel> getAllFavStatus() {
        ArrayList<VideoModel> arrayStatuses = new ArrayList<VideoModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BOOKMARK + " where " + KEY_IS_FAVOURITE + " = 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                VideoModel videoModel = new VideoModel();
                videoModel.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                videoModel.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                videoModel.setImgurl(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_URL)));
                videoModel.setVideourl(cursor.getString(cursor.getColumnIndex(KEY_VIDEO_URL)));
                videoModel.setTag(cursor.getString(cursor.getColumnIndex(KEY_TAG)));
                videoModel.setDownloads(cursor.getString(cursor.getColumnIndex(KEY_DOWNLOAD)));
                videoModel.setView(cursor.getString(cursor.getColumnIndex(KEY_VIEW)));
                videoModel.setShare(cursor.getString(cursor.getColumnIndex(KEY_SHARE)));
                videoModel.setStatus(cursor.getString(cursor.getColumnIndex(KEY_STATUS)));
                videoModel.setDatetime(cursor.getString(cursor.getColumnIndex(KEY_DATE_TIME)));

                arrayStatuses.add(videoModel);

            } while (cursor.moveToNext());
        }
        // return contact list
        return arrayStatuses;
    }

    public Boolean checkFavourite(String sid) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_BOOKMARK + " where " + KEY_ID + "=" + sid + " and " + KEY_IS_FAVOURITE + "=1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        boolean exists = (cursor.getCount() > 0);

        if (exists)
            return true;
        else
            return false;
    }

    public void deleteFavouriteStatus(String statis_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BOOKMARK, KEY_ID + " = ?",
                new String[]{String.valueOf(statis_id)});
    }

    public void deleteDownloadStatus(String statis_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_download, KEY_ID + " = ?",
                new String[]{String.valueOf(statis_id)});
    }


}
