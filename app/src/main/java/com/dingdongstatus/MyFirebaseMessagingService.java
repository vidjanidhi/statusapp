package com.dingdongstatus;


import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.loopj.android.http.HttpGet;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.entity.BufferedHttpEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.TextUtils;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    static final String TAG = "notification";
    public static final int ID_SMALL_NOTIFICATION = 235;
    private int uniqueId;
    String CHANNEL_ID = "vid";
    CharSequence name = "Event Notifications";
    int importance = NotificationManager.IMPORTANCE_HIGH;


    // private NotificationUtils notificationUtils;
    Context mContext;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Log.e(TAG, "From: " + remoteMessage.getFrom());
        mContext = getApplicationContext();
        if (remoteMessage == null)
            return;


        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }


    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("Title");
            String message = "message";

            String imageUrl = data.getString("Image").replace(" ", "%20");
            String URL = data.getString("Image").replace(" ", "%20");
            String ImageSmall = data.getString("Image").replace(" ", "%20");


            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "imageUrl: " + imageUrl);


            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            // resultIntent.setData(Uri.parse("market://details?id="+URL));

            Bitmap bitmap = getBitmapFromURL(imageUrl);
            Bitmap bitmapSmall = getBitmapFromURL(imageUrl);
//            Bitmap bitmapSmall = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

            long when = System.currentTimeMillis();
            if (bitmap != null) {
                showBigNotification(getApplicationContext(), bitmap, bitmapSmall, title, message, resultIntent, when);
            } else {
                return;
            }

            //startActivity(intent);

            /*
            // app is in background, show the notification in notification tray
            Intent resultIntent = new Intent(getApplicationContext(), Home.class);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("category_id","");
            resultIntent.putExtra("category_quantity","");
            resultIntent.putExtra("category_name","");
               */

            /*
            // check for image attachment
            if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, ts, resultIntent);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, ts, resultIntent, imageUrl);
            }
            */


        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }


    private void showBigNotification(Context mContext, Bitmap bitmap, Bitmap bitmapSmall, String title, String des, Intent resultPendingIntent1, long when) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            ID_SMALL_NOTIFICATION,
                            resultPendingIntent1,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            RemoteViews notification_large = new RemoteViews(mContext.getPackageName(), R.layout.notification_large);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            Notification.Builder mNotifyBuilder = new Notification.Builder(mContext, "main").setContentIntent(resultPendingIntent);

            mNotifyBuilder.setSmallIcon(R.drawable.ic_notication);
//            mNotifyBuilder.setCustomContentView(notification_large);
            mNotifyBuilder.setCustomBigContentView(notification_large);
            setBitmap(notification_large, R.id.imageView_large, bitmap);
            setBitmap(notification_large, R.id.imageView, bitmapSmall);
            notification_large.setTextViewText(R.id.txtName, title);


            mNotifyBuilder.setChannelId(CHANNEL_ID);
            nManager.createNotificationChannel(mChannel);
            initChannels(nManager);
            Notification notification = mNotifyBuilder.build();
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            nManager.notify("Heyooooo", uniqueId, notification);

        } else {

            // resultPendingIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            //      resultPendingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            //PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, resultPendingIntent,   PendingIntent.FLAG_CANCEL_CURRENT);
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, resultPendingIntent1, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification foregroundNote;
            RemoteViews notification_large = new RemoteViews(mContext.getPackageName(), R.layout.notification_large);
            RemoteViews notification_small = new RemoteViews(mContext.getPackageName(), R.layout.notification_small);

            Notification.Builder mNotifyBuilder = new Notification.Builder(mContext).setContentIntent(pendingIntent);


            foregroundNote = mNotifyBuilder.setContentText("")
                    .setSmallIcon(R.drawable.ic_notication).build();


            foregroundNote.bigContentView = notification_large;
            foregroundNote.contentView = notification_small;


            setBitmap(notification_large, R.id.imageView_large, bitmap);
            setBitmap(notification_large, R.id.imageView, bitmapSmall);
            notification_large.setTextViewText(R.id.txtName, title);
            notification_small.setTextViewText(R.id.txtName, title);
            setBitmap(notification_small, R.id.imageView, bitmapSmall);

            foregroundNote.flags += Notification.FLAG_AUTO_CANCEL;
//            foregroundNote.flags |= Notification.FLAG_AUTO_CANCEL;
            foregroundNote.defaults |= Notification.DEFAULT_SOUND;

            Random randomno = new Random();
            NotificationManager mNotifyManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            initChannels(mNotifyManager);
            mNotifyManager.notify(randomno.nextInt(10000), foregroundNote);


        }
    }


    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        showNotificationMessage(title, message, timeStamp, intent, null);
    }


    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        // notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }


    public void showNotificationMessage(final String title, final String message, final String timeStamp, Intent intent, String imageUrl) {
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;


        // notification icon
        final int icon = R.mipmap.ic_launcher;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mContext,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext, "main");


        //  final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
        //        + "://" + mContext.getPackageName() + "/raw/notification");
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        // mBuilder.setSound(soundUri);
        if (!TextUtils.isEmpty(imageUrl)) {

            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

                Bitmap bitmap = getBitmapFromURL(imageUrl);

                if (bitmap != null) {
                    showBigNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                } else {
                    showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                }
            }
        } else {
            showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
            playNotificationSound();
        }
    }


    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        inboxStyle.addLine(message);

        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(inboxStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                //  .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        initChannels(notificationManager);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(1, notification);
    }

    private void showBigNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {

        Drawable d = new BitmapDrawable(getResources(), bitmap);

        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(bigPictureStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                //.setSmallIcon(R.mipmap.ic_launcher)
                .setSmallIcon(33)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();


        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        initChannels(notificationManager);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(1, notification);
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL) {
        String urlStr = strURL;
        Bitmap img = null;

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(urlStr);
        HttpResponse response;
        try {
            response = (HttpResponse) client.execute(request);
            HttpEntity entity = response.getEntity();
            BufferedHttpEntity bufferedEntity = new BufferedHttpEntity(entity);
            InputStream inputStream = bufferedEntity.getContent();
            img = BitmapFactory.decodeStream(inputStream);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return img;
    }

    // Playing notification sound
    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + mContext.getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void setBitmap(RemoteViews views, int resId, Bitmap bitmap) {
        Bitmap proxy = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(proxy);
        c.drawBitmap(bitmap, new Matrix(), null);
        views.setImageViewBitmap(resId, proxy);
    }


    private static void initChannels(NotificationManager notificationManager) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }

        NotificationChannel channel = new NotificationChannel("main",
                "Video Notification",
                NotificationManager.IMPORTANCE_LOW);
        channel.setDescription("Video Notification");
        channel.enableVibration(false);
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                .build();

        notificationManager.createNotificationChannel(channel);
    }

}