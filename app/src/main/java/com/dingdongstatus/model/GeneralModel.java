package com.dingdongstatus.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by CRAFT BOX on 2/24/2018.
 */

public class GeneralModel implements Serializable {

    String id;
    String name;
    boolean isCheck;
    String colorCode;
    String slug;
    String service_id;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    int icon;

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public ArrayList<GeneralModel> getOperatorData() {
        return operatorData;
    }

    public void setOperatorData(ArrayList<GeneralModel> operatorData) {
        this.operatorData = operatorData;
    }

    ArrayList<GeneralModel> operatorData= new ArrayList<>();

}
