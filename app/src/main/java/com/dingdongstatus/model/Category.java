package com.dingdongstatus.model;

import java.io.Serializable;

/**
 * Created by Jatin on 20-01-2017.
 */

public class Category implements Serializable {



    private String category_id="";
    private String category_name="";
    private String category_image="";
    private String category_order="";
    private String category_status="";


    @Override
    public String toString() {
        return "Category{" +
                "category_id='" + category_id + '\'' +
                ", category_name='" + category_name + '\'' +
                ", category_image='" + category_image + '\'' +
                ", category_order='" + category_order + '\'' +
                ", category_status='" + category_status + '\'' +
                '}';
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_image() {
        return category_image;
    }

    public void setCategory_image(String category_image) {
        this.category_image = category_image;
    }

    public String getCategory_order() {
        return category_order;
    }

    public void setCategory_order(String category_order) {
        this.category_order = category_order;
    }

    public String getCategory_status() {
        return category_status;
    }

    public void setCategory_status(String category_status) {
        this.category_status = category_status;
    }
}
