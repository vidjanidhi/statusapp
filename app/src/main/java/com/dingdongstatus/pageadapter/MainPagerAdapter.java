package com.dingdongstatus.pageadapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dingdongstatus.MainFragment.LandscapeFragment;
import com.dingdongstatus.MainFragment.PortraitFragment;
import com.dingdongstatus.model.GeneralModel;
import com.dingdongstatus.netutils.MyPreferences;
import com.dingdongstatus.support.FbAds;
import com.dingdongstatus.support.GoogleAds;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CRAFT BOX on 12/23/2016.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {
    Fragment f;
    List<Fragment> mFragmentList = new ArrayList<>();
    ArrayList<GeneralModel> categoryModels = new ArrayList<>();
    String type;
    MyPreferences myPreferences;
    Context context;
    FbAds googleAds;

    public MainPagerAdapter(FragmentManager fm, ArrayList<GeneralModel> topCategoryModels, Context context) {
        super(fm);
        this.categoryModels = topCategoryModels;
        this.context = context;
        googleAds = new FbAds(context);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "" + categoryModels.get(position).getName();//if you want title then return name
    }

    @Override
    public int getCount() {
        return categoryModels.size();
    }

    @Override
    public Fragment getItem(int position) {
        myPreferences = new MyPreferences(context);
        type = myPreferences.getPreferences(MyPreferences.type);

        if (position == 0) {
            f = new PortraitFragment();
            Bundle bundle = new Bundle();
            bundle.putString("fid", "" + categoryModels.get(position).getId());
            bundle.putString("title", "" + categoryModels.get(position).getName());
            f.setArguments(bundle);
            mFragmentList.add(f);
            return f;

        } else if (position == 1) {
            f = new LandscapeFragment();
            Bundle bundle = new Bundle();
            bundle.putString("fid", "" + categoryModels.get(position).getId());
            bundle.putString("title", "" + categoryModels.get(position).getName());
            f.setArguments(bundle);
            mFragmentList.add(f);
            return f;
        }else {
            return f;
        }

    }
}
