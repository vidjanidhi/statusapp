package com.dingdongstatus.pageadapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dingdongstatus.LandscapeFragment.CategoryFragmentLandscape;
import com.dingdongstatus.LandscapeFragment.HomeFragmentLandscape;
import com.dingdongstatus.LandscapeFragment.SearchFragmentLandscape;
import com.dingdongstatus.LandscapeFragment.TrendingFragmentLandscape;
import com.dingdongstatus.PortraitFragment.MoreFragment;
import com.dingdongstatus.model.GeneralModel;
import com.dingdongstatus.netutils.MyPreferences;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.FbAds;
import com.dingdongstatus.support.GoogleAds;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CRAFT BOX on 12/23/2016.
 */

public class LandscapePagerAdapter extends FragmentPagerAdapter {
    Fragment f;
    List<Fragment> mFragmentList = new ArrayList<>();
    ArrayList<GeneralModel> categoryModels = new ArrayList<>();
    String type;
    MyPreferences myPreferences;
    Context context;
    FbAds googleAds;

    public LandscapePagerAdapter(FragmentManager fm, ArrayList<GeneralModel> topCategoryModels, Context context) {
        super(fm);
        this.categoryModels = topCategoryModels;
        this.context = context;
        googleAds = new FbAds(context);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "" + categoryModels.get(position).getName();//if you want title then return name
    }

    @Override
    public int getCount() {
        return categoryModels.size();
    }

    @Override
    public Fragment getItem(int position) {
        myPreferences = new MyPreferences(context);
        type = myPreferences.getPreferences(MyPreferences.type);


        if (position == 0) {
            f = new HomeFragmentLandscape();
            Bundle bundle = new Bundle();
            bundle.putString("fid", "" + categoryModels.get(position).getId());
            bundle.putString("title", "" + categoryModels.get(position).getName());
            f.setArguments(bundle);
            mFragmentList.add(f);
            return f;

        } else if (position == 1) {
            f = new CategoryFragmentLandscape();
            Bundle bundle = new Bundle();
            bundle.putString("type", "1");
            f.setArguments(bundle);
            mFragmentList.add(f);
            return f;
        } else if (position == 2) {
            f = new TrendingFragmentLandscape();
            mFragmentList.add(f);
            return f;
        } else if (position == 3) {
            f = new SearchFragmentLandscape();
            mFragmentList.add(f);
            return f;
        } else {
            f = new MoreFragment();
            Bundle bundle = new Bundle();
            bundle.putString("ori_type", "" + AppConstant.LANDSCAPE);
            f.setArguments(bundle);
            mFragmentList.add(f);
            return f;
        }
    }
}
