package com.dingdongstatus.LandscapeFragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dingdongstatus.Interfaces.RvOnscrollListner;
import com.dingdongstatus.R;
import com.dingdongstatus.adapter.HomeAdapter1;
import com.dingdongstatus.model.Category;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.netutils.GlobalElements;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.EndlessRecyclerOnScrollListenerForStrageredView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragmentLandscape extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;
    @BindView(R.id.rel_screen_loder)
    RelativeLayout relScreenLoder;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    Unbinder unbinder;
    @BindView(R.id.fabGoToTop)
    FloatingActionButton fabGoToTop;
    @BindView(R.id.relative_layout_load_more)
    RelativeLayout relativeLayoutLoadMore;

    private int page = 1, total_rec = 0, limit = 0;

    private static final String ARG_SECTION_NUMBER = "section_number";
    private GridLayoutManager mLayoutManager;
    private HomeAdapter1 Adapter;
    private ArrayList<VideoModel> data = new ArrayList<>();
    private boolean isVisibleToUser;
    private boolean isScrolling = false;

    public interface LoadAd {
        public void load();
    }

    public HomeFragmentLandscape() {
        // Required empty public constructor
    }

    public static HomeFragmentLandscape newInstance(String tagName) {
        HomeFragmentLandscape fragment = new HomeFragmentLandscape();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_NUMBER, tagName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
//        getActivity().setTitle("Category");
        //todo rv

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 1;
                } else if ((position) % 7 == 0) {
                    return 1;
                } else {
                    return 1;
                }
            }
        });
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        Adapter = new HomeAdapter1(getActivity(), data, AppConstant.LANDSCAPE);
        recyclerView.setAdapter(Adapter);
        final RvOnscrollListner listner = (RvOnscrollListner) getContext();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        isScrolling = false;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!isScrolling)
                                    listner.show();
                            }
                        }, 1000);

                        System.out.println("The recyclerView is not scrolling");
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        isScrolling = true;
                        listner.hide();
                        System.out.println("Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
//                        listner.hide();
                        System.out.println("Scroll Settling");
                        break;

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        //todo swipe refresh
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
//                        getCategory();
                    }
                }
        );
        final RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {
            @Override
            protected int getVerticalSnapPreference() {
                fabGoToTop.setVisibility(fabGoToTop.GONE);
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        fabGoToTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                smoothScroller.setTargetPosition(0);
                mLayoutManager.startSmoothScroll(smoothScroller);
            }
        });
        setLoadMore();
        data.clear();
        getCategory();
        return view;
    }

    private void setLoadMore() {
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListenerForStrageredView(mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (total_rec > (limit * page)) {
                    if (isVisibleToUser) {
                        page += 1;
                        getCategory();
                    }
                }
            }

            @Override
            public void onFirstVisibleItem(int pos) {
                if (pos >= 3) {
                    fabGoToTop.setVisibility(fabGoToTop.VISIBLE);
                } else {
                    fabGoToTop.setVisibility(fabGoToTop.GONE);
                }
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        page=1;
        this.isVisibleToUser = isVisibleToUser;
    }

    private void getCategory() {
        if (data.size() == 0) {
            swipeRefreshLayout.setRefreshing(false);
        } else {
            relativeLayoutLoadMore.setVisibility(View.VISIBLE);
        }
//        RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);
        Call<ResponseBody> call = AppConstant.request.getLatestStatus(
                "statuslist",
                AppConstant.LANDSCAPE,
                page + ""
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (data.size() == 0) {
                    data.clear();
                } else {
                    data.remove(data.size() - 1);
                    relativeLayoutLoadMore.setVisibility(View.GONE);
                }
                try {
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    page = Integer.parseInt(json.getString("page"));
                    total_rec = Integer.parseInt(json.getString("total_rec"));
                    limit = Integer.parseInt(json.getString("limit"));
                    Category da = new Category();

                    if (json.getInt("success") == 1) {
                        JSONArray result = json.getJSONArray("data");
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject c = result.getJSONObject(i);
                            VideoModel modelOffers = new VideoModel();
                            modelOffers.setId("" + c.getString("id"));
                            modelOffers.setName("" + c.getString("name"));
                            modelOffers.setImgurl("" + c.getString("imgurl"));
                            modelOffers.setVideourl("" + c.getString("videourl"));
                            modelOffers.setTag("" + c.getString("tag"));
                            modelOffers.setDownloads("" + c.getString("downloads"));
                            modelOffers.setView("" + c.getString("view"));
                            modelOffers.setShare("" + c.getString("share"));
                            modelOffers.setStatus("" + c.getString("status"));
                            modelOffers.setUser_id("" + c.getString("user_id"));
                            modelOffers.setUser_name("" + c.getString("user_name"));
                            modelOffers.setUser_imgurl("" + c.getString("user_imgurl"));
                            modelOffers.setDatetime("" + c.getString("datetime"));
                            data.add(modelOffers);
                        }
                        Adapter.notifyDataSetChanged();
                    } else {
//                        Toaster.show(getActivity(), "" + json.getString("ack_msg"), false, Toaster.DANGER);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.print("error" + t.getMessage());
            }
        });
    }

    @Override
    public void onRefresh() {

        if (GlobalElements.isConnectingToInternet(getActivity())) {
            data.clear();
            page = 1;
            total_rec = 0;
            limit = 0;

            getCategory();
            setLoadMore();
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "No Network Present!!", Toast.LENGTH_SHORT).show();
        }

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
