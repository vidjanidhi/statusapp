package com.dingdongstatus.LandscapeFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dingdongstatus.R;
import com.dingdongstatus.adapter.LanguageAdapter;
import com.dingdongstatus.adapter.CategoryAdapter;
import com.dingdongstatus.model.Category;
import com.dingdongstatus.netutils.GlobalElements;
import com.dingdongstatus.support.AppConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragmentLandscape extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;
    @BindView(R.id.rel_screen_loder)
    RelativeLayout relScreenLoder;
    @BindView(R.id.rv_artist)
    RecyclerView rvArtist;
    @BindView(R.id.recycler_view)
    RecyclerView categoryRecyclerView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    Unbinder unbinder;

    private static final String ARG_SECTION_NUMBER = "section_number";

    private GridLayoutManager mLayoutManager;
    private LinearLayoutManager mLayoutManager1;
    private CategoryAdapter categoryAdapter;
    private LanguageAdapter languageAdapter;
    private ArrayList<Category> gsCategories = new ArrayList<>();
    private ArrayList<Category> artist_data = new ArrayList<>();

    public CategoryFragmentLandscape() {
        // Required empty public constructor
    }

    public static CategoryFragmentLandscape newInstance(String tagName) {
        CategoryFragmentLandscape fragment = new CategoryFragmentLandscape();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_NUMBER, tagName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        unbinder = ButterKnife.bind(this, view);
        ViewCompat.setNestedScrollingEnabled(rvArtist, false);
        ViewCompat.setNestedScrollingEnabled(categoryRecyclerView, false);

//        getActivity().setTitle("Category");
        gsCategories.clear();
        artist_data.clear();
        //todo category rv
        categoryRecyclerView.setHasFixedSize(false);
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        categoryRecyclerView.setLayoutManager(mLayoutManager);
        categoryAdapter = new CategoryAdapter(getActivity(), artist_data,AppConstant.LANDSCAPE);
        categoryRecyclerView.setAdapter(categoryAdapter);
        getartist();

        //todo swipe
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(false);
                                    }
                                }
        );

        //todo artist
        rvArtist.setHasFixedSize(false);
        mLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvArtist.setLayoutManager(mLayoutManager1);
        languageAdapter = new LanguageAdapter(getActivity(), gsCategories, AppConstant.LANDSCAPE);
        rvArtist.setAdapter(languageAdapter);
        getCategory();


//        serverRequest(URL);
//        categoryAdapter.notifyDataSetChanged();
        return view;
    }

    private void getartist() {
        swipeRefreshLayout.setRefreshing(false);
        relScreenLoder.setVisibility(RelativeLayout.VISIBLE);
       /* RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*/
        Call<ResponseBody> call = AppConstant.request.getArtist("artist");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                relScreenLoder.setVisibility(RelativeLayout.GONE);
                try {
                    artist_data.clear();
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    Category da = new Category();


                    if (json.getInt("success") == 1) {
                        JSONArray result = json.getJSONArray("data");
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject c = result.getJSONObject(i);
                            da = new Category();
                            da.setCategory_id("" + c.get("artist_id"));
                            da.setCategory_name("" + c.getString("artist_name"));
                            da.setCategory_image("" + c.getString("artist_image"));
                            da.setCategory_order("" + c.getString("artist_order"));
                            da.setCategory_status("" + c.getString("artist_status"));
                            artist_data.add(da);
                        }
                        categoryAdapter.notifyDataSetChanged();
                    } else {
//                        Toaster.show(getActivity(), "" + json.getString("ack_msg"), false, Toaster.DANGER);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                relScreenLoder.setVisibility(RelativeLayout.GONE);
                System.out.print("error" + t.getMessage());
            }
        });
    }

    private void getCategory() {
        swipeRefreshLayout.setRefreshing(false);
        relScreenLoder.setVisibility(RelativeLayout.VISIBLE);
       /* RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*/
        Call<ResponseBody> call = AppConstant.request.getCategory("cactegory");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                relScreenLoder.setVisibility(RelativeLayout.GONE);
                try {
                    gsCategories.clear();
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    Category da = new Category();


                    if (json.getInt("success") == 1) {
                        JSONArray result = json.getJSONArray("data");
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject c = result.getJSONObject(i);
                            da = new Category();
                            da.setCategory_id("" + c.get("category_id"));
                            da.setCategory_name("" + c.getString("category_name"));
                            da.setCategory_image("" + c.getString("category_image"));
                            da.setCategory_order("" + c.getString("category_order"));
                            da.setCategory_status("" + c.getString("category_status"));
                            gsCategories.add(da);
                        }
                        languageAdapter.notifyDataSetChanged();
                    } else {
//                        Toaster.show(getActivity(), "" + json.getString("ack_msg"), false, Toaster.DANGER);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                relScreenLoder.setVisibility(RelativeLayout.GONE);
                System.out.print("error" + t.getMessage());
            }
        });
    }

    @Override
    public void onRefresh() {

        if (GlobalElements.isConnectingToInternet(getActivity())) {
            gsCategories.clear();

            categoryAdapter.notifyDataSetChanged();
            getCategory();
            swipeRefreshLayout.setRefreshing(true);
//            isPullRefresh=true;
        } else {
            gsCategories.clear();
            categoryAdapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getActivity(), "No Network Present!!", Toast.LENGTH_SHORT).show();
        }

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
