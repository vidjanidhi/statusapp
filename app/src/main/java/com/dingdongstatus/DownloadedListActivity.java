package com.dingdongstatus;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dingdongstatus.database.Database;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.support.FbAds;
import com.dingdongstatus.support.SharingImage;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DownloadedListActivity extends AppCompatActivity {

    @BindView(R.id.txtMessage)
    TextView txtMessage;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.relNoFavouriteRecode)
    RelativeLayout relNoFavouriteRecode;
    @BindView(R.id.download_recycler_view)
    RecyclerView downloadRecyclerView;

    String from = "1"; // 1- download, 2-fav

    private static final String ARG_SECTION_NUMBER = "section_number";
    private ArrayList<VideoModel> videoListModel = new ArrayList<>();
    private DownloadVideoListAdapter downloadVideoListAdapter;
    private GridLayoutManager mLayoutManager;
    Database database;
    FbAds googleAds;
    private String ori_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloaded_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("ori_type")) {
            ori_type = getIntent().getExtras().getString("ori_type");
        }
        from = getIntent().getStringExtra("from");

        googleAds = new FbAds(this);
        database = new Database(this);
        videoListModel.clear();
        if (from.equals("1")) {
            getSupportActionBar().setTitle("Downloads");
            videoListModel.addAll(database.getAllDownloadedStatus());
        } else {
            getSupportActionBar().setTitle("Favorites");
            videoListModel.addAll(database.getAllFavStatus());
        }
        if (videoListModel.isEmpty()) {
            relNoFavouriteRecode.setVisibility(relNoFavouriteRecode.VISIBLE);
        } else {
            relNoFavouriteRecode.setVisibility(relNoFavouriteRecode.GONE);
        }
        downloadRecyclerView.setHasFixedSize(false);
        mLayoutManager = new GridLayoutManager(this, 2);
       /* mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 1;
                } else if ((position+1) % 7 == 0) {
                    return 2;
                } else {
                    return 1;
                }

            }
        });*/
        downloadRecyclerView.setLayoutManager(mLayoutManager);
        downloadVideoListAdapter = new DownloadVideoListAdapter(this, videoListModel);
        downloadRecyclerView.setAdapter(downloadVideoListAdapter);
    }

    public class DownloadVideoListAdapter extends RecyclerView.Adapter<DownloadVideoListAdapter.MainViewHolder> {
        private Context mContext;
        private ArrayList<VideoModel> videoModels;
        private final int VIEW_ITEM = 1;
        private final int VIEW_PROG = 0;
        SharingImage sharingImage;

        public DownloadVideoListAdapter(Context mContext, ArrayList<VideoModel> videoListArrayModel) {
            this.mContext = mContext;
            this.videoModels = videoListArrayModel;
            sharingImage = new SharingImage(mContext);
        }

        @Override
        public void onBindViewHolder(MainViewHolder holder, final int position) {

            if (holder.getItemViewType() == VIEW_ITEM) {

                VideoModel videoModel = videoModels.get(position);
                ItemViewHeader mholder = (ItemViewHeader) holder;

                mholder.txtVideoTitle.setText(videoModel.getName());

                String imgurl = videoModel.getImgurl();
                imgurl = imgurl.replace(" ", "%20");

                Picasso.with(mContext)
                        .load(getfilepath(imgurl) + "200x200_" + getfilename1(imgurl))
                        .placeholder(R.drawable.bg_default_album_art_empity)   // optional
                        .error(R.drawable.bg_default_album_art_empity)      // optional
                        .into(mholder.imgThumbVideo);

                Log.d("imgurlimgurl", "" + imgurl);

                mholder.layoutItemClicked.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        googleAds.caclulateIntersialAds();
                        Intent in = new Intent(mContext, VideoViewActivity.class);
                        in.putExtra("data", videoModels.get(position));
                        in.putExtra("position", position);
                        in.putExtra("ori_type", ori_type);
                        mContext.startActivity(in);
                    }
                });

                mholder.imgDeleteVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        googleAds.caclulateIntersialAds();
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DownloadedListActivity.this);
                        alertDialog.setTitle("Confirm Delete...");
                        alertDialog.setMessage("Are you sure you want delete this?");
                        alertDialog.setIcon(R.mipmap.ic_delete);
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                VideoModel model = (VideoModel) DownloadedListActivity.this.videoListModel.get(position);
                                if (from.equals("1")) {
                                    DownloadedListActivity.this.database.deleteDownloadStatus(model.getId());
                                } else {
                                    DownloadedListActivity.this.database.deleteFavouriteStatus(model.getId());
                                }
                                DownloadedListActivity.this.videoListModel.remove(position);
                                DownloadedListActivity.this.downloadVideoListAdapter.notifyDataSetChanged();
                                notifyItemRemoved(position);
                                String filename12 = getfilename(model.getVideourl());
                                String downloadImageFileName = "";

                                try {
                                    String _imagename_ = getfilename(model.getVideourl());

                                    String someFilepath = _imagename_;
                                    String extension = someFilepath.substring(someFilepath.lastIndexOf("."));
                                    downloadImageFileName = model.getName() + extension;
                                } catch (Exception e) {

                                }
                                final File despath = new File(Environment.getExternalStorageDirectory(),
                                        "Video/VideoStatus/" + downloadImageFileName);
                                if (despath != null && despath.exists()) {
                                    despath.delete();
                                }

                                if (videoListModel.size() == 0)
                                    relNoFavouriteRecode.setVisibility(relNoFavouriteRecode.VISIBLE);
                                else
                                    relNoFavouriteRecode.setVisibility(relNoFavouriteRecode.GONE);

                                notifyDataSetChanged();
                            }
                        });

                        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.show();

                    }
                });


                /*mholder.imgShareWhatsappListItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {
                            googleAds.caclulateIntersialAds();
                            VideoModel model = (VideoModel) DownloadedListActivity.this.videoListModel.get(position);
                            String filename12 = getfilename(model.getVideourl());
                            sharingImage.whatsappShare(ImageStorage.getDownloadedImage(filename12));
                        } catch (Exception e) {
                        }

                    }
                });
*/

            }

        }

        @Override
        public int getItemCount() {
            return videoModels.size();
        }

        @Override
        public int getItemViewType(int position) {
            return videoModels.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        }

        @Override
        public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            switch (viewType) {
                case VIEW_ITEM:
                    return new ItemViewHeader(LayoutInflater.from(mContext).inflate(R.layout.list_item_video, parent, false));
                case VIEW_PROG:
                    return new ProgressViewHolder(LayoutInflater.from(mContext).inflate(R.layout.spical_screen_progress, parent, false));
            }

            return null;
        }

        public class ItemViewHeader extends MainViewHolder {

            public TextView txtVideoTitle;
            ImageView imgThumbVideo, imgDeleteVideo;
            CardView layoutItemClicked;

            public ItemViewHeader(View view) {
                super(view);

                txtVideoTitle = (TextView) view.findViewById(R.id.txtVideoTitle);
                imgThumbVideo = (ImageView) view.findViewById(R.id.imgThumbVideo);
//                imgShareWhatsappListItem = (ImageView) view.findViewById(R.id.imgShareWhatsappListItem);
                imgDeleteVideo = (ImageView) view.findViewById(R.id.imgDeleteVideo);
                layoutItemClicked = (CardView) view.findViewById(R.id.layoutItemClicked);

//                imgShareWhatsappListItem.setVisibility(imgShareWhatsappListItem.VISIBLE);
                imgDeleteVideo.setVisibility(View.VISIBLE);
            }
        }

        public class ProgressViewHolder extends MainViewHolder {

            public ProgressBar progressBar;

            public ProgressViewHolder(View v) {
                super(v);

                progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

            }
        }

        public class MainViewHolder extends RecyclerView.ViewHolder {
            public MainViewHolder(View v) {
                super(v);
            }
        }

        String getfilename(String filename) {
            String fileName1 = "";
            if (filename.length() > 4) {
                try {
                    fileName1 = filename.substring(filename.lastIndexOf('/') + 1);
                } catch (Exception e) {

                }

            }

            return fileName1;
        }

        String getfilename1(String name) {
            String fname = "";

            try {
                String url = name;
                fname = url.substring(url.lastIndexOf('/') + 1);
            } catch (Exception e) {

            }

            return fname;
        }

        String getfilepath(String path) {
            String filepath = "";
            try {

                filepath = path.substring(0, path.lastIndexOf('/') + 1);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return filepath;

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
