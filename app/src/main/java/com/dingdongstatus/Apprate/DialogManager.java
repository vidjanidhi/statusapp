package com.dingdongstatus.Apprate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.dingdongstatus.R;

import static com.dingdongstatus.Apprate.IntentHelper.createIntentForAmazonAppstore;
import static com.dingdongstatus.Apprate.IntentHelper.createIntentForGooglePlay;
import static com.dingdongstatus.Apprate.PreferenceHelper.setAgreeShowDialog;
import static com.dingdongstatus.Apprate.PreferenceHelper.setRemindInterval;
import static com.dingdongstatus.Apprate.Utils.getDialogBuilder;


final class DialogManager {

    private DialogManager() {
    }

    static Dialog create(final Context context, final DialogOptions options) {
        AlertDialog.Builder builder = getDialogBuilder(context);
        builder.setMessage(options.getMessageText(context));

        if (options.shouldShowTitle()) builder.setTitle(options.getTitleText(context));

        builder.setCancelable(options.getCancelable());

        View view = options.getView();
        if (view != null) builder.setView(view);

        final OnClickButtonListener listener = options.getListener();

        builder.setPositiveButton(options.getPositiveText(context), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Intent intentToAppstore = options.getStoreType() == StoreType.GOOGLEPLAY ?
                        createIntentForGooglePlay(context) : createIntentForAmazonAppstore(context);
                context.startActivity(intentToAppstore);
                setAgreeShowDialog(context, false);
                if (listener != null) listener.onClickButton(which);
            }
        });

        if (options.shouldShowNeutralButton()) {
            builder.setNeutralButton(options.getNeutralText(context), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setRemindInterval(context);
                    if (listener != null) listener.onClickButton(which);
                }
            });
        }

        if (options.shouldShowNegativeButton()) {
            builder.setNegativeButton(options.getNegativeText(context), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setAgreeShowDialog(context, false);
                    if (listener != null) listener.onClickButton(which);
                }
            });
        }

        return builder.create();
    }

    static Dialog create_custom_dialog(final Activity context, final DialogOptions options) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = context.getLayoutInflater();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_rate_dialog);
        TextView tv_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView tv_desc = (TextView) dialog.findViewById(R.id.txt_dia);
        TextView tv_cancel_button = (TextView) dialog.findViewById(R.id.btn_no);
        TextView tv_rate_button = (TextView) dialog.findViewById(R.id.btn_yes);

        tv_title.setText(options.getTitleText(context));
        tv_desc.setText(options.getMessageText(context));
        tv_rate_button.setText(options.getPositiveText(context));
        tv_cancel_button.setText(options.getNegativeText(context));

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/app/Montserrat-Regular.ttf");
        Typeface typeface_bold = Typeface.createFromAsset(context.getAssets(), "fonts/app/Montserrat-Bold.ttf");
        tv_title.setTypeface(typeface_bold);
        tv_desc.setTypeface(typeface);
        tv_cancel_button.setTypeface(typeface);
        tv_rate_button.setTypeface(typeface);

        final OnClickButtonListener listener = options.getListener();
        tv_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAgreeShowDialog(context, false);
//                if (listener != null) listener.onClickButton(0);
                dialog.dismiss();
            }
        });
        tv_rate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intentToAppstore = options.getStoreType() == StoreType.GOOGLEPLAY ?
                        createIntentForGooglePlay(context) : createIntentForAmazonAppstore(context);
                context.startActivity(intentToAppstore);
                setAgreeShowDialog(context, false);
//                if (listener != null) listener.onClickButton(1);
            }
        });

        dialog.setCancelable(false);

        /*builder.setMessage(options.getMessageText(context));

        if (options.shouldShowTitle()) builder.setTitle(options.getTitleText(context));

        builder.setCancelable(options.getCancelable());

        View view = options.getView();
        if (view != null) builder.setView(view);



        builder.setPositiveButton(options.getPositiveText(context), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Intent intentToAppstore = options.getStoreType() == StoreType.GOOGLEPLAY ?
                        createIntentForGooglePlay(context) : createIntentForAmazonAppstore(context);
                context.startActivity(intentToAppstore);
                setAgreeShowDialog(context, false);
                if (listener != null) listener.onClickButton(which);
            }
        });

        if (options.shouldShowNeutralButton()) {
            builder.setNeutralButton(options.getNeutralText(context), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setRemindInterval(context);
                    if (listener != null) listener.onClickButton(which);
                }
            });
        }

        if (options.shouldShowNegativeButton()) {
            builder.setNegativeButton(options.getNegativeText(context), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setAgreeShowDialog(context, false);
                    if (listener != null) listener.onClickButton(which);
                }
            });
        }
*/
        return dialog;
    }

    static Dialog create_exit_dialog(final Activity context, final DialogOptions options) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = context.getLayoutInflater();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_rate_dialog);
        TextView tv_title = (TextView) dialog.findViewById(R.id.txt_title);
        TextView tv_desc = (TextView) dialog.findViewById(R.id.txt_dia);
        TextView tv_cancel_button = (TextView) dialog.findViewById(R.id.btn_no);
        TextView tv_rate_button = (TextView) dialog.findViewById(R.id.btn_yes);

        tv_title.setText(options.getTitleText(context));
        tv_desc.setText(options.getMessageText(context));
        tv_rate_button.setText(options.getPositiveText(context));
        tv_cancel_button.setText(options.getNegativeText(context));

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/app/Montserrat-Regular.ttf");
        Typeface typeface_bold = Typeface.createFromAsset(context.getAssets(), "fonts/app/Montserrat-Bold.ttf");
        tv_title.setTypeface(typeface_bold);
        tv_desc.setTypeface(typeface);
        tv_cancel_button.setTypeface(typeface);
        tv_rate_button.setTypeface(typeface);

        final OnClickButtonListener listener = options.getListener();
        tv_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAgreeShowDialog(context, false);
//                if (listener != null) listener.onClickButton(0);
                dialog.dismiss();
            }
        });
        tv_rate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intentToAppstore = options.getStoreType() == StoreType.GOOGLEPLAY ?
                        createIntentForGooglePlay(context) : createIntentForAmazonAppstore(context);
                context.startActivity(intentToAppstore);
                setAgreeShowDialog(context, false);
            }
        });

        dialog.setCancelable(false);

        return dialog;
    }

}