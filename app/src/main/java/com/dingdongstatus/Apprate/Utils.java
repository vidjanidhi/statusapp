package com.dingdongstatus.Apprate;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.widget.TextView;
import android.widget.Toast;

import com.dingdongstatus.MainActivity;
import com.dingdongstatus.R;
import com.dingdongstatus.database.Database;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.netutils.MyPreferences;
import com.dingdongstatus.other.Constants;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class Utils {

    private Utils() {
    }

    static boolean underHoneyComb() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB;
    }

    static boolean isLollipop() {
        return Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP || Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1;
    }

    static int getDialogTheme() {
        return isLollipop() ? R.style.CustomLollipopDialogStyle : 0;
    }

    @SuppressLint("NewApi")
    static AlertDialog.Builder getDialogBuilder(Context context) {
        if (underHoneyComb()) {
            return new AlertDialog.Builder(context);
        } else {
            return new AlertDialog.Builder(context, getDialogTheme());
        }
    }

    public static void setTypefaceRegular(Context context, TextView tv) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/app/Montserrat-Regular.ttf");
        tv.setTypeface(typeface);
    }

    public static Uri getVideoUrifromPath(String urlMain) {
        urlMain = urlMain.replace(" ", "%20");
        return Uri.parse(urlMain);
    }

    public static String getImageUriFromPath(String image_path) {
        if (!image_path.equals("")) {
            image_path = image_path.replace(" ", "%20");
            String filepath = "";
            String url = image_path;
            String fname = "";
            try {

                filepath = image_path.substring(0, image_path.lastIndexOf('/') + 1);
                fname = url.substring(url.lastIndexOf('/') + 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return filepath + fname;

        } else {
            return null;
        }


    }

    public static int getSoftButtonsBarHeight(Activity context) {
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            context.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    public static int getWndowHeight(Activity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        return height;
    }

    public static String getfilenameFromVideoURL(String filename) {
        String fileName1 = "";
        if (filename.length() > 4) {
            try {
                fileName1 = filename.substring(filename.lastIndexOf('/') + 1);
            } catch (Exception e) {

            }
        }
        return fileName1;
    }

    public static boolean checkifVideoExistsInFolder(String imagename, String name) {
        String _imagename_ = getfilenameFromVideoURL(imagename);
        if (!_imagename_.equals("")) {
            String extension = _imagename_.substring(_imagename_.lastIndexOf("."));
            String downloadImageFileName = name + extension;
            File file = getVideoFromStorage("/" + downloadImageFileName + "");
            return file.exists();
        } else {
            return false;
        }

    }

    public static boolean checkIfVideoAddedToFav(String id, Context context) {
        Database database = new Database(context);
        return database.checkFavourite(id);
    }

    public static void addToFav(VideoModel model, Context context) {
        Database database = new Database(context);
        if (database.checkFavourite(model.getId())) {
            database.deleteFavouriteStatus(model.getId());
        } else {
            database.addFavStatus(model);
        }
    }

    public static File getVideoFromStorage(String imagename) {
        File mediaImage = null;
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root);
            if (!myDir.exists())
                return null;
            mediaImage = new File(myDir.getPath() + "/Video/VideoStatus/" + imagename);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mediaImage;
    }

    public static String getFilenameWithExtension(String imagename, String name) {
        String _imagename_ = getfilenameFromVideoURL(imagename);
        String someFilepath = _imagename_;
        String extension = someFilepath.substring(someFilepath.lastIndexOf("."));
        return name + extension + "";
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    static String CHANNEL_ID = "Event";
    public static final int NOTIFICATION_ID = 1;

   /* public static void sendNotification(String title, String msg, String url, Context context) {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent;
        if (!Constants.pushCID.equals("0")) {
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("ispushnoti", true);
        } else if (url != null && !url.equals("")) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
        } else {
            intent = new Intent(context, MainActivity.class);
        }
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationChannel mChannel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "QuotesPush";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setAutoCancel(true)
                .setSound(uri)
                .setAutoCancel(true)
                .setLights(Color.RED, 800, 800)
                .setContentText(msg)
                .setChannelId(CHANNEL_ID);

        mBuilder.setSmallIcon(getNotificationIcon(mBuilder));
        try {
            mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        } catch (Exception e) {
            Toast.makeText(context, "errror large- " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        if (title.trim().isEmpty()) {
            mBuilder.setContentTitle(context.getString(R.string.app_name));
            mBuilder.setTicker(context.getString(R.string.app_name));
        } else {
            mBuilder.setContentTitle(title);
            mBuilder.setTicker(title);
        }

        if (url != null) {
            mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(getBitmapFromURL(url)));
        }

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }*/


    public static class generatePictureStyleNotification extends AsyncTask<String, Void, Bitmap> {

        private Context mContext;
        private String title, message, imageUrl;

        public generatePictureStyleNotification(Context context, String title, String message, String imageUrl) {
            super();
            this.mContext = context;
            this.title = title;
            this.message = message;
            this.imageUrl = imageUrl;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {
                URL url = new URL(this.imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);

            NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent intent;
            if (!Constants.pushCID.equals("0")) {
                intent = new Intent(mContext, MainActivity.class);
                intent.putExtra("ispushnoti", true);
            } else if (imageUrl != null && !imageUrl.equals("")) {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(imageUrl));
            } else {
                intent = new Intent(mContext, MainActivity.class);
            }
            PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationChannel mChannel;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                CharSequence name = "QuotesPush";// The user-visible name of the channel.
                int importance = NotificationManager.IMPORTANCE_HIGH;
                mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                mNotificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                    .setAutoCancel(true)
                    .setSound(uri)
                    .setAutoCancel(true)
                    .setLights(Color.RED, 800, 800)
                    .setContentText(message)
                    .setChannelId(CHANNEL_ID);

            mBuilder.setSmallIcon(getNotificationIcon(mBuilder));
            try {
                mBuilder.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher));
            } catch (Exception e) {
                Toast.makeText(mContext, "errror large- " + e.getMessage(), Toast.LENGTH_LONG).show();
            }

            if (title.trim().isEmpty()) {
                mBuilder.setContentTitle(mContext.getString(R.string.app_name));
                mBuilder.setTicker(mContext.getString(R.string.app_name));
            } else {
                mBuilder.setContentTitle(title);
                mBuilder.setTicker(title);
            }

            if (imageUrl != null) {
                mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(result));
            }

            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
    }


    public static int getColour() {
        return 0x8b5630;
    }

    public static int getNotificationIcon(NotificationCompat.Builder notificationBuilder) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(getColour());
            return R.drawable.ic_notication;
        } else {
            return R.mipmap.ic_launcher;
        }
    }

    public static boolean checkAdsDisplayTime(Context context) {
        MyPreferences myPreferences = new MyPreferences(context);
//        PrefManager prf = new PrefManager(activity.getApplicationContext());
        if (!myPreferences.getPreferences(MyPreferences.SUBSCRIBED).equalsIgnoreCase("FALSE")) {
            return false;
        }
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(c.getTime());

        if (myPreferences.getPreferences(MyPreferences.LAST_DATE_ADS).equals("")) {
            myPreferences.setPreferences(MyPreferences.LAST_DATE_ADS, strDate);
        } else {
            String toyBornTime = myPreferences.getPreferences(MyPreferences.LAST_DATE_ADS);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            try {
                Date oldDate = dateFormat.parse(toyBornTime);
                System.out.println(oldDate);

                Date currentDate = new Date();

                long diff = currentDate.getTime() - oldDate.getTime();
                long seconds = diff / 1000;

                if (seconds > Integer.parseInt(context.getResources().getString(R.string.AD_MOB_TIME))) {
                    myPreferences.setPreferences(MyPreferences.LAST_DATE_ADS, strDate);
                    return true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

}
