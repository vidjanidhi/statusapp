package com.dingdongstatus;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dingdongstatus.model.Category;
import com.dingdongstatus.model.VideoModel;
import com.facebook.ads.NativeAdLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.dingdongstatus.adapter.HomeAdapter;
import com.dingdongstatus.netutils.GlobalElements;
import com.dingdongstatus.netutils.MyPreferences;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.EndlessRecyclerOnScrollListenerForStrageredView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dingdongstatus.support.FbAds.showNativeAd;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.tv_follower_number)
    TextView tvFollowerNumber;
    @BindView(R.id.tv_following_number)
    TextView tvFollowingNumber;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    MyPreferences myPreferences;
    @BindView(R.id.tv_name)
    TextView tvName;

    String user_type = "", ori_type = "", user_id = "", image = "", name = "";
    @BindView(R.id.btn_edit_profile)
    Button btnEditProfile;
    private GridLayoutManager mLayoutManager;
    private HomeAdapter Adapter;
    private ArrayList<VideoModel> data = new ArrayList<>();
    private int page = 1, total_rec = 0, limit = 0;
    private Picasso picassoInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        picassoInstance = new Picasso.Builder(this).build();
        //todo fb ads
        NativeAdLayout native_banner_ad_container = (NativeAdLayout) findViewById(R.id.native_banner_ad_container);
        showNativeAd(this, native_banner_ad_container);
        myPreferences = new MyPreferences(this);
//        imgProfile.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));


        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("user_type")) {
            user_type = getIntent().getExtras().getString("user_type");
            if (user_type.equals(AppConstant.me)) {
                user_id = myPreferences.getPreferences(MyPreferences.id);
            } else {
                btnEditProfile.setVisibility(View.GONE);
                if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("user_id")) {
                    user_id = getIntent().getExtras().getString("user_id");
                }
                if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("image")) {
                    image = getIntent().getExtras().getString("image");
                    picassoInstance.load(image).fit().centerInside().into(imgProfile, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
//                            holderHeadar.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            imgProfile.setImageResource(R.drawable.ic_down);

                        }
                    });
                  /*  Glide.with(this).load(image)
                            .thumbnail(0.5f)
                            .crossFade()
                            .placeholder(R.drawable.image_roteate).error(R.drawable.image_roteate)
                            .into(imgProfile);*/
                }
                if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("name")) {
                    name = getIntent().getExtras().getString("name");
                }
            }
        }
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("ori_type")) {
            ori_type = getIntent().getExtras().getString("ori_type");
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (user_type.equals(AppConstant.user)) {
            getSupportActionBar().setTitle(name);
            tvName.setText(name);
        } else {
            getSupportActionBar().setTitle(myPreferences.getPreferences(MyPreferences.name));
            tvName.setText(myPreferences.getPreferences(MyPreferences.name));
        }
        setUserProfile();
        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddDialog();
            }


        });

        recyclerView.setHasFixedSize(true);
        if (ori_type.equals(AppConstant.PORTRAIT)) {
            mLayoutManager = new GridLayoutManager(this, 2);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position == 0) {
                        return 1;
                    } else if ((position + 1) % 7 == 0) {
                        return 2;
                    } else {
                        return 1;
                    }

                }
            });
        } else {
            mLayoutManager = new GridLayoutManager(this, 1);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position == 0) {
                        return 1;
                    } else if ((position + 1) % 7 == 0) {
                        return 1;
                    } else {
                        return 1;
                    }

                }
            });
        }

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        Adapter = new HomeAdapter(this, data, ori_type);
        recyclerView.setAdapter(Adapter);
        getStatus();

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListenerForStrageredView((GridLayoutManager) mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (total_rec > (limit * page)) {
                    page += 1;
                    getStatus();
                }
            }

            @Override
            public void onFirstVisibleItem(int pos) {

            }
        });

    }

    public void setUserProfile() {
        if (user_type.equals(AppConstant.me)) {
            if (!myPreferences.getPreferences(MyPreferences.id).equals("")) {
                if (myPreferences.getPreferences(MyPreferences.login_type).equals("1")) {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    tvName.setText(user.getDisplayName());
                    Glide.with(this).load(user.getPhotoUrl().toString())
                            .thumbnail(0.5f)
                            .crossFade()
                            .placeholder(R.mipmap.ic_launcher)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgProfile);

//                imgProfile.setImageURI(user.getPhotoUrl());
                } else if (myPreferences.getPreferences(MyPreferences.login_type).equals("3")) {
                    tvName.setText(myPreferences.getPreferences(MyPreferences.name));
                    Glide.with(this).load(myPreferences.getPreferences(MyPreferences.profile_image))
                            .thumbnail(0.5f)
                            .crossFade()
                            .placeholder(R.mipmap.ic_launcher)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgProfile);

                }
            }
        } else {
            try {
                if (!image.equals(""))
                    Glide.with(this).load(image)
                            .thumbnail(0.5f)
                            .crossFade()
                            .placeholder(R.mipmap.ic_launcher)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(imgProfile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getStatus() {
        if (data.size() == 0) {
        }
        /*RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*/
        Call<ResponseBody> call = AppConstant.request.getLatestStatus(
                "statuslist",
                "" + ori_type,
                "" + user_id,
                page + ""
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (data.size() == 0) {
                    data.clear();
                } else {
                    data.remove(data.size() - 1);
                }
                try {
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    page = Integer.parseInt(json.getString("page"));
                    total_rec = Integer.parseInt(json.getString("total_rec"));
                    limit = Integer.parseInt(json.getString("limit"));
                    Category da = new Category();

                    if (json.getInt("success") == 1) {
//                        LoadAd loadAd = (LoadAd) getContext();
//                        loadAd.load();
                        JSONArray result = json.getJSONArray("data");
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject c = result.getJSONObject(i);
                            VideoModel modelOffers = new VideoModel();
                            modelOffers.setId("" + c.getString("id"));
                            modelOffers.setName("" + c.getString("name"));
                            modelOffers.setImgurl("" + c.getString("imgurl"));
                            modelOffers.setVideourl("" + c.getString("videourl"));
                            modelOffers.setTag("" + c.getString("tag"));
                            modelOffers.setDownloads("" + c.getString("downloads"));
                            modelOffers.setView("" + c.getString("view"));
                            modelOffers.setShare("" + c.getString("share"));
                            modelOffers.setStatus("" + c.getString("status"));
                            modelOffers.setUser_id("" + c.getString("user_id"));
                            modelOffers.setUser_name("" + c.getString("user_name"));
                            modelOffers.setUser_imgurl("" + c.getString("user_imgurl"));
                            data.add(modelOffers);
                        }
                        Adapter.notifyDataSetChanged();
                    } else {
//                        Toaster.show(getActivity(), "" + json.getString("ack_msg"), false, Toaster.DANGER);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                relScreenLoder.setVisibility(relScreenLoder.GONE);
                System.out.print("error" + t.getMessage());
            }
        });
    }

    private void showAddDialog() {
        AlertDialog buildInfosDialog = null;
        final AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(ProfileActivity.this);
        alertDialog2.setTitle("Add Service");

        LayoutInflater inflater = ProfileActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_new_folder, null);
        alertDialog2.setView(dialogView);

        final EditText newFolderEdt = (EditText) dialogView.findViewById(R.id.new_folder_edt);
        newFolderEdt.setText(tvName.getText() + "");
        final AlertDialog finalBuildInfosDialog = buildInfosDialog;
        alertDialog2.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog2.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                    }
                });
        buildInfosDialog = alertDialog2.create();

        final AlertDialog finalBuildInfosDialog1 = buildInfosDialog;
        buildInfosDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) finalBuildInfosDialog1).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        if (GlobalElements.isConnectingToInternet(ProfileActivity.this)) {
                            if (newFolderEdt.getText().toString().equals("")) {
                                newFolderEdt.setError("Enter Service Master");
                            } else {
                                finalBuildInfosDialog1.dismiss();
                                getCategory("" + newFolderEdt.getText().toString());
                            }
                        } else {
                            GlobalElements.showDialog(ProfileActivity.this);
                        }
                        //Dismiss once everything is OK.

                    }
                });
            }
        });
        buildInfosDialog.show();
    }

    private void getCategory(final String name) {

        /*RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*//**/
        Call<ResponseBody> call = AppConstant.request.updateProfile(
                "updateprofile",
                ori_type + "",
                myPreferences.getPreferences(MyPreferences.id) + "",
                name + ""
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    if (json.getInt("success") == 1) {
                        Toast.makeText(ProfileActivity.this, "User Profile Updated successfully", Toast.LENGTH_SHORT).show();
                        tvName.setText(name);
                        myPreferences.setPreferences(MyPreferences.name, name);
                        getSupportActionBar().setTitle(name);

                    } else {
                        Toast.makeText(ProfileActivity.this, "User Profile Update Failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                relScreenLoder.setVisibility(relScreenLoder.GONE);
                System.out.print("error" + t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (user_type.equals(AppConstant.me))
            getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                //add the function to perform here
                myPreferences.clearPreferences();
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                return (true);

            case android.R.id.home:
                finish();
                break;
        }
        return (super.onOptionsItemSelected(item));
    }

}
