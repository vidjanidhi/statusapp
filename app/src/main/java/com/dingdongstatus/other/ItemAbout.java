package com.dingdongstatus.other;

import com.google.gson.annotations.SerializedName;

public class ItemAbout {

	@SerializedName("app_name")
 	private String appName = "";

	@SerializedName("app_logo")
	private String appLogo = "";

	@SerializedName("app_description")
	private String appDesc = "";

	@SerializedName("app_version")
	private String appVersion = "";

	@SerializedName("app_author")
	private String author = "";

	@SerializedName("app_contact")
	private String contact = "";

	@SerializedName("app_email")
	private String email = "";

	@SerializedName("app_website")
	private String website = "";

	@SerializedName("app_privacy_policy")
	private String privacy = "";

	@SerializedName("app_developed_by")
	private String developedBy = "";

	@SerializedName("publisher_id")
	private String publisherID = "";

	@SerializedName("interstital_ad_id")
	private String interstitalAdId = "";

	@SerializedName("banner_ad_id")
	private String bannerAdId = "";

	@SerializedName("interstital_ad")
	private String isInterstitialAd = "true";

	@SerializedName("banner_ad")
	private String isBannerAd = "true";

	@SerializedName("interstital_ad_click")
	private String click = "5";

	public ItemAbout(String appName, String appLogo, String appDesc, String appVersion, String author, String contact, String email, String website, String privacy, String developedBy, String publisherID, String interstitalAdId, String bannerAdId, String isInterstitialAd, String isBannerAd, String click) {
		this.appName = appName;
		this.appLogo = appLogo;
		this.appDesc = appDesc;
		this.appVersion = appVersion;
		this.author = author;
		this.contact = contact;
		this.email = email;
		this.website = website;
		this.privacy = privacy;
		this.developedBy = developedBy;
		this.publisherID = publisherID;
		this.interstitalAdId = interstitalAdId;
		this.bannerAdId = bannerAdId;
		this.isInterstitialAd = isInterstitialAd;
		this.isBannerAd = isBannerAd;
		this.click = click;
	}

	public String getAppName() {
		return appName;
	}

	public String getAppLogo() {
		return appLogo;
	}

	public String getAppDesc() {
		return appDesc;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public String getAuthor() {
		return author;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getPrivacy() {
		return privacy;
	}

	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}

	public String getDevelopedBy() {
		return developedBy;
	}

	public String getInterstitalAdId() {
		return interstitalAdId;
	}

	public String getBannerAdId() {
		return bannerAdId;
	}

	public String getIsInterstitialAd() {
		return isInterstitialAd;
	}

	public String getIsBannerAd() {
		return isBannerAd;
	}

	public String getPublisherID() {
		return publisherID;
	}

	public String getClick() {
		return click;
	}
}