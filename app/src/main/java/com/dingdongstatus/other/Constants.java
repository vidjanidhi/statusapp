package com.dingdongstatus.other;

import android.net.Uri;

import com.dingdongstatus.BuildConfig;

public class Constants {

    public static final String TAG_ROOT = "QUOTES_DIARY";

    private static String TAG_HOST_URL = BuildConfig.SERVER_URL;

    //this is the path of uploaded image of server where image store
    public static final String SERVER_IMAGE_UPFOLDER = TAG_HOST_URL + "/images/";

    public static final String URL_ABOUT_US_LOGO = TAG_HOST_URL + "images/";

//    public static APIInterface apiInterface;

    public static Boolean isUpdate = false;

    public static ItemAbout itemAbout;
    public static Boolean isLogged = false, isFromPush = false;
    public static Uri uri_setwall;
    public static String pushCID = "0";

    public static int adCount = 0;
}