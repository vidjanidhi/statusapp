package com.dingdongstatus.support;

import android.content.Context;

import com.dingdongstatus.R;
import com.dingdongstatus.netutils.MyPreferences;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class GoogleAds {
    static int count = 0;
    static int threadshote = 10;
    static int threadshoteReward = 15;
    Context context;
    InterstitialAd interstitial;
    MyPreferences myPreferences;
    RewardedVideoAd mRewardedVideoAd;
    public static final String ADMOB_AD_UNIT_ID = "ca-app-pub-2562193586583848/6534214146";
    public static final String ADMOB_APP_ID = "ca-app-pub-3940256099942544~3347511713";

    public GoogleAds(Context context) {
        this.context = context;
        myPreferences = new MyPreferences(context);
    }

    public void bannerAds(final AdView mAdView) {

        AdRequest adRequest = new AdRequest.Builder()
                //todo add test device id here
                .addTestDevice("8FA8F6BFAD9D3B6931C9B81BDA18A5F6")
                .build();


        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(mAdView.VISIBLE);

            }
        });

    }

    public void caclulateIntersialAds() {
        count = myPreferences.getIntPreferences(MyPreferences.adsclickecount);
        if (count == threadshote) {
            count = 0;
            myPreferences.setPreferences(MyPreferences.adsclickecount, "0");
            intersial();
        } else {
            count = count + 1;
            myPreferences.setPreferences(MyPreferences.adsclickecount, count + "");
        }
    }

    public void intersial() {
        interstitial = new InterstitialAd(context);
        interstitial.setAdUnitId(context.getString(R.string.admob_interstial_id));
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("8FA8F6BFAD9D3B6931C9B81BDA18A5F6")
                .build();
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial();
            }
        });
    }

    public void displayInterstitial() {

        if (interstitial.isLoaded()) {
            interstitial.show();

        }
    }

    public void caclulateReward() {
        count = myPreferences.getIntPreferences(MyPreferences.adsclickecountreward);
        if (count == threadshoteReward) {
            count = 0;
            myPreferences.setPreferences(MyPreferences.adsclickecountreward, "0");
            showRewarded();
        } else {
            count = count + 1;
            myPreferences.setPreferences(MyPreferences.adsclickecountreward, count + "");
        }
    }

    public void showRewarded() {

//        MobileAds.initialize(context, "ca-app-pub-3940256099942544~3347511713");//test
        MobileAds.initialize(context, "ca-app-pub-8478640474420921/5136832330");
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
        mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                displayReward();
            }

            @Override
            public void onRewardedVideoAdOpened() {

            }

            @Override
            public void onRewardedVideoStarted() {

            }

            @Override
            public void onRewardedVideoAdClosed() {

            }

            @Override
            public void onRewarded(RewardItem rewardItem) {

            }

            @Override
            public void onRewardedVideoAdLeftApplication() {

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {

            }

            @Override
            public void onRewardedVideoCompleted() {

            }


        });
        mRewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917",
                new AdRequest.Builder().build());
    }

    public void displayReward() {
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        }
    }
}
