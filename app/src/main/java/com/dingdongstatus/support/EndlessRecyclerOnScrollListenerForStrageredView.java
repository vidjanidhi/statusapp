package com.dingdongstatus.support;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

public abstract class EndlessRecyclerOnScrollListenerForStrageredView extends RecyclerView.OnScrollListener {
    public static String TAG = EndlessRecyclerOnScrollListener.class.getSimpleName();
    private int scrolledDistance = 0;
    private boolean controlsVisible = false;

    private boolean loading = true; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = 5; // The minimum amount of items to have below your current scroll position before loading more.

    private int pastVisibleItems, visibleItemCount, totalItemCount;

    private int current_page = 1;

    private int previousTotal = 0;
    int type = 1;

    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private GridLayoutManager gridLayoutManager;

    public EndlessRecyclerOnScrollListenerForStrageredView(StaggeredGridLayoutManager staggeredGridLayoutManager) {
        this.mStaggeredGridLayoutManager = staggeredGridLayoutManager;
        type = 1;

    }

    public EndlessRecyclerOnScrollListenerForStrageredView(GridLayoutManager staggeredGridLayoutManager1) {
        this.gridLayoutManager = staggeredGridLayoutManager1;
        type = 2;

    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        /*visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mStaggeredGridLayoutManager.getItemCount();
        //firstVisibleItem = mStaggeredGridLayoutManager.findFirstVisibleItemPosition();
        int[] firstVisibleItems = null;
        firstVisibleItems = mStaggeredGridLayoutManager.findFirstVisibleItemPositions(firstVisibleItems);
        if (firstVisibleItems != null && firstVisibleItems.length > 0) {
            pastVisibleItems = firstVisibleItems[0];
        }

       *//* if(visibleItemCount==0)
        {
            previousTotal = 0;
            loading = true;
        }*//*


        if (loading) {
            if (totalItemCount > previousTotal+1) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount)
                <= (pastVisibleItems + visibleThreshold)) {
            // End has been reached

            // Do something
            current_page++;

            onLoadMore(current_page);

            loading = true;
        }

        if (scrolledDistance > 1 && controlsVisible) {
            controlsVisible = false;
            scrolledDistance = 0;
        } else if (scrolledDistance < -1 && !controlsVisible) {
            controlsVisible = true;
            scrolledDistance = 0;
        }

        if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0)) {
            scrolledDistance += dy;
        }

        onFirstVisibleItem(pastVisibleItems);*/

        visibleItemCount = recyclerView.getChildCount();
        if (type == 1){
            totalItemCount = mStaggeredGridLayoutManager.getItemCount();
            int[] firstVisibleItems = null;
            firstVisibleItems = mStaggeredGridLayoutManager.findFirstVisibleItemPositions(firstVisibleItems);
            pastVisibleItems = firstVisibleItems[0];
        }else {
            totalItemCount = gridLayoutManager.getItemCount();
            int firstVisibleItems = 0;
            firstVisibleItems = gridLayoutManager.findFirstVisibleItemPosition();
            pastVisibleItems = firstVisibleItems;
        }
        if (visibleItemCount == 0) {
            previousTotal = 0;
            loading = true;
        }
        if (loading) {
            if (totalItemCount > previousTotal + 1) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (pastVisibleItems + visibleThreshold)) {
            // End has been reached
            // Do something
            current_page++;
            onLoadMore(current_page);
            loading = true;
        }
        onFirstVisibleItem(pastVisibleItems);
    }

    public abstract void onLoadMore(int current_page);

    public abstract void onFirstVisibleItem(int pos);


}