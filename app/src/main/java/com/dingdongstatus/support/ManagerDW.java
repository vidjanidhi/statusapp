package com.dingdongstatus.support;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;

import com.dingdongstatus.model.VideoModel;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;
import com.dingdongstatus.R;
import com.dingdongstatus.VideoViewActivity;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Jatin on 31/10/2017.
 */

public class ManagerDW {

    private static volatile ManagerDW Instance = null;
    public static DownloadStatusIndication downloadStatusIndicationInterface;
    public static RetryPolicy retryPolicy;
    public static ThinDownloadManager downloadManager;
    public int downloadId1;

    public static ManagerDW getInstance() {
        ManagerDW localInstance = Instance;
        if (localInstance == null) {
            synchronized (ManagerDW.class) {
                localInstance = Instance;
                if (localInstance == null) {
                    Instance = localInstance = new ManagerDW();
                    downloadManager = new ThinDownloadManager(10);
                    retryPolicy = new DefaultRetryPolicy();
                }
            }
        }
        return localInstance;
    }

    public interface DownloadStatusIndication {
        public void onDownloadUpdate(String id, String downloadid, int progress);

        public void onDownloadComplete(String id, String downloadid);
    }

    public static void setDownloadStatusIndicationInterface(DownloadStatusIndication playPushInterface) {
        ManagerDW.downloadStatusIndicationInterface = playPushInterface;
    }

    public void callDownload(final Context context, final String myID, String URL, final String downloadFileName, final VideoModel videoModel) {
        ImageStorage.createDir();

        DownloadFile d = new DownloadFile();
        DownloadRequest dr = d.DownloadFile(context, myID, URL, downloadFileName, videoModel);
        int a = downloadManager.add(dr);
        Preferance.saveDownloadFileValue(context, myID, a);

    }

    public class DownloadFile {
        public DownloadFile() {
        }

        public DownloadRequest DownloadFile(final Context context,
                                            final String myID,
                                            String URL,
                                            final String downloadFileName,
                                            final VideoModel videoModel) {

            final NotificationManager mNotifyManager;
            final NotificationCompat.Builder mBuilder;

            mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder = new NotificationCompat.Builder(context, "1");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String CHANNEL_ID = "my_channel_01";// The id of the channel.
                CharSequence name = context.getString(R.string.app_name);// The user-visible name of the channel.
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel("1", name, importance);
                mChannel.setSound(null, null);
                mChannel.enableVibration(false);
                mNotifyManager.createNotificationChannel(mChannel);
            } else {

            }
            mBuilder.setContentTitle(downloadFileName)
                    .setSound(null)
                    .setVibrate(null)
                    .setContentText("Download in progress")
                    .setSmallIcon(R.drawable.image_roteate);

            File filesDir = new File(Environment.getExternalStorageDirectory().getPath() + "/Video/VideoStatus/");
            Uri downloadUri = Uri.parse(URL);
            final Uri destinationUri = Uri.parse(filesDir + "/" + downloadFileName);
            final DownloadRequest downloadRequest1 = new DownloadRequest(downloadUri)
                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.LOW)
                    .setRetryPolicy(retryPolicy)
                    .setDownloadContext("Download1" + myID)
                    .setDownloadListener(new DownloadStatusListener() {
                        @Override
                        public void onDownloadComplete(int id) {
                            if (ManagerDW.downloadStatusIndicationInterface != null) {
                                ManagerDW.downloadStatusIndicationInterface.onDownloadComplete(myID, myID);
                            }
                            long when = System.currentTimeMillis();
                            Intent intent = new Intent(context, VideoViewActivity.class);
//                            Intent intent = new Intent(Intent.ACTION_VIEW, destinationUri);
//                            intent.setDataAndType(destinationUri, "video/mp4");
//                            intent.setAction(Intent.ACTION_VIEW);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.setData((Uri.parse("mystring" + when)));

                            ArrayList<VideoModel> data = new ArrayList<>();
                            data.add(videoModel);
                            intent.putExtra("data", videoModel);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            mNotifyManager.cancel(Integer.parseInt(videoModel.getId()));
                            PendingIntent Pintent = PendingIntent.getActivity(context, (int) when,
                                    intent, PendingIntent.FLAG_UPDATE_CURRENT);

                            NotificationCompat.Builder mBuilder2;
                            mBuilder2 = new NotificationCompat.Builder(context, "1");
                            mBuilder2.setContentTitle("" + downloadFileName)
                                    .setContentText("Download complete")
                                    .setSound(null)
                                    .setVibrate(null)
                                    .setSmallIcon(R.drawable.ic_notication)
                                    .setLargeIcon(BitmapFactory.decodeResource
                                            (context.getResources(),
                                                    R.mipmap.ic_launcher))
                                    .setContentIntent(Pintent)
                                    .setProgress(0, 0, false)
                                    .setAutoCancel(true);

                            NotificationManager mNotifyManager2 = (NotificationManager) context.getSystemService(
                                    Context.NOTIFICATION_SERVICE);

                            mNotifyManager2.notify(Integer.parseInt(videoModel.getId()),
                                    mBuilder2.build());

                        }

                        @Override
                        public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                            mNotifyManager.cancel(Integer.parseInt(videoModel.getId()));
                        }

                        @Override
                        public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {

                            if (ManagerDW.downloadStatusIndicationInterface != null) {
                                ManagerDW.downloadStatusIndicationInterface.onDownloadUpdate(myID, myID, progress);

                            }
                            mBuilder.setProgress(100, progress, false);
                            mNotifyManager.notify(Integer.parseInt(videoModel.getId()), mBuilder.build());

                        }
                    });
            ;
            return downloadRequest1;
        }
    }

    void cancelAllDownloadProcess() {
        if (downloadManager != null)
            downloadManager.cancelAll();
    }

    public void cancleSpecifiedFile(int adds) {
        if (downloadManager != null)
            downloadManager.cancel(adds);
    }
}
