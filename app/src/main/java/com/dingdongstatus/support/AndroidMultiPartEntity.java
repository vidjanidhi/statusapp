package com.dingdongstatus.support;

/**
 * Created by Matrix 10 on 14-04-2016.
 */
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

@SuppressWarnings("deprecation")
public class AndroidMultiPartEntity extends MultipartEntity

{

    private final ProgressListener listener;

    public AndroidMultiPartEntity(final ProgressListener listener) {
        super();
        this.listener = listener;
    }

    public AndroidMultiPartEntity(final HttpMultipartMode mode,
                                  final ProgressListener listener) {
        super(mode);
        this.listener = listener;
    }

    public AndroidMultiPartEntity(HttpMultipartMode mode, final String boundary,
                                  final Charset charset, final ProgressListener listener) {
        super(mode, boundary, charset);
        this.listener = listener;
    }



    @Override
    public void writeTo(final OutputStream outstream) throws IOException {
        super.writeTo(new CountingOutputStream(outstream, this.listener));
    }

    public static interface ProgressListener {
        void transferred(long num);
    }

    public static class CountingOutputStream extends FilterOutputStream {

        private final ProgressListener listener;
        private long transferred;

        public CountingOutputStream(final OutputStream out,
                                    final ProgressListener listener) {
            super(out);
            this.listener = listener;
            this.transferred = 0;
        }

        public void write(byte[] b, int off, int len) throws IOException {
            out.write(b, off, len);
            this.transferred += len;
            this.listener.transferred(this.transferred);
        }

        public void write(int b) throws IOException {
            out.write(b);
            this.transferred++;
            this.listener.transferred(this.transferred);
        }
    }

    /**
     * Created by Jatin on 03/07/2017.
     */





    public static class CommanMemoryInterface
    {

        private static String state_id_fooder ="";
        private static String city_id_fooder ="";


        private static volatile CommanMemoryInterface Instance = null;

        public static CommanMemoryInterface getInstance() {
            CommanMemoryInterface localInstance = Instance;
            if (localInstance == null) {
                synchronized (CommanMemoryInterface.class) {
                    localInstance = Instance;
                    if (localInstance == null) {
                        Instance = localInstance = new CommanMemoryInterface();
                    }
                }
            }
            return localInstance;
        }



    }
}