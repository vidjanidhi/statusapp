package com.dingdongstatus.support;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.dingdongstatus.BuildConfig;
import com.dingdongstatus.model.VideoModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 01-10-2016.
 */

public class SharingImage {
    Context context;
    File source;
    String filenametext;


    public SharingImage(Context context) {
        this.context = context;
    }


    public File inilizeOnShare(VideoModel currentData) {
        File despath = null;

        Intent share = new Intent(Intent.ACTION_SEND);
        String fileName = currentData.getVideourl();
        //filenametext=currentData.getId();
        String fn = currentData.getVideourl().substring(0, currentData.getVideourl().lastIndexOf('.'));
        fn = fn.replace(" ", "%20");
        if (ImageStorage.checkifImageExists(fn)) {
            source = new File(Environment.getExternalStorageDirectory(), "Video/VideoStatus/image_catch/" + fn);
            despath = new File(Environment.getExternalStorageDirectory(), "Video/VideoStatus/image_catch/" + fn + ".mp4");

            try {
                fileCopy(source, despath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Log.d("SOURCE", "" + source);
        Log.d("despath", "" + despath);

        return despath;
    }


    public void generalShare(File despath) {

        try {

            File imageFileToShare = new File(despath.toString());
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", imageFileToShare);
            Intent share = new Intent(Intent.ACTION_SEND);
            share.putExtra(Intent.EXTRA_STREAM, uri);
            share.setType("video/mp4");
            share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(share);


        } catch (Exception e) {
        }


    }


    public void whatsappShare(File despath) {
        try {
            File imageFileToShare = new File(despath.toString());
            if (imageFileToShare.exists()) {
                Log.i("filename", imageFileToShare.getPath() + "");
            } else {
                Log.i("filename", "" + "file not exist");
            }

            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", imageFileToShare);
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.setType("video/mp4");
            whatsappIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            whatsappIntent.putExtra(Intent.EXTRA_STREAM, uri);
            try {
                context.startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(context, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hikeShare(File despath) {

        try {

            File imageFileToShare = new File(despath.toString());
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", imageFileToShare);
            Intent hikeIntent = new Intent(Intent.ACTION_SEND);
            hikeIntent.setType("video/mp4");
            hikeIntent.setPackage("com.bsb.hike");
            hikeIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            hikeIntent.putExtra(Intent.EXTRA_STREAM, uri);
            try {
                context.startActivity(hikeIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(context, "Hike Messanger have not been installed.", Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {
        }


    }


    public void instagramShare(File despath) {
        try {
            File imageFileToShare = new File(despath.toString());
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", imageFileToShare);
            Intent hikeIntent = new Intent(Intent.ACTION_SEND);
            hikeIntent.setType("video/mp4");
            hikeIntent.setPackage("com.instagram.android");
            hikeIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            hikeIntent.putExtra(Intent.EXTRA_STREAM, uri);
            try {
                context.startActivity(hikeIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(context, "Instagram have not been installed.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void fbmessagnerShare(File despath) {
        try {
            File imageFileToShare = new File(despath.toString());
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", imageFileToShare);
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("video/mp4");
            sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
            sendIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            sendIntent.setPackage("com.facebook.orca");
            try {
                context.startActivity(sendIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(context, "Please Install Facebook Messenger", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
        }

    }


    public void facebookShare(File despath) {
        try {
            File imageFileToShare = new File(despath.toString());
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", imageFileToShare);
            List<Intent> targetedShareIntents = new ArrayList<Intent>();
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("video/mp4");
            List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(share, 0);
            if (!resInfo.isEmpty()) {
                for (ResolveInfo info : resInfo) {
                    Intent targetedShare = new Intent(Intent.ACTION_SEND);
                    targetedShare.setType("video/*");
                    if (info.activityInfo.packageName.toLowerCase().contains("com.facebook.katana") || info.activityInfo.name.toLowerCase().contains("com.facebook.katana")) {
                        targetedShare.putExtra(Intent.EXTRA_STREAM, uri);

                        targetedShare.setPackage(info.activityInfo.packageName);
                        targetedShareIntents.add(targetedShare);
                    }
                }
                Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
                context.startActivity(chooserIntent);
            }
        } catch (Exception e) {
        }

    }


    public void fileCopy(File src, File dst) throws IOException {
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (Exception e) {
        }
    }


}
