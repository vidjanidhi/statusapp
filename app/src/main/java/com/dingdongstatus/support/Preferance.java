package com.dingdongstatus.support;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Preferance {

    public static SharedPreferences getPreferanse(Context context) {
        return context.getSharedPreferences("PANDEVIDEOSTATUS", Activity.MODE_PRIVATE);
    }

    public static void saveAdsClickCount(Context context, int positon) {
        SharedPreferences.Editor editor = getPreferanse(context).edit();
        editor.putInt("adsclickecount", positon);
        editor.commit();
    }

    public static int getAdsClickCount(Context context) {
        SharedPreferences mSharedPreferences = getPreferanse(context);
        return mSharedPreferences.getInt("adsclickecount", 0);
    }

    public static void saveDownloadFileValue(Context context, String para, int positon) {
        SharedPreferences.Editor editor = getPreferanse(context).edit();
        editor.putInt("DOWN" + para, positon);
        editor.commit();
    }

    public static int getDownloadFileValue(Context context, String para) {
        SharedPreferences mSharedPreferences = getPreferanse(context);
        return mSharedPreferences.getInt("DOWN" + para, 0);
    }

}