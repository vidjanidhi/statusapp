package com.dingdongstatus.support;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;

public class ImageStorage {

    public static void createDir()
    {
        File sdcard = Environment.getExternalStorageDirectory() ;
        File folder = new File(sdcard.getAbsoluteFile(), "Video");//the dot makes this directory hidden to the user
        folder.mkdir();

        folder = new File(sdcard.getAbsoluteFile(), "Video/VideoStatus");//the dot makes this directory hidden to the user
        folder.mkdir();

        folder = new File(sdcard.getAbsoluteFile(), "Video/VideoStatus");//the dot makes this directory hidden to the user
        folder.mkdir();

    }



    public static File getImage(String imagename) {

        File mediaImage = null;
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root);
            if (!myDir.exists())
                return null;

            mediaImage = new File(myDir.getPath() + "Video/VideoStatus/image_catch/"+imagename);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mediaImage;
    }
    public static boolean checkifImageExists(String imagename)
    {
        Bitmap b = null ;
        File file = ImageStorage.getImage("/"+imagename+"");
        String path = file.getAbsolutePath();

        if (path != null)

            try
            {
                b = BitmapFactory.decodeFile(path);
            }
            catch (Exception e)
            {

            }

        if(b == null ||  b.equals(""))
        {
            return false ;
        }
        return true ;
    }


    public static File getDownloadedImage(String imagename) {

        File mediaImage = null;
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root);
            if (!myDir.exists())
                return null;

            mediaImage = new File(myDir.getPath() + "/Video/VideoStatus/"+imagename);


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mediaImage;
    }


    public static boolean checkifImageExistsInDownload(String imagename)
    {
        Bitmap b = null ;
        File file = ImageStorage.getDownloadedImage("/"+imagename+"");
        return file.exists() ;
    }
}