package com.dingdongstatus;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.dingdongstatus.adapter.HomeAdapter;
import com.dingdongstatus.model.Category;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.EndlessRecyclerOnScrollListenerForStrageredView;
import com.dingdongstatus.support.FbAds;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchViewActivity extends AppCompatActivity {

    private com.google.android.gms.ads.AdView mAdView;
    private FbAds googleAds;

    RelativeLayout rel_screen_loder;
    private ArrayList<VideoModel> data = new ArrayList<>();
    private RecyclerView search_recycler_view;
    private HomeAdapter videoListAdapter;
    private int page = 1, total_rec = 0, limit = 0;
    private GridLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAdView = (com.google.android.gms.ads.AdView) findViewById(R.id.adView);
        googleAds = new FbAds(getApplicationContext());

        ActionBar ab = getSupportActionBar();
        ab.setTitle(getIntent().getStringExtra("searchtext"));
        ab.setTitle(Html.fromHtml("<font color=\"#ffffff\">" + getIntent().getStringExtra("searchtext") + "</font>"));
        ab.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rel_screen_loder = (RelativeLayout) findViewById(R.id.rel_screen_loder);
       /* fixUrl = SEARCHSTATUS+"&tag="+getIntent().getStringExtra("searchtext");
        URL = SEARCHSTATUS+"&tag="+getIntent().getStringExtra("searchtext");*/
        search_recycler_view = (RecyclerView) findViewById(R.id.search_recycler_view);
        search_recycler_view.setHasFixedSize(false);
        mLayoutManager = new GridLayoutManager(this, 2);
       /* mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 1;
                } else if ((position+1) % 7 == 0) {
                    return 2;
                } else {
                    return 1;
                }

            }
        });*/
        search_recycler_view.setLayoutManager(mLayoutManager);
        videoListAdapter = new HomeAdapter(SearchViewActivity.this, data, AppConstant.LANDSCAPE);
        search_recycler_view.setAdapter(videoListAdapter);
        search_recycler_view.addOnScrollListener(new EndlessRecyclerOnScrollListenerForStrageredView(
                (GridLayoutManager) mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (total_rec > (limit * page)) {
                    page += 1;
                    getCategory();
                }
            }

            @Override
            public void onFirstVisibleItem(int pos) {
               /* if (pos >= 3) {
                    fabGoToTop.setVisibility(fabGoToTop.VISIBLE);
                } else {
                    fabGoToTop.setVisibility(fabGoToTop.GONE);
                }*/
            }
        });

        getCategory();
        videoListAdapter.notifyDataSetChanged();

    }

    private void getCategory() {
        if (data.size() == 0) {
            rel_screen_loder.setVisibility(View.VISIBLE);
        }
        /*RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*/
        Call<ResponseBody> call = AppConstant.request.getSearchStatus(
                "searchstatus",
                page + "",
                "1",
                getIntent().getStringExtra("searchtext") + ""

        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                rel_screen_loder.setVisibility(View.GONE);
                if (data.size() == 0) {
                    rel_screen_loder.setVisibility(View.GONE);
                    data.clear();
                } else {
                    data.remove(data.size() - 1);
                }
                try {
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    page = Integer.parseInt(json.getString("page"));
                    total_rec = Integer.parseInt(json.getString("total_rec"));
                    limit = Integer.parseInt(json.getString("limit"));
                    Category da = new Category();


                    if (json.getInt("success") == 1) {
                        JSONArray result = json.getJSONArray("data");
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject c = result.getJSONObject(i);
                            VideoModel modelOffers = new VideoModel();
                            modelOffers.setId("" + c.getString("id"));
                            modelOffers.setName("" + c.getString("name"));
                            modelOffers.setImgurl("" + c.getString("imgurl"));
                            modelOffers.setVideourl("" + c.getString("videourl"));
                            modelOffers.setTag("" + c.getString("tag"));
                            modelOffers.setDownloads("" + c.getString("downloads"));
                            modelOffers.setView("" + c.getString("view"));
                            modelOffers.setShare("" + c.getString("share"));
                            modelOffers.setStatus("" + c.getString("status"));
                            modelOffers.setDatetime("" + c.getString("datetime"));
                            data.add(modelOffers);
                        }
                        videoListAdapter.notifyDataSetChanged();
                    } else {
//                        Toaster.show(getActivity(), "" + json.getString("ack_msg"), false, Toaster.DANGER);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                rel_screen_loder.setVisibility(View.GONE);
                System.out.print("error" + t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) SearchViewActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
