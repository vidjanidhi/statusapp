package com.dingdongstatus;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dingdongstatus.model.Category;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.support.FbAds;
import com.facebook.ads.NativeAdLayout;
import com.google.android.gms.ads.AdView;
import com.dingdongstatus.adapter.HomeAdapter;
import com.dingdongstatus.netutils.GlobalElements;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.EndlessRecyclerOnScrollListenerForStrageredView;
import com.dingdongstatus.support.NpaGridLayoutManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dingdongstatus.support.FbAds.showNativeAd;

public class CategoryListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;
    @BindView(R.id.rel_screen_loder)
    RelativeLayout relScreenLoder;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    Unbinder unbinder;
    @BindView(R.id.fabGoToTop)
    FloatingActionButton fabGoToTop;


    @BindView(R.id.adView)
    AdView adView;
    @BindView(R.id.ad_container)
    LinearLayout adContainer;

    private int page = 1, total_rec = 0, limit = 0;

    private static final String ARG_SECTION_NUMBER = "section_number";
    private NpaGridLayoutManager mLayoutManager;
    private HomeAdapter Adapter;
    private ArrayList<VideoModel> data = new ArrayList<>();


    private FbAds googleAds;
    private String catid = "", catname = "";
    private String type = "1", ori_type = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        ButterKnife.bind(this);
        //todo fb ads
        NativeAdLayout native_banner_ad_container = (NativeAdLayout) findViewById(R.id.native_banner_ad_container);
        showNativeAd(this, native_banner_ad_container);
       /* MobileAds.initialize(this, GoogleAds.ADMOB_APP_ID);
        FrameLayout frameLayout =
                findViewById(R.id.fl_adplaceholder);
        GoogleAds.refreshAd(this, frameLayout);*/
        adView = (AdView) findViewById(R.id.adView);
        googleAds = new FbAds(getApplicationContext());


        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("catid")) {
            catid = getIntent().getExtras().getString("catid");
        }
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("type")) {
            type = getIntent().getExtras().getString("type");
        }
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("ori_type")) {
            ori_type = getIntent().getExtras().getString("ori_type");
        }

        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("catname")) {
            catname = getIntent().getExtras().getString("catname");
            getSupportActionBar().setTitle(catname);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //todo rv
        recyclerView.setHasFixedSize(false);
        if (ori_type.equals(AppConstant.PORTRAIT)){
            mLayoutManager = new NpaGridLayoutManager(this, 2);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position == 0) {
                        return 1;
                    } else if ((position+1) % 7 == 0) {
                        return 2;
                    } else {
                        return 1;
                    }

                }
            });
        }

        else{
            mLayoutManager = new NpaGridLayoutManager(this, 1);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position == 0) {
                        return 1;
                    } else if ((position + 1) % 7 == 0) {
                        return 1;
                    } else {
                        return 1;
                    }

                }
            });
        }

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        Adapter = new HomeAdapter(this, data, ori_type);
        recyclerView.setItemAnimator(null);
        recyclerView.setAdapter(Adapter);
        //todo swipe refresh
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );
        final RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(this) {
            @Override
            protected int getVerticalSnapPreference() {
                fabGoToTop.setVisibility(fabGoToTop.GONE);
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        fabGoToTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                smoothScroller.setTargetPosition(0);
                mLayoutManager.startSmoothScroll(smoothScroller);
            }
        });

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListenerForStrageredView((GridLayoutManager) mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (total_rec > (limit * page)) {
                    page += 1;
                    getCategory();
                }
            }

            @Override
            public void onFirstVisibleItem(int pos) {
                if (pos >= 3) {
                    fabGoToTop.setVisibility(fabGoToTop.VISIBLE);
                } else {
                    fabGoToTop.setVisibility(fabGoToTop.GONE);
                }
            }
        });

        data.clear();
        getCategory();
    }

    private void getCategory() {
        if (data.size() == 0) {
            swipeRefreshLayout.setRefreshing(false);
            relScreenLoder.setVisibility(View.VISIBLE);
        }
       /* RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*/
        Call<ResponseBody> call = null;
        if (type.equals("2")) {
            call = AppConstant.request.getStatusListFromCat(
                    "statuslist",
                    catid + "",
                    "" + ori_type,
                    page + ""
            );
        } else {
            call = AppConstant.request.getStatusListFromArtist(
                    "statuslist",
                    catid + "",
                    "" + ori_type,
                    page + ""
            );
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                relScreenLoder.setVisibility(View.GONE);
                try {
                    if (data.size() == 0) {
                        swipeRefreshLayout.setRefreshing(false);
                        relScreenLoder.setVisibility(View.GONE);
                        data.clear();
                    } else {
                        data.remove(data.size() - 1);
                    }
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
                    page = Integer.parseInt(json.getString("page"));
                    total_rec = Integer.parseInt(json.getString("total_rec"));
                    limit = Integer.parseInt(json.getString("limit"));
                    Category da = new Category();
                    if (json.getInt("success") == 1) {
                        JSONArray result = json.getJSONArray("data");
                        ArrayList<VideoModel> tempData = new ArrayList<VideoModel>();
                        tempData = data;
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject c = result.getJSONObject(i);
                            VideoModel modelOffers = new VideoModel();
                            modelOffers.setId("" + c.getString("id"));
                            modelOffers.setName("" + c.getString("name"));
                            modelOffers.setImgurl("" + c.getString("imgurl"));
                            modelOffers.setVideourl("" + c.getString("videourl"));
                            modelOffers.setTag("" + c.getString("tag"));
                            modelOffers.setDownloads("" + c.getString("downloads"));
                            modelOffers.setView("" + c.getString("view"));
                            modelOffers.setShare("" + c.getString("share"));
                            modelOffers.setStatus("" + c.getString("status"));
                            modelOffers.setDatetime("" + c.getString("datetime"));
                            modelOffers.setUser_id("" + c.getString("user_id"));
                            modelOffers.setUser_name("" + c.getString("user_name"));
                            modelOffers.setUser_imgurl("" + c.getString("user_imgurl"));
                            tempData.add(modelOffers);

                        }
//                        data.addAll(tempData);
                        data = tempData;
                        Adapter.notifyDataSetChanged();
                        if (data.isEmpty()) {
                            Toast.makeText(CategoryListActivity.this, "No record found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        Toaster.show(getActivity(), "" + json.getString("ack_msg"), false, Toaster.DANGER);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                relScreenLoder.setVisibility(View.GONE);
                System.out.print("error" + t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {

        if (GlobalElements.isConnectingToInternet(this)) {
            data.clear();
            Adapter.notifyDataSetChanged();
            page = 1;
            total_rec = 0;
            limit = 0;

            getCategory();
            swipeRefreshLayout.setRefreshing(true);
//            isPullRefresh=true;
        } else {
//            gsCategories.clear();
            swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(this, "No Network Present!!", Toast.LENGTH_SHORT).show();
        }

        swipeRefreshLayout.setRefreshing(false);

    }


}
