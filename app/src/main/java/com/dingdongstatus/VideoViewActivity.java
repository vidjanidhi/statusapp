package com.dingdongstatus;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.dingdongstatus.Apprate.Utils;
import com.dingdongstatus.database.Database;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.netutils.GlobalElements;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.FbAds;
import com.dingdongstatus.support.ImageStorage;
import com.dingdongstatus.support.ManagerDW;
import com.dingdongstatus.support.Preferance;
import com.dingdongstatus.support.SharingImage;
import com.dingdongstatus.support.SingleMediaScanner;
import com.facebook.ads.NativeAdLayout;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dingdongstatus.support.FbAds.showNativeAd;

public class VideoViewActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<VideoModel> data = new ArrayList<>();
    VideoModel model = new VideoModel();
    int position = 0, size = 0;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.video_view)
    VideoView videoView;
    @BindView(R.id.imageViewThumbDetails)
    ImageView imageViewThumbDetails;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_report)
    ImageView imgReport;
    @BindView(R.id.imgPlayIco)
    ImageView imgPlayIco;
    @BindView(R.id.imgFavourite)
    ImageView imgFavourite;
    @BindView(R.id.imgShareAction)
    ImageView imgShareAction;
    @BindView(R.id.imgDownloadAction)
    ImageView imgDownloadAction;
    @BindView(R.id.layoutShare)
    LinearLayout layoutShare;

    @BindView(R.id.img_user)
    CircleImageView imgUser;
    @BindView(R.id.progressBarDownloadNumber)
    NumberProgressBar progressBarDownloadNumber;
    @BindView(R.id.imgCancelDownload)
    ImageView imgCancelDownload;
    @BindView(R.id.layoutProgress)
    LinearLayout layoutProgress;

    @BindView(R.id.ad_container)
    LinearLayout adContainer;
    @BindView(R.id.imgShareWhatsandroid)
    ImageView imgShareWhatsandroid;
    @BindView(R.id.tv_status_name)
    TextView tvStatusName;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_downloads)
    TextView tvDownloads;

    private FbAds googleAds;
    int play_control_height = 200;
    Database database;
    boolean issaved = false;
    private String imageAction = "";
    SharingImage sharingImage;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 3;
    private String downloadImageFileName = "";
    private boolean playVisible = false;
    String ori_type = "";

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);
        ButterKnife.bind(this);
        //todo fb ads
        mInterstitialAd = new InterstitialAd(getApplication());
        mInterstitialAd.setAdUnitId(getString(R.string.admob_interstial_id));
        requestNewInterstitial();


        googleAds = new FbAds(this);
        database = new Database(this);
        sharingImage = new SharingImage(VideoViewActivity.this);
        NativeAdLayout native_banner_ad_container = (NativeAdLayout) findViewById(R.id.native_banner_ad_container);
        showNativeAd(this, native_banner_ad_container);

        initUI();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private void setupUI() {
        tvStatusName.setText(model.getName() + "");
        tvUsername.setText(model.getUser_name() + "");
        tvDownloads.setText(model.getDownloads() + "");

        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorAccent));
        imgFavourite.setImageDrawable(drawable);


        Utils.setTypefaceRegular(this, tvStatusName);
        Utils.setTypefaceRegular(this, tvUsername);
        Utils.setTypefaceRegular(this, tvDownloads);
        if (!model.getUser_imgurl().equals("")) {
            Picasso.with(this)
                    .load(model.getUser_imgurl())
                    .placeholder(R.drawable.bg_default_album_art_empity)
                    .error(R.drawable.bg_default_album_art_empity)
                    .into(imgUser);
        }

        videoUpdatemanager();
        setDownloadImageUI();
        setFavouriteImageUI();
        setLayoutParams();
        addToView("viewstatus");
        playVideo();
    }

    private void initUI() {
        model = (VideoModel) getIntent().getSerializableExtra("data");
        position = getIntent().getIntExtra("position", 0);
        if (this.getIntent().getExtras() != null && this.getIntent().getExtras().containsKey("ori_type")) {
            ori_type = getIntent().getExtras().getString("ori_type");
            if (ori_type.equals(AppConstant.LANDSCAPE)) {
                Intent i = new Intent(this, VideoDescActivity.class);
                i.putExtra("data", model);
                i.putExtra("ori_type", ori_type);
                startActivity(i);
                finish();
            }
        }

        try {
            String _imagename_ = getfilename(model.getVideourl());
            String someFilepath = _imagename_;
            String extension = someFilepath.substring(someFilepath.lastIndexOf("."));
            downloadImageFileName = model.getName() + extension;
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgFavourite.setOnClickListener(this);
        imgDownloadAction.setOnClickListener(this);
        imgCancelDownload.setOnClickListener(this);
        imgShareAction.setOnClickListener(this);
        imgPlayIco.setOnClickListener(this);
        videoView.setOnClickListener(this);
        imgShareWhatsandroid.setOnClickListener(this);
        imgUser.setOnClickListener(this);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imgReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showReportDialog();
            }
        });


        setupUI();
    }

    private void addToView(String req) {
        try {
            final ProgressDialog pd = new ProgressDialog(VideoViewActivity.this);
            pd.setTitle("Please Wait");
            pd.setMessage("Loading");
            pd.setCancelable(true);
//            pd.show();
            /*RequestInterface request = RetrofitClient.getClient().create(RequestInterface.class);*/
            Call<ResponseBody> call = AppConstant.request.view(req,
                    "" + model.getId()
            );

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
//                        pd.dismiss();
                        String jsonst = response.body().string();
                        JSONObject json = new JSONObject(jsonst);
                        if (json.getInt("sucess") == 1) {
//                            Toast.makeText(VideoViewActivity.this, "Report Sent", Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(getApplicationContext(), "" + json.getString("ack_msg"), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    pd.dismiss();
                    System.out.print("error" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPlayButton() {
        if (videoView.isPlaying()) {
            imgPlayIco.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_circle_filled_black_48dp));
        } else {
            imgPlayIco.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_circle_filled_black_48dp));

        }
    }

    String getfilename(String filename) {
        String fileName1 = "";
        if (filename.length() > 4) {
            try {
                fileName1 = filename.substring(filename.lastIndexOf('/') + 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return fileName1;
    }

    void videoUpdatemanager() {
        ManagerDW md = ManagerDW.getInstance();
        md.setDownloadStatusIndicationInterface(new ManagerDW.DownloadStatusIndication() {
            @Override
            public void onDownloadUpdate(String id, String downloadid, int progress) {
                if (id.equals(model.getId())) {
                    layoutProgress.setVisibility(View.VISIBLE);
                    layoutShare.setVisibility(View.GONE);
                    progressBarDownloadNumber.setMax(100);
                    progressBarDownloadNumber.setProgress(progress);
                    progressBarDownloadNumber.getProgress();

                }
            }

            @Override
            public void onDownloadComplete(String id, String downloadid) {
                int downloads = Integer.parseInt(model.getDownloads());
                tvDownloads.setText((downloads + 1) + "");

                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Video/VideoStatus/"
                        + Utils.getFilenameWithExtension(model.getVideourl(), model.getName()));
                new SingleMediaScanner(VideoViewActivity.this, file);
                addToView("downlodsstatus");
                if (id.equals(model.getId())) {
                    setDownloadImageUI();
                    layoutProgress.setVisibility(layoutProgress.GONE);
                    layoutShare.setVisibility(View.VISIBLE);

                    String filename = Utils.getfilenameFromVideoURL(model.getVideourl());

                    if (file.exists()) {
                        if (imageAction.equals("share")) {
                            sharingImage.generalShare(ImageStorage.getDownloadedImage(downloadImageFileName));
                        } else if (imageAction.equals("whatsapp")) {
                            sharingImage.whatsappShare(ImageStorage.getDownloadedImage(downloadImageFileName));
                        } else if (imageAction.equals("hike")) {
                            sharingImage.hikeShare(ImageStorage.getDownloadedImage(downloadImageFileName));
                        } else if (imageAction.equals("messagner")) {
                            sharingImage.fbmessagnerShare(ImageStorage.getDownloadedImage(downloadImageFileName));
                        } else if (imageAction.equals("instagram")) {
                            sharingImage.instagramShare(ImageStorage.getDownloadedImage(downloadImageFileName));
                        } else if (imageAction.equals("facebook")) {
                            sharingImage.facebookShare(ImageStorage.getDownloadedImage(downloadImageFileName));
                        }
                    }
                }
            }
        });
    }

    void setDownloadImageUI() {
        try {
            if (!Utils.checkifVideoExistsInFolder(model.getVideourl(), model.getName())) {
                imgDownloadAction.setImageDrawable(getResources().getDrawable(R.drawable.ic_down));
            } else {
                imgDownloadAction.setImageDrawable(getResources().getDrawable(R.drawable.ic_download_complete));
                boolean issaved = true;
                database.addDownloadedStatus(model);
            }
        } catch (Exception e) {
        }
    }

    private void showReportDialog() {
        videoView.pause();
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_report_video);
        TextView Cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tvSubmit = (TextView) dialog.findViewById(R.id.tv_submit);
        final RadioGroup radiogroup = (RadioGroup) dialog.findViewById(R.id.radiogroup);
        final RadioButton rb_copy = (RadioButton) dialog.findViewById(R.id.rb_copy);
        final RadioButton rb_sexual = (RadioButton) dialog.findViewById(R.id.rb_sexual);
        final RadioButton rb_other = (RadioButton) dialog.findViewById(R.id.rb_other);
        final EditText name_edt = (EditText) dialog.findViewById(R.id.name_edt);
        final EditText email_edt = (EditText) dialog.findViewById(R.id.email_edt);
        final EditText desc_edt = (EditText) dialog.findViewById(R.id.desc_edt);
        TextInputLayout name_layout = (TextInputLayout) dialog.findViewById(R.id.name_layout);
        TextInputLayout email_layout = (TextInputLayout) dialog.findViewById(R.id.email_layout);
        TextInputLayout desc_layout = (TextInputLayout) dialog.findViewById(R.id.desc_layout);


        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = radiogroup.getCheckedRadioButtonId();
                final String[] report_type = new String[1];
                radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case R.id.rb_copy:
                                report_type[0] = rb_copy.getText().toString();
                                // do operations specific to this selection
                                break;
                            case R.id.rb_other:
                                report_type[0] = rb_other.getText().toString();
                                // do operations specific to this selection
                                break;
                            case R.id.rb_sexual:
                                report_type[0] = rb_sexual.getText().toString();
                                // do operations specific to this selection
                                break;
                        }
                    }
                });

                String email = email_edt.getText().toString();
                String name = name_edt.getText().toString();
                String desc = desc_edt.getText().toString();
                String status_id = model.getId();

                ReportVideo(report_type[0], email, name, status_id, desc);
                dialog.dismiss();


            }
        });
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                videoView.start();
            }
        });
        dialog.show();
    }

    private void ReportVideo(String reportType, String email, String name, String status_id, String desc) {
        try {
            final ProgressDialog pd = new ProgressDialog(VideoViewActivity.this);
            pd.setTitle("Please Wait");
            pd.setMessage("Loading");
            pd.setCancelable(true);
            pd.show();
            /* RequestInterface request = RetrofitClient.getClient().create(RequestInterface.class);*/
            Call<ResponseBody> call = AppConstant.request.report("reportvideo",
                    "" + reportType,
                    "" + email,
                    "" + status_id,
                    "" + desc,
                    "" + name
            );

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        pd.dismiss();
                        String jsonst = response.body().string();
                        JSONObject json = new JSONObject(jsonst);
                        if (json.getInt("success") == 1) {
                            Toast.makeText(VideoViewActivity.this, "Report Sent", Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(getApplicationContext(), "" + json.getString("ack_msg"), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    pd.dismiss();
                    System.out.print("error" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLayoutParams() {
        //todo get window height
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        //todo set all layout height
        videoView.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
        videoView.setMeasureBasedOnAspectRatioEnabled(true);
        videoView.setScaleType(ScaleType.FIT_CENTER);
    }

    private void playVideo() {
        String imgurl = model.getImgurl();
        imgurl = imgurl.replace(" ", "%20");
        Picasso.with(VideoViewActivity.this)
                .load(Utils.getImageUriFromPath(imgurl))
                .placeholder(R.drawable.bg_default_album_art_empity)
                .error(R.drawable.bg_default_album_art_empity)
                .into(imageViewThumbDetails);
        String video_url = model.getVideourl();
        if (videoView.isPlaying()) {
            videoView.pause();
        }

        videoView.setVideoURI(Utils.getVideoUrifromPath(video_url));
        videoView.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared() {
                videoView.start();
                setPlayButton();
            }
        });
        imageViewThumbDetails.setVisibility(View.GONE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView.isPlaying()) {
            videoView.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoView.reset();
        videoView = null;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
//        googleAds.caclulateIntersialAds();
        switch (v.getId()) {
            case R.id.imgFavourite:
                Utils.addToFav(model, VideoViewActivity.this);
                setFavouriteImageUI();
                break;
            case R.id.imgShareWhatsandroid:
//                googleAds.caclulateIntersialAds();
                if (mInterstitialAd.isLoaded()) {
                    if (Utils.checkAdsDisplayTime(this)) {
                        mInterstitialAd.show();
                        mInterstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                requestNewInterstitial();
                                shareWhatsapp();
                            }
                        });
                    } else {
                        shareWhatsapp();
                    }
                } else {
                    shareWhatsapp();
                }
                break;
            case R.id.imgDownloadAction:
                googleAds.caclulateIntersialAds();
                if (ContextCompat.checkSelfPermission(VideoViewActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(VideoViewActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                } else {
                    sharingImage.inilizeOnShare(model);
                    if (!ImageStorage.checkifImageExistsInDownload(
                            Utils.getFilenameWithExtension(model.getVideourl(), model.getName()))) {
                        if (GlobalElements.isConnectingToInternet(this)) {
                            layoutProgress.setVisibility(View.VISIBLE);
                            layoutShare.setVisibility(View.GONE);
                            ManagerDW dm = ManagerDW.getInstance();
                            dm.callDownload(VideoViewActivity.this,
                                    model.getId(),
                                    model.getVideourl().replace(" ", "%20"),
                                    downloadImageFileName,
                                    model);
                        } else {
                            Toast.makeText(VideoViewActivity.this, "No Network Present", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        try {
                            String filename12 = downloadImageFileName;
                            File despath = new File(Environment.getExternalStorageDirectory(),
                                    "Video/VideoStatus/" + filename12);
                            File imageFileToShare = new File(despath.toString());
                            Uri uri = FileProvider.getUriForFile(getApplicationContext(),
                                    BuildConfig.APPLICATION_ID + ".provider", imageFileToShare);
                            Intent i = new Intent(Intent.ACTION_VIEW, uri);
                            i.setAction(Intent.ACTION_VIEW);
                            i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            i.setDataAndType(uri, "video/mp4");
                            try {
                                startActivity(i);
                            } catch (ActivityNotFoundException ex) {
                                Toast.makeText(VideoViewActivity.this, "No any suitable app found to play video", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                Toast.makeText(VideoViewActivity.this, "Download Process", Toast.LENGTH_SHORT).show();
                break;
            case R.id.imgCancelDownload:
                googleAds.caclulateIntersialAds();
                if (ContextCompat.checkSelfPermission(VideoViewActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(VideoViewActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                } else {
                    final File despath = new File(Environment.getExternalStorageDirectory(),
                            "Video/VideoStatus/" +
                                    downloadImageFileName);
                    if (despath != null && despath.exists()) {
                        despath.delete();
                    }
                    setDownloadImageUI();
                    int id = Preferance.getDownloadFileValue(VideoViewActivity.this, model.getId());

                    if (id != 0) {
                        ManagerDW.getInstance().cancleSpecifiedFile(id);
                        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        nMgr.cancel(Integer.parseInt(model.getId()));
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {

                                layoutProgress.setVisibility(layoutProgress.GONE);
                                layoutShare.setVisibility(layoutShare.VISIBLE);

                            }
                        }, 200);
                    }
                }
                break;
            case R.id.imgShareAction:
                googleAds.caclulateIntersialAds();
                if (ContextCompat.checkSelfPermission(VideoViewActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(VideoViewActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                } else {
                    imageAction = "share";
                    sharingImage.inilizeOnShare(model);
                    if (ImageStorage.checkifImageExistsInDownload(
                            Utils.getFilenameWithExtension(model.getVideourl(), model.getName()))) {
                        sharingImage.generalShare(ImageStorage.getDownloadedImage(
                                Utils.getFilenameWithExtension(model.getVideourl(), model.getName())));
                    } else {
                        layoutProgress.setVisibility(View.VISIBLE);
                        layoutShare.setVisibility(View.GONE);
                        ManagerDW dm = ManagerDW.getInstance();
                        dm.callDownload(VideoViewActivity.this, model.getId(),
                                model.getVideourl().replace(" ", "%20"),
                                downloadImageFileName,
                                model);
                    }
                }
                break;
            case R.id.imgPlayIco:
                if (videoView != null) {
                    togglePlayer();
                }
                break;
            case R.id.img_user:
                googleAds.caclulateIntersialAds();
                Intent intent = new Intent(VideoViewActivity.this, ProfileActivity.class);
                intent.putExtra("user_type", AppConstant.user);
                intent.putExtra("user_id", model.getUser_id());
                intent.putExtra("ori_type", ori_type);
                startActivity(intent);

                break;
            case R.id.video_view:
                toggle();

                break;

        }
    }

    private void shareWhatsapp() {
        if (ContextCompat.checkSelfPermission(VideoViewActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(VideoViewActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        } else {
            if (ContextCompat.checkSelfPermission(VideoViewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(VideoViewActivity.this, new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            } else {
                sharingImage.inilizeOnShare(model);
                if (ContextCompat.checkSelfPermission(VideoViewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(VideoViewActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                } else {
                    if (ImageStorage.checkifImageExistsInDownload(downloadImageFileName)) {
                        sharingImage.whatsappShare(ImageStorage.getDownloadedImage(downloadImageFileName));
                    } else {
                        imageAction = "whatsapp";
                        ManagerDW dm = ManagerDW.getInstance();
                        dm.callDownload(VideoViewActivity.this, model.getId(), model.getVideourl().replace(" ", "%20"), downloadImageFileName, model);
                    }
                }

            }
        }
    }

    private void SetPlayIcon() {
        if (videoView.isPlaying()) {
            imgPlayIco.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_circle_filled_black_48dp));
        } else {
            imgPlayIco.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_circle_filled_black_48dp));
        }
    }

    private void togglePlayer() {
        if (videoView.isPlaying()) {
            videoView.pause();
            imgPlayIco.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_circle_filled_black_48dp));
        } else {
            videoView.start();
            imgPlayIco.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_circle_filled_black_48dp));
        }
    }

    private void toggle() {
        if (playVisible) {
            imgPlayIco.setVisibility(View.VISIBLE);
        } else {
            imgPlayIco.setVisibility(View.GONE);
        }
        playVisible = !playVisible;
    }

    void setFavouriteImageUI() {
        try {
            if (!Utils.checkIfVideoAddedToFav(model.getId(), this)) {
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp);
                if (drawable.equals("")) {
                    imgFavourite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                } else {
                    drawable = DrawableCompat.wrap(drawable);
                    DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorPrimaryDark));
                    imgFavourite.setImageDrawable(drawable);
                }
            } else {
                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.ic_favorite_black_24dp);
                if (drawable.equals("")) {
                    imgFavourite.setImageResource(R.drawable.ic_favorite_black_24dp);
                } else {
                    drawable = DrawableCompat.wrap(drawable);
                    DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorPrimaryDark));
                    imgFavourite.setImageDrawable(drawable);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
