package com.dingdongstatus.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dingdongstatus.Apprate.Utils;
import com.dingdongstatus.CategoryListActivity;
import com.dingdongstatus.R;
import com.dingdongstatus.model.Category;

import java.util.ArrayList;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {

    private ArrayList<Category> categoryList;
    private Context mContext;
    String ori_type;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textCategoryName;
        LinearLayout LLcatclick;


        public MyViewHolder(View view) {
            super(view);
            textCategoryName = (TextView) view.findViewById(R.id.textCategoryName);
            LLcatclick = (LinearLayout) view.findViewById(R.id.LLcatclick);
        }
    }

    public LanguageAdapter(Context mContext, ArrayList<Category> categoryList, String ori_type) {
        this.categoryList = categoryList;
        this.mContext = mContext;
        this.ori_type = ori_type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artist, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Category gsCategory = categoryList.get(position);
        holder.textCategoryName.setText(gsCategory.getCategory_name());
        Utils.setTypefaceRegular(mContext, holder.textCategoryName);

        holder.LLcatclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Category gsCategory = categoryList.get(position);
                Intent in = new Intent(mContext, CategoryListActivity.class);
                in.putExtra("catname", gsCategory.getCategory_name());
                in.putExtra("catid", gsCategory.getCategory_id());
                in.putExtra("type", "2");
                in.putExtra("ori_type", ori_type);
                mContext.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}