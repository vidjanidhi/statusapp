package com.dingdongstatus.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dingdongstatus.Apprate.Utils;
import com.dingdongstatus.ProfileActivity;
import com.dingdongstatus.R;
import com.dingdongstatus.VideoViewActivity;
import com.dingdongstatus.model.Category;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.GoogleAds;
import com.facebook.ads.NativeAdLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dingdongstatus.support.FbAds.showNativeAdAdapter;

public class HomeAdapter1 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String ori_type = "";
    private ArrayList<VideoModel> data;
    private static final int LIST_AD_DELTA = 7;

    private Context mContext;
    private int AD_TYPE = 1, CONTENT_TYPE = 2, POSITION_TRENDING = 3;
    GoogleAds googleAds;
    private LinearLayoutManager trnding_layout;
    private TrendingAdapter trending_adapter;
    private ArrayList<VideoModel> trending_data = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_status)
        ImageView imgStatus;

        @BindView(R.id.tv_downloads)
        TextView tvDownloads;

        @BindView(R.id.img_user_iamge)
        CircleImageView imgUserIamge;

        @BindView(R.id.tv_username)
        TextView tvUsername;

        @BindView(R.id.tv_title)
        TextView tvTitle;

        @BindView(R.id.rl_main)
        RelativeLayout rlMain;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.fl_adplaceholder)
        FrameLayout flAdplaceholder;
        NativeAdLayout native_banner_ad_container;

        public AdViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            native_banner_ad_container = (NativeAdLayout) view.findViewById(R.id.native_banner_ad_container);
        }
    }

    public class TrendingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recycler_view_trending)
        RecyclerView recyclerViewTrending;
        @BindView(R.id.LLcatclick)
        LinearLayout LLcatclick;

        @BindView(R.id.tv_title_trending)
        TextView tvTitleTrending;
        @BindView(R.id.tv_title_latest)
        TextView tvTitleLatest;

        public TrendingViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public HomeAdapter1(Context mContext, ArrayList<VideoModel> categoryList, String ori_type) {
        this.data = categoryList;
        this.mContext = mContext;
        this.ori_type = ori_type;
        googleAds = new GoogleAds(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        RecyclerView.ViewHolder vh = null;
        if (viewType == AD_TYPE) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_ad, parent, false);
            vh = new AdViewHolder(v);
        } else if (viewType == CONTENT_TYPE) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
            vh = new MyViewHolder(v);
        } else if (viewType == POSITION_TRENDING) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_position_zero, parent, false);
            vh = new TrendingViewHolder(v);
        }


        return vh;
    }

    @Override
    public int getItemViewType(int position) {

        if (position > 0 && (position) % LIST_AD_DELTA == 0) {
            return AD_TYPE;
        } else if (position == 0) {
            return POSITION_TRENDING;
        } else {
            return CONTENT_TYPE;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (getItemViewType(position) == AD_TYPE) {
            AdViewHolder holder = (AdViewHolder) viewHolder;
            showNativeAdAdapter(mContext, holder.native_banner_ad_container);
//            GoogleAds.refreshAd((Activity) mContext, holder.flAdplaceholder);
        } else if (getItemViewType(position) == POSITION_TRENDING) {
            //todo trending
            TrendingViewHolder holder = (TrendingViewHolder) viewHolder;

            Utils.setTypefaceRegular(mContext, holder.tvTitleLatest);
            Utils.setTypefaceRegular(mContext, holder.tvTitleTrending);

            holder.recyclerViewTrending.setHasFixedSize(true);
            trnding_layout = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            holder.recyclerViewTrending.setLayoutManager(trnding_layout);
            holder.recyclerViewTrending.setItemAnimator(new DefaultItemAnimator());
            trending_adapter = new TrendingAdapter(mContext, trending_data, ori_type);
            holder.recyclerViewTrending.setAdapter(trending_adapter);

            getCategory();

        } else if (getItemViewType(position) == CONTENT_TYPE) {
            MyViewHolder holder = (MyViewHolder) viewHolder;

           /* if (ori_type.equals(AppConstant.LANDSCAPE)) {
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPx(200));
                holder.rlMain.setLayoutParams(layoutParams);
//                holder.imgStatus.getLayoutParams().height = 200;
//                holder.imgStatus.requestLayout();
            } else {
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPx(320));
                holder.rlMain.setLayoutParams(layoutParams);
            }*/

            final VideoModel model = data.get(getRealPosition(position));
            final int realPOsition = getRealPosition(position);

            Utils.setTypefaceRegular(mContext, holder.tvTitle);
            Utils.setTypefaceRegular(mContext, holder.tvUsername);
            Utils.setTypefaceRegular(mContext, holder.tvDownloads);

            holder.tvTitle.setText(model.getName());
            holder.tvUsername.setText(model.getUser_name());

            holder.tvDownloads.setText(model.getDownloads());
//        holder.tvView.setText(model.getView());
            String imguserurl = model.getUser_imgurl();
            imguserurl = imguserurl.replace(" ", "%20");

            String imgurl = model.getImgurl();
            imgurl = imgurl.replace(" ", "%20");

            Picasso.with(mContext)
                    .load(Utils.getImageUriFromPath(imgurl))
                    .placeholder(R.color.cardview_dark_background)
                    .error(R.drawable.bg_default_album_art_empity)
                    .into(holder.imgStatus);

            Picasso.with(mContext)
                    .load(Utils.getImageUriFromPath(imguserurl))
                    .placeholder(R.drawable.back)
                    .error(R.drawable.back)
                    .into(holder.imgUserIamge);


            holder.imgUserIamge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    googleAds.caclulateIntersialAds();
                    Intent intent = new Intent(mContext, ProfileActivity.class);
                    intent.putExtra("user_type", AppConstant.user);
                    intent.putExtra("user_id", model.getUser_id());
                    intent.putExtra("image", model.getUser_imgurl());
                    intent.putExtra("name", model.getUser_name());
                    intent.putExtra("ori_type", ori_type);
                    mContext.startActivity(intent);
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    googleAds.caclulateIntersialAds();
                    VideoModel model = data.get(realPOsition);
                    Intent in = new Intent(mContext, VideoViewActivity.class);
                    in.putExtra("data", model);
                    in.putExtra("position", realPOsition);
                    in.putExtra("ori_type", ori_type);
                    mContext.startActivity(in);
                }
            });
        }
    }

    private int getRealPosition(int position) {
        if (LIST_AD_DELTA == 0) {
            return position;
        } else if ((position + 1) % LIST_AD_DELTA == 0) {
            int p = (position - (position / LIST_AD_DELTA) - 1);
            return p;
        } else {
            int p = (position - (position / LIST_AD_DELTA) - 1);
            return p;
        }

    }

    @Override
    public int getItemCount() {

//        return data.size();
        int additionalContent = 0;
        if (data.size() > 0 && LIST_AD_DELTA > 0 && data.size() > LIST_AD_DELTA) {
            additionalContent = (data.size() + (data.size() / LIST_AD_DELTA)) / LIST_AD_DELTA;
        }
        Log.i("size", data.size() + additionalContent + 1 + "");
        return data.size() + additionalContent + 1;
    }


    private void getCategory() {

        /* RequestInterface req = RetrofitClient.getClient().create(RequestInterface.class);*/
        Call<ResponseBody> call = AppConstant.request.getLatestStatus(
                "statuslist",
                ori_type,
                "1"
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                data.clear();
                try {
                    String jsonst = response.body().string();
                    JSONObject json = new JSONObject(jsonst);
//                    page = Integer.parseInt(json.getString("page"));
//                    total_rec = Integer.parseInt(json.getString("total_rec"));
//                    limit = Integer.parseInt(json.getString("limit"));
                    Category da = new Category();

                    if (json.getInt("success") == 1) {
//                        LoadAd loadAd = (LoadAd) getContext();
//                        loadAd.load();
                        JSONArray result = json.getJSONArray("data");
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject c = result.getJSONObject(i);
                            VideoModel modelOffers = new VideoModel();
                            modelOffers.setId("" + c.getString("id"));
                            modelOffers.setName("" + c.getString("name"));
                            modelOffers.setImgurl("" + c.getString("imgurl"));
                            modelOffers.setVideourl("" + c.getString("videourl"));
                            modelOffers.setTag("" + c.getString("tag"));
                            modelOffers.setDownloads("" + c.getString("downloads"));
                            modelOffers.setView("" + c.getString("view"));
                            modelOffers.setShare("" + c.getString("share"));
                            modelOffers.setStatus("" + c.getString("status"));
                            modelOffers.setUser_id("" + c.getString("user_id"));
                            modelOffers.setUser_name("" + c.getString("user_name"));
                            modelOffers.setUser_imgurl("" + c.getString("user_imgurl"));
                            trending_data.add(modelOffers);
                        }
                        trending_adapter.notifyDataSetChanged();
                    } else {
//                        Toaster.show(getActivity(), "" + json.getString("ack_msg"), false, Toaster.DANGER);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.print("error" + t.getMessage());
            }
        });
    }
}