package com.dingdongstatus.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dingdongstatus.Apprate.Utils;
import com.dingdongstatus.R;
import com.dingdongstatus.VideoDescActivity;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.support.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RelatedVideoAdapter extends RecyclerView.Adapter<RelatedVideoAdapter.MyViewHolder> {

    private ArrayList<VideoModel> categoryList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textCategoryName, textCountWallpaper;
        ImageView imgcategory;


        public MyViewHolder(View view) {
            super(view);
            textCategoryName = (TextView) view.findViewById(R.id.txtVideoTitle);
            imgcategory = (ImageView) view.findViewById(R.id.imgThumbVideo);
        }
    }

    public RelatedVideoAdapter(Context mContext, ArrayList<VideoModel> categoryList) {
        this.categoryList = categoryList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_grid_video, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final VideoModel gsCategory = categoryList.get(position);
        holder.textCategoryName.setText(gsCategory.getName());

        Utils.setTypefaceRegular(mContext, holder.textCategoryName);


        String imgurl = categoryList.get(position).getImgurl();
        imgurl = imgurl.replace(" ", "%20");
        Picasso.with(mContext)
                .load(imgurl)
                .placeholder(R.drawable.bg_default_album_art_empity)
                .error(R.drawable.bg_default_album_art_empity)
                .into(holder.imgcategory);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VideoModel model = categoryList.get(position);
                Intent in = new Intent(mContext, VideoDescActivity.class);
                in.putExtra("data", model);
                in.putExtra("position", position);
                in.putExtra("ori_type", AppConstant.LANDSCAPE);
                mContext.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}