package com.dingdongstatus.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dingdongstatus.Apprate.Utils;
import com.dingdongstatus.R;
import com.dingdongstatus.model.Moreapp;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MoreappAdapter extends RecyclerView.Adapter<MoreappAdapter.MyViewHolder> {
    private List<Moreapp> moreappList;
    private Context context;
    android.support.v7.app.AlertDialog alertDialog;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtmoreapptitle;
        LinearLayout LLcatclick;
        ImageView imgmoreapplogo;


        public MyViewHolder(View view) {
            super(view);
            txtmoreapptitle = (TextView) view.findViewById(R.id.txtmoreapptitle);
            LLcatclick = (LinearLayout) view.findViewById(R.id.LLcatclick);
            imgmoreapplogo = (ImageView) view.findViewById(R.id.imgmoreapplogo);

            txtmoreapptitle.setSelected(true);
            txtmoreapptitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            txtmoreapptitle.setSingleLine(true);


        }
    }

    public MoreappAdapter(Context context, List<Moreapp> moreappList, android.support.v7.app.AlertDialog alertDialog) {
        this.moreappList = moreappList;
        this.context = context;
        this.alertDialog = alertDialog;
    }

    @Override
    public MoreappAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_more_app, parent, false);

        return new MoreappAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MoreappAdapter.MyViewHolder mholder, int position) {
        final Moreapp gsMoreapp = moreappList.get(position);


        try {
            mholder.txtmoreapptitle.setText(gsMoreapp.getImgurl());
            Utils.setTypefaceRegular(context, mholder.txtmoreapptitle);

            Picasso.with(context)
                    .load(gsMoreapp.getIcon())
                    .placeholder(R.mipmap.ic_launcher)   // optional
                    .error(R.mipmap.ic_launcher)      // optional
                    .into(mholder.imgmoreapplogo);
        } catch (Exception e) {

        }


        mholder.LLcatclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                alertDialog.dismiss();
                String name = gsMoreapp.getUrl();

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(name));
                context.startActivity(i);


            }
        });

    }

    @Override
    public int getItemCount() {
        return moreappList.size();
    }
}