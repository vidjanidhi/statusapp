package com.dingdongstatus.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dingdongstatus.Apprate.Utils;
import com.dingdongstatus.ProfileActivity;
import com.dingdongstatus.R;
import com.dingdongstatus.VideoViewActivity;
import com.dingdongstatus.model.VideoModel;
import com.dingdongstatus.other.Methods;
import com.dingdongstatus.support.AppConstant;
import com.dingdongstatus.support.GoogleAds;
import com.facebook.ads.NativeAdLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.dingdongstatus.support.FbAds.showNativeAdAdapter;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private String ori_type = "";
    private ArrayList<VideoModel> data;
    private static final int LIST_AD_DELTA = 7;
    private Context mContext;
    private int AD_TYPE = 1, CONTENT_TYPE = 2;
    GoogleAds googleAds;
    Methods methods;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_status)
        ImageView imgStatus;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.img_user_iamge)
        CircleImageView imgUserIamge;
        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.ll_song_name)
        LinearLayout ll_song_name;
        @BindView(R.id.tv_downloads)
        TextView tvDownloads;
        @BindView(R.id.rl_main)
        RelativeLayout rlMain;


        //        @BindView(R.id.tv_downloads)
//        TextView tvDownloads;
       /* @BindView(R.id.tv_view)
        TextView tvView;*/
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.fl_adplaceholder)
        FrameLayout flAdplaceholder;
        NativeAdLayout native_banner_ad_container;

        public AdViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            native_banner_ad_container = (NativeAdLayout) view.findViewById(R.id.native_banner_ad_container);
        }
    }

    public HomeAdapter(Context mContext, ArrayList<VideoModel> categoryList, String ori_type) {
        this.data = categoryList;
        this.mContext = mContext;
        this.ori_type = ori_type;
        googleAds = new GoogleAds(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        RecyclerView.ViewHolder vh = null;
        if (viewType == AD_TYPE) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_ad, parent, false);
            vh = new AdViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
            vh = new MyViewHolder(v);
        }


        return vh;
    }

    @Override
    public int getItemViewType(int position) {

        if (position > 0 && (position + 1) % LIST_AD_DELTA == 0) {
            return AD_TYPE;
        } /*else if (position == 0) {
            return CUSTOM_AD;
        }*/
        return CONTENT_TYPE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (getItemViewType(position) == AD_TYPE) {
            AdViewHolder holder = (AdViewHolder) viewHolder;
            showNativeAdAdapter(mContext, holder.native_banner_ad_container);
//            GoogleAds.refreshAd((Activity) mContext, holder.flAdplaceholder);
        } else {
            MyViewHolder holder = (MyViewHolder) viewHolder;

          /*  if (ori_type.equals(AppConstant.LANDSCAPE)) {
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPx(200));
                holder.rlMain.setLayoutParams(layoutParams);
//                holder.imgStatus.getLayoutParams().height = 200;
//                holder.imgStatus.requestLayout();
            } else {
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPx(320));
                holder.rlMain.setLayoutParams(layoutParams);
            }*/
            final VideoModel model = data.get(getRealPosition(position));
            final int realPOsition = getRealPosition(position);
            holder.tvTitle.setText(model.getName());
            holder.tvUsername.setText(model.getUser_name());

            Utils.setTypefaceRegular(mContext, holder.tvTitle);
            Utils.setTypefaceRegular(mContext, holder.tvUsername);
            Utils.setTypefaceRegular(mContext, holder.tvDownloads);

            holder.tvDownloads.setText(model.getDownloads());
//        holder.tvView.setText(model.getView());
            String imguserurl = model.getUser_imgurl();
            imguserurl = imguserurl.replace(" ", "%20");

            String imgurl = model.getImgurl();
            imgurl = imgurl.replace(" ", "%20");

            Picasso.with(mContext)
                    .load(Utils.getImageUriFromPath(imgurl))
                    .placeholder(R.color.cardview_dark_background)
                    .error(R.drawable.bg_default_album_art_empity)
                    .into(holder.imgStatus);

            Picasso.with(mContext)
                    .load(Utils.getImageUriFromPath(imguserurl))
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.drawable.bg_default_album_art_empity)
                    .into(holder.imgUserIamge);
            holder.imgUserIamge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    googleAds.caclulateIntersialAds();
                    Intent intent = new Intent(mContext, ProfileActivity.class);
                    intent.putExtra("user_type", AppConstant.user);
                    intent.putExtra("user_id", model.getUser_id());
                    intent.putExtra("image", model.getUser_imgurl());
                    intent.putExtra("name", model.getUser_name());
                    intent.putExtra("ori_type", ori_type);
                    mContext.startActivity(intent);
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    googleAds.caclulateIntersialAds();
                    VideoModel model = data.get(realPOsition);
                    Intent in = new Intent(mContext, VideoViewActivity.class);
                    in.putExtra("data", model);
                    in.putExtra("position", realPOsition);
                    in.putExtra("ori_type", ori_type);
                    mContext.startActivity(in);
                }
            });
        }


    }

    private int getRealPosition(int position) {
        if (LIST_AD_DELTA == 0) {
            return position;
        } else if (position % LIST_AD_DELTA == 0) {
            int p = (position - position / LIST_AD_DELTA);
            return p;
        } else {
            int p = (position - position / LIST_AD_DELTA);
            return p;
        }

    }

    String getfilename(String name) {
        String fname = "";

        try {
            String url = name;
            fname = url.substring(url.lastIndexOf('/') + 1);
        } catch (Exception e) {

        }


        return fname;
    }

    String getfilepath(String path) {
        String filepath = "";
        try {

            filepath = path.substring(0, path.lastIndexOf('/') + 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return filepath;

    }

    @Override
    public int getItemCount() {

//        return data.size();
        int additionalContent = 0;
        if (data.size() > 0 && LIST_AD_DELTA > 0 && data.size() > LIST_AD_DELTA) {
            additionalContent = (data.size() + (data.size() / LIST_AD_DELTA)) / LIST_AD_DELTA;
        }
        return data.size() + additionalContent;
    }
}