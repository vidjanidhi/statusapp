package com.dingdongstatus;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dingdongstatus.support.AppConstant;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.dingdongstatus.netutils.GlobalElements;
import com.dingdongstatus.netutils.MyPreferences;
import com.dingdongstatus.netutils.RequestInterface;
import com.dingdongstatus.netutils.RetrofitClient;
import com.dingdongstatus.support.MyTextWatcher;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.btn_back)
    ImageView btnBack;
    @BindView(R.id.mobile_edt)
    EditText mobileEdt;
    @BindView(R.id.mobile_layout)
    TextInputLayout mobileLayout;
    @BindView(R.id.password_edt)
    EditText passwordEdt;
    @BindView(R.id.password_layout)
    TextInputLayout passwordLayout;
    /* @BindView(R.id.ll_g_login)
     LinearLayout llGLogin;*/

    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.g_login)
    SignInButton gLogin;
    @BindView(R.id.tv_sign_up)
    TextView tvSignUp;
    /*@BindView(R.id.ll_fb_login)
    com.facebook.login.widget.LoginButton llFbLogin;*/

    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private String TAG = "login";
    FirebaseAuth.AuthStateListener mAuthStateListener;
    private static final int RC_SIGN_IN = 102;
    MyPreferences myPreferences;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

       /* //todo facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        llFbLogin.setReadPermissions("email", "public_profile");
        // Callback registration
        llFbLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.e(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "facebook:onCancel");
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e(TAG, "facebook:onError", exception);
                // App code
            }
        });*/
        //todo google


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail().requestProfile()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
        myPreferences = new MyPreferences(this);

        mobileEdt.addTextChangedListener(new MyTextWatcher(mobileLayout));
        passwordEdt.addTextChangedListener(new MyTextWatcher(passwordLayout));
        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivityForResult(i, 101);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        gLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GlobalElements.isConnectingToInternet(LoginActivity.this)) {
                    if (mobileEdt.getText().toString().equals("")) {
                        mobileLayout.setError("Enter Email");
                    } else if (passwordEdt.getText().toString().equals("")) {
                        passwordLayout.setError("Enter Password");
                    } else {
                        LoginUser();
                    }
                } else {
                    GlobalElements.showDialog(LoginActivity.this);
                }

            }
        });
    }
/*
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUIfacebook(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
//                            updateUI(null);
                        }
                    }
                });
    }*/


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    /* @Override
     public void onStart() {
         super.onStart();
        *//* // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);*//*
        FirebaseAuth.getInstance().addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
*/
   /* private void updateUIfacebook(FirebaseUser currentUser) {
        myPreferences.setPreferences(MyPreferences.login_type, "1");
        myPreferences.setPreferences(MyPreferences.id, currentUser.getUid());
        myPreferences.setPreferences(MyPreferences.email, currentUser.getEmail());
        myPreferences.setPreferences(MyPreferences.name, currentUser.getDisplayName());
        SendDataToServer(currentUser, "facebook");
    }*/

    private void updateUIGoogle(FirebaseUser currentUser) {
        myPreferences.setPreferences(MyPreferences.login_type, "1");
        myPreferences.setPreferences(MyPreferences.id, currentUser.getUid());
        myPreferences.setPreferences(MyPreferences.email, currentUser.getEmail());
        myPreferences.setPreferences(MyPreferences.name, currentUser.getDisplayName());
        myPreferences.setPreferences(MyPreferences.profile_image, currentUser.getPhotoUrl().toString());
        SendDataToServer(currentUser, "google");
    }

    private void SendDataToServer(FirebaseUser currentUser, String type) {
        try {
            final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
            pd.setTitle("Please Wait");
            pd.setMessage("Loading");
            pd.setCancelable(true);
            pd.show();
            String url = currentUser.getPhotoUrl().toString();
            /*RequestInterface request = RetrofitClient.getClient().create(RequestInterface.class);*/
            Call<ResponseBody> call = AppConstant.request.register("" + currentUser.getEmail(),
                    "" + currentUser.getDisplayName(),
                    "",
                    "" + "register",
                    "" + currentUser.getUid(),
                    type + "",
                    "" + currentUser.getPhotoUrl().toString(),
                    ""
            );

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        pd.dismiss();
                        String jsonst = response.body().string();
                        JSONObject json = new JSONObject(jsonst);
                        if (json.getInt("success") == 1) {
//                            JSONArray array = json.getJSONArray("data");
                            JSONObject result = json.getJSONObject("data");
                            String name = result.getString("name");
                            String email = result.getString("email");
                            String id = result.getString("id");
                            myPreferences.setPreferences(MyPreferences.email, email);
                            myPreferences.setPreferences(MyPreferences.id, id);
                            myPreferences.setPreferences(MyPreferences.name, name);
//                            myPreferences.setPreferences(MyPreferences.login_type, "3");
                            Intent myIntent = new Intent(LoginActivity.this,
                                    MainActivity.class);
                            startActivity(myIntent);
                            setResult(101);
                            finish();
                        } else {
                            setResult(101);
                            finish();
                            Toast.makeText(getApplicationContext(), "" + json.getString("data"), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    pd.dismiss();
                    System.out.print("error" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            updateUIGoogle(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
//                            Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
//                            updateUIGoogle(null);
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
         // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
            }
        } else if (requestCode == 101) {
            setResult(101);
            finish();
        }
    }

    private void LoginUser() {
        try {
            final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
            pd.setTitle("Please Wait");
            pd.setMessage("Loading");
            pd.setCancelable(true);
            pd.show();
           /* RequestInterface request = RetrofitClient.getClient().create(RequestInterface.class);*/
            Call<ResponseBody> call = AppConstant.request.login("" + mobileEdt.getText().toString(), "login",
                    "" + passwordEdt.getText().toString()
            );

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        pd.dismiss();
                        String jsonst = response.body().string();
                        JSONObject json = new JSONObject(jsonst);
                        if (json.getInt("success") == 1) {
                            JSONObject result = json.getJSONObject("data");
                            String id = result.getString("id");
                            String name = result.getString("name");
                            String email = result.getString("email");
                            myPreferences.setPreferences(MyPreferences.email, email);
                            myPreferences.setPreferences(MyPreferences.id, id);
                            myPreferences.setPreferences(MyPreferences.name, name);
                            myPreferences.setPreferences(MyPreferences.login_type, "3");
//                            Intent myIntent = new Intent(LoginActivity.this,
//                                    MainActivity.class);
//                            startActivity(myIntent);
                            setResult(101);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "" + json.getString("ack_msg"), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    pd.dismiss();
                    System.out.print("error" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection failed", Toast.LENGTH_LONG).show();
    }
}
