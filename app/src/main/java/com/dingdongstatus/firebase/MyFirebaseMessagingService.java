package com.dingdongstatus.firebase;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.dingdongstatus.MainActivity;
import com.dingdongstatus.R;
import com.dingdongstatus.other.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    // private NotificationUtils notificationUtils;
    Context mContext;
    String CHANNEL_ID = "Event";
    CharSequence name = "Event Notifications";
    int importance = NotificationManager.IMPORTANCE_HIGH;
//    private String bigpicture;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Log.e(TAG, "From: " + remoteMessage.getFrom());
        mContext = getApplicationContext();
        if (remoteMessage == null)
            return;
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    public static final int NOTIFICATION_ID = 1;

    private void sendNotification(String title, String msg, String url) {
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent;
        if (!Constants.pushCID.equals("0")) {
            intent = new Intent(this, MainActivity.class);
            intent.putExtra("ispushnoti", true);
        } else if (url != null && !url.equals("")) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
        } else {
            intent = new Intent(this, MainActivity.class);
        }
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationChannel mChannel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "QuotesPush";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mNotificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setAutoCancel(true)
                .setSound(uri)
                .setAutoCancel(true)
                .setLights(Color.RED, 800, 800)
                .setContentText(msg)
                .setChannelId(CHANNEL_ID);

        mBuilder.setSmallIcon(getNotificationIcon(mBuilder));
        try {
            mBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "errror large- " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        if (title.trim().isEmpty()) {
            mBuilder.setContentTitle(getString(R.string.app_name));
            mBuilder.setTicker(getString(R.string.app_name));
        } else {
            mBuilder.setContentTitle(title);
            mBuilder.setTicker(title);
        }

        if (url != null) {
            mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(getBitmapFromURL(url)));
        }

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            return null;
        }
    }

    private int getNotificationIcon(NotificationCompat.Builder notificationBuilder) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(getColour());
            return R.drawable.ic_notication;
        } else {
            return R.mipmap.ic_launcher;
        }
    }

    private int getColour() {
        return 0x8b5630;
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());
        try {
            JSONObject data = json.getJSONObject("data");
            String title = data.getString("Title");
            String message = "message";
            String imageUrl = data.getString("Image");
            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "imageUrl: " + imageUrl);
            sendNotification(title, message, imageUrl);

          /*  Bitmap bitmap = getBitmapFromURL(imageUrl);
            Bitmap bitmapSmall = getBitmapFromURL(imageUrl);
//            Bitmap bitmapSmall = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

            long when = System.currentTimeMillis();
            if (bitmap != null) {
                showBigNotification(getApplicationContext(), bitmap, bitmapSmall, title, message, when);
                new generatePictureStyleNotification(this, "Title", "Message",
                        "http://api.androidhive.info/images/sample.jpg").execute();
            } else {
                return;
            }*/

            //startActivity(intent);

            /*
            // app is in background, show the notification in notification tray
            Intent resultIntent = new Intent(getApplicationContext(), Home.class);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("category_id","");
            resultIntent.putExtra("category_quantity","");
            resultIntent.putExtra("category_name","");
               */

            /*
            // check for image attachment
            if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, ts, resultIntent);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, ts, resultIntent, imageUrl);
            }
            */


        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }


    private void showBigNotification(Context mContext, Bitmap bitmap, Bitmap bitmapSmall, String title, String des, long when) {
        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext,
                0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification foregroundNote;
        RemoteViews notification_large = new RemoteViews(mContext.getPackageName(), R.layout.notification_large);
        RemoteViews notification_small = new RemoteViews(mContext.getPackageName(), R.layout.notification_small);

        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(mContext, "1")
                .setContentIntent(pendingIntent);
        setBitmap(notification_large, R.id.imageView_large, bitmap);
        setBitmap(notification_large, R.id.imageView, bitmapSmall);
        notification_large.setTextViewText(R.id.txtName, title);
        notification_small.setTextViewText(R.id.txtName, title);
        setBitmap(notification_small, R.id.imageView, bitmapSmall);
        NotificationManager mNotifyManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = "my_channel_01";// The id of the channel.
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mNotifyManager.createNotificationChannel(mChannel);

            foregroundNote = mNotifyBuilder
                    .setContentText("Slide down on note to expand Slide down on note to expandSlide down on note to expandSlide down on note to expandSlide down on note to expandSlide down on note to expandSlide down on note to expand")
                    .setSmallIcon(R.drawable.ic_notication)
                    .setChannelId(CHANNEL_ID)
                    .build();
            foregroundNote.bigContentView = notification_large;
            foregroundNote.contentView = notification_small;
            foregroundNote.flags += Notification.FLAG_AUTO_CANCEL;
            foregroundNote.flags |= Notification.FLAG_AUTO_CANCEL;
            foregroundNote.defaults |= Notification.DEFAULT_SOUND;
        } else {
            foregroundNote = mNotifyBuilder
                    .setContentText("Slide down on note to expand Slide down on note to expandSlide down on note to expandSlide down on note to expandSlide down on note to expandSlide down on note to expandSlide down on note to expand")
                    .setSmallIcon(R.drawable.ic_notication)
                    .build();
            foregroundNote.bigContentView = notification_large;
            foregroundNote.contentView = notification_small;
            foregroundNote.flags += Notification.FLAG_AUTO_CANCEL;
            foregroundNote.flags |= Notification.FLAG_AUTO_CANCEL;
            foregroundNote.defaults |= Notification.DEFAULT_SOUND;
        }
        Random randomno = new Random();
        mNotifyManager.notify(randomno.nextInt(10000), foregroundNote);
    }

    public class generatePictureStyleNotification extends AsyncTask<String, Void, Bitmap> {

        private Context mContext;
        private String title, message, imageUrl;

        public generatePictureStyleNotification(Context context, String title, String message, String imageUrl) {
            super();
            this.mContext = context;
            this.title = title;
            this.message = message;
            this.imageUrl = imageUrl;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {
                URL url = new URL(this.imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);

            Intent intent = new Intent(mContext, MainActivity.class);
//            intent.putExtra("key", "value");
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 100, intent, PendingIntent.FLAG_ONE_SHOT);

            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

            Notification notif = new Notification.Builder(mContext)
                    .setContentIntent(pendingIntent)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(result)
                    .setStyle(new Notification.BigPictureStyle().bigPicture(result))
                    .build();
            notif.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(1, notif);
        }
    }

    /*public Bitmap getBitmapFromURL(String strURL) {
        String urlStr = strURL;
        Bitmap img = null;

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(urlStr);
        HttpResponse response;
        try {
            response = (HttpResponse) client.execute(request);
            HttpEntity entity = response.getEntity();
            BufferedHttpEntity bufferedEntity = new BufferedHttpEntity(entity);
            InputStream inputStream = bufferedEntity.getContent();
            img = BitmapFactory.decodeStream(inputStream);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return img;
    }*/


    private void setBitmap(RemoteViews views, int resId, Bitmap bitmap) {

        Bitmap proxy = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(proxy);
        c.drawBitmap(bitmap, new Matrix(), null);
        views.setImageViewBitmap(resId, proxy);
    }


}