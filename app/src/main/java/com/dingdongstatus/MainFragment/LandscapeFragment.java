package com.dingdongstatus.MainFragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dingdongstatus.R;
import com.dingdongstatus.model.GeneralModel;
import com.dingdongstatus.pageadapter.LandscapePagerAdapter;
import com.dingdongstatus.support.FbAds;
import com.dingdongstatus.support.GoogleAds;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class LandscapeFragment extends Fragment {


    Unbinder unbinder;
    ArrayList<GeneralModel> data = new ArrayList<>();
    LandscapePagerAdapter pagerAdapter;
    @BindView(R.id.tab_layout_one)
    TabLayout tabLayout;
    @BindView(R.id.view_pager_one)
    ViewPager viewPager;
    private FbAds googleAds;

    public LandscapeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_landscape, container, false);
        unbinder = ButterKnife.bind(this, view);
        googleAds = new FbAds(getContext());
        LoadTiles();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                googleAds.caclulateIntersialAds();

                Drawable drawable = ContextCompat.getDrawable(getContext(), Integer.parseInt(tab.getTag().toString()));
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable, getResources().getColor(R.color.darkOrchid));

                ImageView img = (ImageView) tab.getCustomView().findViewById(R.id.tab_icon);
                img.setImageDrawable(drawable);
//                tvTitle.setText(tab.getTag().toString());
//                getSupportActionBar().setTitle(tab.getText());
//                tab.getIcon().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), Integer.parseInt(tab.getTag().toString()));
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorPrimaryDark));

                ImageView img = (ImageView) tab.getCustomView().findViewById(R.id.tab_icon);
                img.setImageDrawable(drawable);
//                tab.getIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        return view;
    }

    private void LoadTiles() {
        GeneralModel da = new GeneralModel();
        da = new GeneralModel();
        da.setId("1");
        da.setName("Home");
        da.setIcon(R.drawable.ic_home_black_24dp);
        data.add(da);

        da = new GeneralModel();
        da.setId("2");
        da.setName("Categories");
        da.setIcon(R.mipmap.ic_category);
        data.add(da);

        da = new GeneralModel();
        da.setId("3");
        da.setName("Trending");
        da.setIcon(R.drawable.ic_whatshot_black_24dp);
        data.add(da);

        da = new GeneralModel();
        da.setId("5");
        da.setName("Search");
        da.setIcon(R.mipmap.ic_action_action_search);
        data.add(da);

        da = new GeneralModel();
        da.setId("4");
        da.setName("More");
        da.setIcon(R.drawable.ic_more_black_24dp);
        data.add(da);

        pagerAdapter = new LandscapePagerAdapter(getFragmentManager(), data, getContext());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(1);
        pagerAdapter.notifyDataSetChanged();
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {
        for (int i = 0; i < data.size(); i++) {
            tabLayout.getTabAt(i).setCustomView(R.layout.tab_custom_layout);
            tabLayout.getTabAt(i).setTag(data.get(i).getIcon());
            TextView tab_name = (TextView) tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tv_tab);
            tab_name.setText("" + data.get(i).getName());

            Drawable drawable = ContextCompat.getDrawable(getContext(), data.get(i).getIcon());
            drawable = DrawableCompat.wrap(drawable);
            if (i == 0) {
                DrawableCompat.setTint(drawable, getResources().getColor(R.color.darkOrchid));
            } else {
                DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorPrimaryDark));
            }
//            tabLayout.getTabAt(i).setIcon(drawable);

            ImageView img = (ImageView) tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_icon);
            img.setImageDrawable(drawable);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
