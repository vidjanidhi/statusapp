package com.devbrackets.android.exomedia.core.audio;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.FloatRange;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.devbrackets.android.exomedia.ExoMedia.RendererType;
import com.devbrackets.android.exomedia.core.ListenerMux;
import com.devbrackets.android.exomedia.core.api.AudioPlayerApi;
import com.google.android.exoplayer2.drm.MediaDrmCallback;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import java.util.Map;

public class NativeAudioPlayer implements AudioPlayerApi {
    private static final String TAG = "NativeMediaPlayer";
    @NonNull
    protected final Context context;
    protected int currentBufferPercent = 0;
    @NonNull
    protected InternalListeners internalListeners = new InternalListeners();
    protected ListenerMux listenerMux;
    @NonNull
    protected final MediaPlayer mediaPlayer;
    protected long requestedSeek;

    protected class InternalListeners implements OnBufferingUpdateListener {
        protected InternalListeners() {
        }

        public void onBufferingUpdate(MediaPlayer mediaPlayer, int percent) {
            NativeAudioPlayer.this.listenerMux.onBufferingUpdate(percent);
            NativeAudioPlayer.this.currentBufferPercent = percent;
        }
    }

    public NativeAudioPlayer(@NonNull Context context) {
        this.context = context;
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer.setOnBufferingUpdateListener(this.internalListeners);
    }

    public void setDataSource(@Nullable Uri uri) {
        setDataSource(uri, null);
    }

    public void setDataSource(@Nullable Uri uri, @Nullable MediaSource mediaSource) {
        try {
            this.requestedSeek = 0;
            this.mediaPlayer.setDataSource(this.context, uri);
        } catch (Exception e) {
            Log.d(TAG, "MediaPlayer: error setting data source", e);
        }
    }

    public void setDrmCallback(@Nullable MediaDrmCallback drmCallback) {
    }

    public void prepareAsync() {
        try {
            this.mediaPlayer.prepareAsync();
        } catch (Exception e) {
        }
    }

    public void reset() {
        this.mediaPlayer.reset();
    }

    public void setVolume(@FloatRange(from = 0.0d, to = 1.0d) float left, @FloatRange(from = 0.0d, to = 1.0d) float right) {
        this.mediaPlayer.setVolume(left, right);
    }

    public void seekTo(@IntRange(from = 0) long milliseconds) {
        if (this.listenerMux == null || !this.listenerMux.isPrepared()) {
            this.requestedSeek = milliseconds;
            return;
        }
        this.mediaPlayer.seekTo((int) milliseconds);
        this.requestedSeek = 0;
    }

    public boolean isPlaying() {
        return this.mediaPlayer.isPlaying();
    }

    public void start() {
        this.mediaPlayer.start();
        if (this.listenerMux != null) {
            this.listenerMux.setNotifiedCompleted(false);
        }
    }

    public void pause() {
        this.mediaPlayer.pause();
    }

    public void stopPlayback() {
        this.mediaPlayer.stop();
    }

    public boolean restart() {
        if (this.listenerMux == null || !this.listenerMux.isPrepared()) {
            return false;
        }
        this.mediaPlayer.seekTo(0);
        this.mediaPlayer.start();
        this.listenerMux.setNotifiedCompleted(false);
        return true;
    }

    public long getDuration() {
        if (this.listenerMux == null || !this.listenerMux.isPrepared()) {
            return 0;
        }
        return (long) this.mediaPlayer.getDuration();
    }

    public long getCurrentPosition() {
        if (this.listenerMux == null || !this.listenerMux.isPrepared()) {
            return 0;
        }
        return (long) this.mediaPlayer.getCurrentPosition();
    }

    public int getBufferedPercent() {
        return this.currentBufferPercent;
    }

    public void release() {
        this.mediaPlayer.release();
    }

    public int getAudioSessionId() {
        return this.mediaPlayer.getAudioSessionId();
    }

    public boolean setPlaybackSpeed(float speed) {
        if (VERSION.SDK_INT < 23) {
            return false;
        }
        PlaybackParams params = new PlaybackParams();
        params.setSpeed(speed);
        this.mediaPlayer.setPlaybackParams(params);
        return true;
    }

    public void setAudioStreamType(int streamType) {
        this.mediaPlayer.setAudioStreamType(streamType);
    }

    public void setWakeMode(Context context, int mode) {
        this.mediaPlayer.setWakeMode(context, mode);
    }

    public boolean trackSelectionAvailable() {
        return false;
    }

    public void setTrack(RendererType trackType, int trackIndex) {
    }

    @Nullable
    public Map<RendererType, TrackGroupArray> getAvailableTracks() {
        return null;
    }

    public void setListenerMux(ListenerMux listenerMux) {
        this.listenerMux = listenerMux;
        this.mediaPlayer.setOnCompletionListener(listenerMux);
        this.mediaPlayer.setOnPreparedListener(listenerMux);
        this.mediaPlayer.setOnBufferingUpdateListener(listenerMux);
        this.mediaPlayer.setOnSeekCompleteListener(listenerMux);
        this.mediaPlayer.setOnErrorListener(listenerMux);
    }

    public void onMediaPrepared() {
        if (this.requestedSeek != 0) {
            seekTo(this.requestedSeek);
        }
    }
}
