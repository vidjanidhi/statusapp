package com.devbrackets.android.exomedia.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntRange;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.devbrackets.android.exomedia.R;
import com.devbrackets.android.exomedia.ui.animation.BottomViewHideShowAnimation;
import com.devbrackets.android.exomedia.util.ResourceUtil;
import com.devbrackets.android.exomedia.util.TimeFormatUtil;

@TargetApi(21)
public class VideoControlsLeanback extends VideoControls {
    protected static final int FAST_FORWARD_REWIND_AMOUNT = 10000;
    protected ButtonFocusChangeListener buttonFocusChangeListener = new ButtonFocusChangeListener();
    protected ViewGroup controlsParent;
    protected View currentFocus;
    protected ImageButton fastForwardButton;
    protected ProgressBar progressBar;
    protected ImageButton rewindButton;
    protected ImageView rippleIndicator;

    protected class ButtonFocusChangeListener implements OnFocusChangeListener {
        protected ButtonFocusChangeListener() {
        }

        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                VideoControlsLeanback.this.rippleIndicator.startAnimation(new RippleTranslateAnimation(getHorizontalDelta(view)));
            }
        }

        protected int getHorizontalDelta(View selectedView) {
            int[] position = new int[2];
            selectedView.getLocationOnScreen(position);
            int viewX = position[0];
            VideoControlsLeanback.this.rippleIndicator.getLocationOnScreen(position);
            return (viewX - ((VideoControlsLeanback.this.rippleIndicator.getWidth() - selectedView.getWidth()) / 2)) - position[0];
        }
    }

    protected class LeanbackInternalListener extends InternalListener {
        protected LeanbackInternalListener() {
            super();
        }

        public boolean onFastForwardClicked() {
            if (VideoControlsLeanback.this.videoView == null) {
                return false;
            }
            long newPosition = VideoControlsLeanback.this.videoView.getCurrentPosition() + 10000;
            if (newPosition > ((long) VideoControlsLeanback.this.progressBar.getMax())) {
                newPosition = (long) VideoControlsLeanback.this.progressBar.getMax();
            }
            VideoControlsLeanback.this.performSeek(newPosition);
            return true;
        }

        public boolean onRewindClicked() {
            if (VideoControlsLeanback.this.videoView == null) {
                return false;
            }
            long newPosition = VideoControlsLeanback.this.videoView.getCurrentPosition() - 10000;
            if (newPosition < 0) {
                newPosition = 0;
            }
            VideoControlsLeanback.this.performSeek(newPosition);
            return true;
        }
    }

    protected class RemoteKeyListener implements OnKeyListener {
        protected RemoteKeyListener() {
        }

        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (event.getAction() != 0) {
                return false;
            }
            switch (keyCode) {
                case 4:
                    if (VideoControlsLeanback.this.isVisible && VideoControlsLeanback.this.canViewHide && !VideoControlsLeanback.this.isLoading) {
                        VideoControlsLeanback.this.hide();
                        return true;
                    } else if (VideoControlsLeanback.this.controlsParent.getAnimation() != null) {
                        return true;
                    } else {
                        return false;
                    }
                case 19:
                    VideoControlsLeanback.this.showTemporary();
                    return true;
                case 20:
                    VideoControlsLeanback.this.hide();
                    return true;
                case 21:
                    VideoControlsLeanback.this.showTemporary();
                    VideoControlsLeanback.this.focusPrevious(VideoControlsLeanback.this.currentFocus);
                    return true;
                case 22:
                    VideoControlsLeanback.this.showTemporary();
                    VideoControlsLeanback.this.focusNext(VideoControlsLeanback.this.currentFocus);
                    return true;
                case 23:
                    VideoControlsLeanback.this.showTemporary();
                    VideoControlsLeanback.this.currentFocus.callOnClick();
                    return true;
                case 85:
                    VideoControlsLeanback.this.onPlayPauseClick();
                    return true;
                case 87:
                    VideoControlsLeanback.this.onNextClick();
                    return true;
                case 88:
                    VideoControlsLeanback.this.onPreviousClick();
                    return true;
                case 89:
                    VideoControlsLeanback.this.onRewindClick();
                    return true;
                case 90:
                    VideoControlsLeanback.this.onFastForwardClick();
                    return true;
                case 126:
                    if (VideoControlsLeanback.this.videoView == null || VideoControlsLeanback.this.videoView.isPlaying()) {
                        return false;
                    }
                    VideoControlsLeanback.this.videoView.start();
                    return true;
                case 127:
                    if (VideoControlsLeanback.this.videoView == null || !VideoControlsLeanback.this.videoView.isPlaying()) {
                        return false;
                    }
                    VideoControlsLeanback.this.videoView.pause();
                    return true;
                default:
                    return false;
            }
        }
    }

    protected class RippleTranslateAnimation extends TranslateAnimation implements AnimationListener {
        protected static final long DURATION = 250;
        protected int xDelta;

        public RippleTranslateAnimation(int xDelta) {
            super(0.0f, (float) xDelta, 0.0f, 0.0f);
            this.xDelta = xDelta;
            setDuration(DURATION);
            setAnimationListener(this);
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            VideoControlsLeanback.this.rippleIndicator.setX(VideoControlsLeanback.this.rippleIndicator.getX() + ((float) this.xDelta));
            VideoControlsLeanback.this.rippleIndicator.clearAnimation();
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }

    public VideoControlsLeanback(Context context) {
        super(context);
    }

    public VideoControlsLeanback(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoControlsLeanback(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public VideoControlsLeanback(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void setup(Context context) {
        super.setup(context);
        this.internalListener = new LeanbackInternalListener();
        registerForInput();
        setFocusable(true);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.playPauseButton.requestFocus();
        this.currentFocus = this.playPauseButton;
    }

    protected int getLayoutResource() {
        return R.layout.exomedia_default_controls_leanback;
    }

    public void setPosition(long position) {
        this.currentTimeTextView.setText(TimeFormatUtil.formatMs(position));
        this.progressBar.setProgress((int) position);
    }

    public void setDuration(long duration) {
        if (duration != ((long) this.progressBar.getMax())) {
            this.endTimeTextView.setText(TimeFormatUtil.formatMs(duration));
            this.progressBar.setMax((int) duration);
        }
    }

    public void updateProgress(@IntRange(from = 0) long position, @IntRange(from = 0) long duration, @IntRange(from = 0, to = 100) int bufferPercent) {
        this.progressBar.setSecondaryProgress((int) (((float) this.progressBar.getMax()) * (((float) bufferPercent) / 100.0f)));
        this.progressBar.setProgress((int) position);
        this.currentTimeTextView.setText(TimeFormatUtil.formatMs(position));
    }

    public void setRewindDrawable(Drawable drawable) {
        if (this.rewindButton != null) {
            this.rewindButton.setImageDrawable(drawable);
        }
    }

    public void setFastForwardDrawable(Drawable drawable) {
        if (this.fastForwardButton != null) {
            this.fastForwardButton.setImageDrawable(drawable);
        }
    }

    public void setRewindButtonEnabled(boolean enabled) {
        if (this.rewindButton != null) {
            this.rewindButton.setEnabled(enabled);
            this.enabledViews.put(R.id.exomedia_controls_rewind_btn, enabled);
        }
    }

    public void setFastForwardButtonEnabled(boolean enabled) {
        if (this.fastForwardButton != null) {
            this.fastForwardButton.setEnabled(enabled);
            this.enabledViews.put(R.id.exomedia_controls_fast_forward_btn, enabled);
        }
    }

    public void setRewindButtonRemoved(boolean removed) {
        if (this.rewindButton != null) {
            this.rewindButton.setVisibility(removed ? 8 : 0);
        }
    }

    public void setFastForwardButtonRemoved(boolean removed) {
        if (this.fastForwardButton != null) {
            this.fastForwardButton.setVisibility(removed ? 8 : 0);
        }
    }

    protected void retrieveViews() {
        super.retrieveViews();
        this.progressBar = (ProgressBar) findViewById(R.id.exomedia_controls_video_progress);
        this.rewindButton = (ImageButton) findViewById(R.id.exomedia_controls_rewind_btn);
        this.fastForwardButton = (ImageButton) findViewById(R.id.exomedia_controls_fast_forward_btn);
        this.rippleIndicator = (ImageView) findViewById(R.id.exomedia_controls_leanback_ripple);
        this.controlsParent = (ViewGroup) findViewById(R.id.exomedia_controls_parent);
    }

    protected void registerListeners() {
        super.registerListeners();
        this.rewindButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                VideoControlsLeanback.this.onRewindClick();
            }
        });
        this.fastForwardButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                VideoControlsLeanback.this.onFastForwardClick();
            }
        });
        this.previousButton.setOnFocusChangeListener(this.buttonFocusChangeListener);
        this.rewindButton.setOnFocusChangeListener(this.buttonFocusChangeListener);
        this.playPauseButton.setOnFocusChangeListener(this.buttonFocusChangeListener);
        this.fastForwardButton.setOnFocusChangeListener(this.buttonFocusChangeListener);
        this.nextButton.setOnFocusChangeListener(this.buttonFocusChangeListener);
    }

    protected void updateButtonDrawables() {
        super.updateButtonDrawables();
        this.rewindButton.setImageDrawable(ResourceUtil.tintList(getContext(), R.drawable.exomedia_ic_rewind_white, R.color.exomedia_default_controls_button_selector));
        this.fastForwardButton.setImageDrawable(ResourceUtil.tintList(getContext(), R.drawable.exomedia_ic_fast_forward_white, R.color.exomedia_default_controls_button_selector));
    }

    protected void animateVisibility(boolean toVisible) {
        if (this.isVisible != toVisible) {
            if (!this.isLoading) {
                this.controlsParent.startAnimation(new BottomViewHideShowAnimation(this.controlsParent, toVisible, 300));
            }
            this.isVisible = toVisible;
            onVisibilityChanged();
        }
    }

    protected void updateTextContainerVisibility() {
        if (this.isVisible) {
            boolean emptyText = isTextContainerEmpty();
            if (this.hideEmptyTextContainer && emptyText && this.textContainer.getVisibility() == 0) {
                this.textContainer.clearAnimation();
                this.textContainer.startAnimation(new BottomViewHideShowAnimation(this.textContainer, false, 300));
            } else if ((!this.hideEmptyTextContainer || !emptyText) && this.textContainer.getVisibility() != 0) {
                this.textContainer.clearAnimation();
                this.textContainer.startAnimation(new BottomViewHideShowAnimation(this.textContainer, true, 300));
            }
        }
    }

    public void showLoading(boolean initialLoad) {
        if (!this.isLoading) {
            this.isLoading = true;
            this.controlsContainer.setVisibility(8);
            this.rippleIndicator.setVisibility(8);
            this.loadingProgressBar.setVisibility(0);
            show();
        }
    }

    public void finishLoading() {
        boolean z = false;
        if (this.isLoading) {
            this.isLoading = false;
            this.controlsContainer.setVisibility(0);
            this.rippleIndicator.setVisibility(0);
            this.loadingProgressBar.setVisibility(8);
            if (this.videoView != null && this.videoView.isPlaying()) {
                z = true;
            }
            updatePlaybackState(z);
        }
    }

    protected void onRewindClick() {
        if (this.buttonsListener == null || !this.buttonsListener.onRewindClicked()) {
            this.internalListener.onRewindClicked();
        }
    }

    protected void onFastForwardClick() {
        if (this.buttonsListener == null || !this.buttonsListener.onFastForwardClicked()) {
            this.internalListener.onFastForwardClicked();
        }
    }

    protected void performSeek(long seekToTime) {
        if (this.seekListener == null || !this.seekListener.onSeekEnded(seekToTime)) {
            this.internalListener.onSeekEnded(seekToTime);
        }
    }

    protected void showTemporary() {
        show();
        if (this.videoView != null && this.videoView.isPlaying()) {
            hideDelayed();
        }
    }

    protected void registerForInput() {
        RemoteKeyListener remoteKeyListener = new RemoteKeyListener();
        setOnKeyListener(remoteKeyListener);
        this.playPauseButton.setOnKeyListener(remoteKeyListener);
        this.previousButton.setOnKeyListener(remoteKeyListener);
        this.nextButton.setOnKeyListener(remoteKeyListener);
        this.rewindButton.setOnKeyListener(remoteKeyListener);
        this.fastForwardButton.setOnKeyListener(remoteKeyListener);
    }

    protected void focusNext(View view) {
        int nextId = view.getNextFocusRightId();
        if (nextId != -1) {
            View nextView = findViewById(nextId);
            if (nextView.getVisibility() != 0) {
                focusNext(nextView);
                return;
            }
            nextView.requestFocus();
            this.currentFocus = nextView;
            this.buttonFocusChangeListener.onFocusChange(nextView, true);
        }
    }

    protected void focusPrevious(View view) {
        int previousId = view.getNextFocusLeftId();
        if (previousId != -1) {
            View previousView = findViewById(previousId);
            if (previousView.getVisibility() != 0) {
                focusPrevious(previousView);
                return;
            }
            previousView.requestFocus();
            this.currentFocus = previousView;
            this.buttonFocusChangeListener.onFocusChange(previousView, true);
        }
    }
}
