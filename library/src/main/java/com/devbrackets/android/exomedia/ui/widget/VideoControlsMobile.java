package com.devbrackets.android.exomedia.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.devbrackets.android.exomedia.R;
import com.devbrackets.android.exomedia.ui.animation.BottomViewHideShowAnimation;
import com.devbrackets.android.exomedia.ui.animation.TopViewHideShowAnimation;
import com.devbrackets.android.exomedia.util.TimeFormatUtil;
import java.util.LinkedList;
import java.util.List;

public class VideoControlsMobile extends VideoControls {
    protected LinearLayout extraViewsContainer;
    protected SeekBar seekBar;
    protected boolean userInteracting = false;

    protected class SeekBarChanged implements OnSeekBarChangeListener {
        private long seekToTime;

        protected SeekBarChanged() {
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                this.seekToTime = (long) progress;
                if (VideoControlsMobile.this.currentTimeTextView != null) {
                    VideoControlsMobile.this.currentTimeTextView.setText(TimeFormatUtil.formatMs(this.seekToTime));
                }
            }
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            VideoControlsMobile.this.userInteracting = true;
            if (VideoControlsMobile.this.seekListener == null || !VideoControlsMobile.this.seekListener.onSeekStarted()) {
                VideoControlsMobile.this.internalListener.onSeekStarted();
            }
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            VideoControlsMobile.this.userInteracting = false;
            if (VideoControlsMobile.this.seekListener == null || !VideoControlsMobile.this.seekListener.onSeekEnded(this.seekToTime)) {
                VideoControlsMobile.this.internalListener.onSeekEnded(this.seekToTime);
            }
        }
    }

    public VideoControlsMobile(Context context) {
        super(context);
    }

    public VideoControlsMobile(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoControlsMobile(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public VideoControlsMobile(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected int getLayoutResource() {
        return R.layout.exomedia_default_controls_mobile;
    }

    public void setPosition(@IntRange(from = 0) long position) {
        this.currentTimeTextView.setText(TimeFormatUtil.formatMs(position));
        this.seekBar.setProgress((int) position);
    }

    public void setDuration(@IntRange(from = 0) long duration) {
        if (duration != ((long) this.seekBar.getMax())) {
            this.endTimeTextView.setText(TimeFormatUtil.formatMs(duration));
            this.seekBar.setMax((int) duration);
        }
    }

    public void updateProgress(@IntRange(from = 0) long position, @IntRange(from = 0) long duration, @IntRange(from = 0, to = 100) int bufferPercent) {
        if (!this.userInteracting) {
            this.seekBar.setSecondaryProgress((int) (((float) this.seekBar.getMax()) * (((float) bufferPercent) / 100.0f)));
            this.seekBar.setProgress((int) position);
            this.currentTimeTextView.setText(TimeFormatUtil.formatMs(position));
        }
    }

    protected void retrieveViews() {
        super.retrieveViews();
        this.seekBar = (SeekBar) findViewById(R.id.exomedia_controls_video_seek);
        this.extraViewsContainer = (LinearLayout) findViewById(R.id.exomedia_controls_extra_container);
    }

    protected void registerListeners() {
        super.registerListeners();
        this.seekBar.setOnSeekBarChangeListener(new SeekBarChanged());
    }

    public void addExtraView(@NonNull View view) {
        this.extraViewsContainer.addView(view);
    }

    public void removeExtraView(@NonNull View view) {
        this.extraViewsContainer.removeView(view);
    }

    @NonNull
    public List<View> getExtraViews() {
        int childCount = this.extraViewsContainer.getChildCount();
        if (childCount <= 0) {
            return super.getExtraViews();
        }
        List<View> children = new LinkedList();
        for (int i = 0; i < childCount; i++) {
            children.add(this.extraViewsContainer.getChildAt(i));
        }
        return children;
    }

    public void hideDelayed(long delay) {
        this.hideDelay = delay;
        if (delay >= 0 && this.canViewHide && !this.isLoading && !this.userInteracting) {
            this.visibilityHandler.postDelayed(new Runnable() {
                public void run() {
                    VideoControlsMobile.this.animateVisibility(false);
                }
            }, delay);
        }
    }

    protected void animateVisibility(boolean toVisible) {
        if (this.isVisible != toVisible) {
            if (!(this.hideEmptyTextContainer && isTextContainerEmpty())) {
                this.textContainer.startAnimation(new TopViewHideShowAnimation(this.textContainer, toVisible, 300));
            }
            if (!this.isLoading) {
                this.controlsContainer.startAnimation(new BottomViewHideShowAnimation(this.controlsContainer, toVisible, 300));
            }
            this.isVisible = toVisible;
            onVisibilityChanged();
        }
    }

    protected void updateTextContainerVisibility() {
        if (this.isVisible) {
            boolean emptyText = isTextContainerEmpty();
            if (this.hideEmptyTextContainer && emptyText && this.textContainer.getVisibility() == 0) {
                this.textContainer.clearAnimation();
                this.textContainer.startAnimation(new TopViewHideShowAnimation(this.textContainer, false, 300));
            } else if ((!this.hideEmptyTextContainer || !emptyText) && this.textContainer.getVisibility() != 0) {
                this.textContainer.clearAnimation();
                this.textContainer.startAnimation(new TopViewHideShowAnimation(this.textContainer, true, 300));
            }
        }
    }

    public void showLoading(boolean initialLoad) {
        if (!this.isLoading) {
            this.isLoading = true;
            this.loadingProgressBar.setVisibility(0);
            if (initialLoad) {
                this.controlsContainer.setVisibility(8);
            } else {
                this.playPauseButton.setEnabled(false);
                this.previousButton.setEnabled(false);
                this.nextButton.setEnabled(false);
            }
            show();
        }
    }

    public void finishLoading() {
        boolean z = true;
        if (this.isLoading) {
            this.isLoading = false;
            this.loadingProgressBar.setVisibility(8);
            this.controlsContainer.setVisibility(0);
            this.playPauseButton.setEnabled(true);
            this.previousButton.setEnabled(this.enabledViews.get(R.id.exomedia_controls_previous_btn, true));
            this.nextButton.setEnabled(this.enabledViews.get(R.id.exomedia_controls_next_btn, true));
            if (this.videoView == null || !this.videoView.isPlaying()) {
                z = false;
            }
            updatePlaybackState(z);
        }
    }
}
