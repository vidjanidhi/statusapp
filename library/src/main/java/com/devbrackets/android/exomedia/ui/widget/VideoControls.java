package com.devbrackets.android.exomedia.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.IntRange;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.devbrackets.android.exomedia.R;
import com.devbrackets.android.exomedia.listener.VideoControlsButtonListener;
import com.devbrackets.android.exomedia.listener.VideoControlsSeekListener;
import com.devbrackets.android.exomedia.listener.VideoControlsVisibilityListener;
import com.devbrackets.android.exomedia.util.Repeater;
import com.devbrackets.android.exomedia.util.Repeater.RepeatListener;
import com.devbrackets.android.exomedia.util.ResourceUtil;
import java.util.LinkedList;
import java.util.List;

public abstract class VideoControls extends RelativeLayout {
    protected static final long CONTROL_VISIBILITY_ANIMATION_LENGTH = 300;
    public static final int DEFAULT_CONTROL_HIDE_DELAY = 2000;
    @Nullable
    protected VideoControlsButtonListener buttonsListener;
    protected boolean canViewHide = true;
    protected ViewGroup controlsContainer;
    protected TextView currentTimeTextView;
    protected TextView descriptionTextView;
    @NonNull
    protected SparseBooleanArray enabledViews = new SparseBooleanArray();
    protected TextView endTimeTextView;
    protected ImageButton exomedia_controls_screen_change;
    protected long hideDelay = 2000;
    protected boolean hideEmptyTextContainer = true;
    @NonNull
    protected InternalListener internalListener = new InternalListener();
    protected boolean isLoading = false;
    protected boolean isVisible = true;
    protected ProgressBar loadingProgressBar;
    protected ImageButton nextButton;
    private OnClickListener onScreenChangeClickListener;
    protected Drawable pauseDrawable;
    protected Drawable playDrawable;
    protected ImageButton playPauseButton;
    protected ImageButton previousButton;
    @NonNull
    protected Repeater progressPollRepeater = new Repeater();
    @Nullable
    protected VideoControlsSeekListener seekListener;
    protected TextView subTitleTextView;
    protected ViewGroup textContainer;
    protected TextView titleTextView;
    @Nullable
    protected VideoView videoView;
    @NonNull
    protected Handler visibilityHandler = new Handler();
    @Nullable
    protected VideoControlsVisibilityListener visibilityListener;

    protected class InternalListener implements VideoControlsSeekListener, VideoControlsButtonListener {
        protected boolean pausedForSeek = false;

        protected InternalListener() {
        }

        public boolean onPlayPauseClicked() {
            if (VideoControls.this.videoView == null) {
                return false;
            }
            if (VideoControls.this.videoView.isPlaying()) {
                VideoControls.this.videoView.pause();
            } else {
                VideoControls.this.videoView.start();
            }
            return true;
        }

        public boolean onPreviousClicked() {
            return false;
        }

        public boolean onNextClicked() {
            return false;
        }

        public boolean onRewindClicked() {
            return false;
        }

        public boolean onFastForwardClicked() {
            return false;
        }

        public boolean onSeekStarted() {
            if (VideoControls.this.videoView == null) {
                return false;
            }
            if (VideoControls.this.videoView.isPlaying()) {
                this.pausedForSeek = true;
                VideoControls.this.videoView.pause();
            }
            VideoControls.this.show();
            return true;
        }

        public boolean onSeekEnded(long seekTime) {
            if (VideoControls.this.videoView == null) {
                return false;
            }
            VideoControls.this.videoView.seekTo(seekTime);
            if (this.pausedForSeek) {
                this.pausedForSeek = false;
                VideoControls.this.videoView.start();
                VideoControls.this.hideDelayed();
            }
            return true;
        }
    }

    protected abstract void animateVisibility(boolean z);

    public abstract void finishLoading();

    @LayoutRes
    protected abstract int getLayoutResource();

    public abstract void setDuration(@IntRange(from = 0) long j);

    public abstract void setPosition(@IntRange(from = 0) long j);

    public abstract void showLoading(boolean z);

    public abstract void updateProgress(@IntRange(from = 0) long j, @IntRange(from = 0) long j2, @IntRange(from = 0, to = 100) int i);

    protected abstract void updateTextContainerVisibility();

    public VideoControls(Context context) {
        super(context);
        setup(context);
    }

    public VideoControls(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context);
    }

    public VideoControls(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup(context);
    }

    @TargetApi(21)
    public VideoControls(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setup(context);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.progressPollRepeater.setRepeatListener(new RepeatListener() {
            public void onRepeat() {
                VideoControls.this.updateProgress();
            }
        });
        if (this.videoView != null && this.videoView.isPlaying()) {
            updatePlaybackState(true);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.progressPollRepeater.stop();
        this.progressPollRepeater.setRepeatListener(null);
    }

    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 1) {
            this.exomedia_controls_screen_change.setImageResource(R.drawable.jc_enlarge);
        } else if (newConfig.orientation == 2) {
            this.exomedia_controls_screen_change.setImageResource(R.drawable.jc_shrink);
        }
    }

    public void setOnScreenChangeClickListener(OnClickListener onScreenChangeClickListener) {
        this.onScreenChangeClickListener = onScreenChangeClickListener;
    }

    public void setVideoView(@Nullable VideoView VideoView) {
        this.videoView = VideoView;
    }

    public void setSeekListener(@Nullable VideoControlsSeekListener callbacks) {
        this.seekListener = callbacks;
    }

    public void setButtonListener(@Nullable VideoControlsButtonListener callback) {
        this.buttonsListener = callback;
    }

    public void setVisibilityListener(@Nullable VideoControlsVisibilityListener callbacks) {
        this.visibilityListener = callbacks;
    }

    public void updatePlaybackState(boolean isPlaying) {
        updatePlayPauseImage(isPlaying);
        this.progressPollRepeater.start();
        if (isPlaying) {
            hideDelayed();
        } else {
            show();
        }
    }

    public void setTitle(@Nullable CharSequence title) {
        this.titleTextView.setText(title);
        updateTextContainerVisibility();
    }

    public void setSubTitle(@Nullable CharSequence subTitle) {
        this.subTitleTextView.setText(subTitle);
        updateTextContainerVisibility();
    }

    public void setDescription(@Nullable CharSequence description) {
        this.descriptionTextView.setText(description);
        updateTextContainerVisibility();
    }

    public void setPlayPauseDrawables(Drawable playDrawable, Drawable pauseDrawable) {
        this.playDrawable = playDrawable;
        this.pauseDrawable = pauseDrawable;
        boolean z = this.videoView != null && this.videoView.isPlaying();
        updatePlayPauseImage(z);
    }

    public void setPreviousDrawable(Drawable drawable) {
        this.previousButton.setImageDrawable(drawable);
    }

    public void setNextDrawable(Drawable drawable) {
        this.nextButton.setImageDrawable(drawable);
    }

    public void setRewindDrawable(Drawable drawable) {
    }

    public void setFastForwardDrawable(Drawable drawable) {
    }

    public void updatePlayPauseImage(boolean isPlaying) {
        this.playPauseButton.setImageDrawable(isPlaying ? this.pauseDrawable : this.playDrawable);
    }

    public void setPreviousButtonEnabled(boolean enabled) {
        this.previousButton.setEnabled(enabled);
        this.enabledViews.put(R.id.exomedia_controls_previous_btn, enabled);
    }

    public void setNextButtonEnabled(boolean enabled) {
        this.nextButton.setEnabled(enabled);
        this.enabledViews.put(R.id.exomedia_controls_next_btn, enabled);
    }

    public void setRewindButtonEnabled(boolean enabled) {
    }

    public void setFastForwardButtonEnabled(boolean enabled) {
    }

    public void setPreviousButtonRemoved(boolean removed) {
        this.previousButton.setVisibility(removed ? 8 : 0);
    }

    public void setNextButtonRemoved(boolean removed) {
        this.nextButton.setVisibility(removed ? 8 : 0);
    }

    public void setRewindButtonRemoved(boolean removed) {
    }

    public void setFastForwardButtonRemoved(boolean removed) {
    }

    public void addExtraView(@NonNull View view) {
    }

    public void removeExtraView(@NonNull View view) {
    }

    @NonNull
    public List<View> getExtraViews() {
        return new LinkedList();
    }

    public void show() {
        this.visibilityHandler.removeCallbacksAndMessages(null);
        clearAnimation();
        animateVisibility(true);
    }

    public void hide() {
        if (this.canViewHide && !this.isLoading) {
            this.visibilityHandler.removeCallbacksAndMessages(null);
            clearAnimation();
            animateVisibility(false);
        }
    }

    public void hideDelayed() {
        hideDelayed(this.hideDelay);
    }

    public void hideDelayed(long delay) {
        this.hideDelay = delay;
        if (delay >= 0 && this.canViewHide && !this.isLoading) {
            this.visibilityHandler.postDelayed(new Runnable() {
                public void run() {
                    VideoControls.this.hide();
                }
            }, delay);
        }
    }

    public void setHideDelay(long delay) {
        this.hideDelay = delay;
    }

    public void setCanHide(boolean canHide) {
        this.canViewHide = canHide;
    }

    public void setHideEmptyTextContainer(boolean hide) {
        this.hideEmptyTextContainer = hide;
        updateTextContainerVisibility();
    }

    public boolean isVisible() {
        return this.isVisible;
    }

    protected void retrieveViews() {
        this.currentTimeTextView = (TextView) findViewById(R.id.exomedia_controls_current_time);
        this.endTimeTextView = (TextView) findViewById(R.id.exomedia_controls_end_time);
        this.titleTextView = (TextView) findViewById(R.id.exomedia_controls_title);
        this.subTitleTextView = (TextView) findViewById(R.id.exomedia_controls_sub_title);
        this.descriptionTextView = (TextView) findViewById(R.id.exomedia_controls_description);
        this.playPauseButton = (ImageButton) findViewById(R.id.exomedia_controls_play_pause_btn);
        this.exomedia_controls_screen_change = (ImageButton) findViewById(R.id.exomedia_controls_screen_change);
        this.previousButton = (ImageButton) findViewById(R.id.exomedia_controls_previous_btn);
        this.nextButton = (ImageButton) findViewById(R.id.exomedia_controls_next_btn);
        this.loadingProgressBar = (ProgressBar) findViewById(R.id.exomedia_controls_video_loading);
        this.controlsContainer = (ViewGroup) findViewById(R.id.exomedia_controls_interactive_container);
        this.textContainer = (ViewGroup) findViewById(R.id.exomedia_controls_text_container);
        int orientation = getContext().getResources().getConfiguration().orientation;
        if (orientation == 1) {
            this.exomedia_controls_screen_change.setImageResource(R.drawable.jc_enlarge);
        } else if (orientation == 2) {
            this.exomedia_controls_screen_change.setImageResource(R.drawable.jc_shrink);
        }
    }

    protected void registerListeners() {
        this.playPauseButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                VideoControls.this.onPlayPauseClick();
            }
        });
        this.previousButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                VideoControls.this.onPreviousClick();
            }
        });
        this.nextButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                VideoControls.this.onNextClick();
            }
        });
        this.exomedia_controls_screen_change.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (VideoControls.this.onScreenChangeClickListener != null) {
                    VideoControls.this.onScreenChangeClickListener.onClick(view);
                }
            }
        });
    }

    protected void updateButtonDrawables() {
        this.playDrawable = ResourceUtil.tintList(getContext(), R.drawable.exomedia_ic_play_arrow_white, R.color.exomedia_default_controls_button_selector);
        this.pauseDrawable = ResourceUtil.tintList(getContext(), R.drawable.exomedia_ic_pause_white, R.color.exomedia_default_controls_button_selector);
        this.playPauseButton.setImageDrawable(this.playDrawable);
        this.previousButton.setImageDrawable(ResourceUtil.tintList(getContext(), R.drawable.exomedia_ic_skip_previous_white, R.color.exomedia_default_controls_button_selector));
        this.nextButton.setImageDrawable(ResourceUtil.tintList(getContext(), R.drawable.exomedia_ic_skip_next_white, R.color.exomedia_default_controls_button_selector));
    }

    protected void onPlayPauseClick() {
        if (this.buttonsListener == null || !this.buttonsListener.onPlayPauseClicked()) {
            this.internalListener.onPlayPauseClicked();
        }
    }

    protected void onPreviousClick() {
        if (this.buttonsListener == null || !this.buttonsListener.onPreviousClicked()) {
            this.internalListener.onPreviousClicked();
        }
    }

    protected void onNextClick() {
        if (this.buttonsListener == null || !this.buttonsListener.onNextClicked()) {
            this.internalListener.onNextClicked();
        }
    }

    protected void setup(Context context) {
        View.inflate(context, getLayoutResource(), this);
        retrieveViews();
        registerListeners();
        updateButtonDrawables();
    }

    protected boolean isTextContainerEmpty() {
        if (this.titleTextView.getText() != null && this.titleTextView.getText().length() > 0) {
            return false;
        }
        if (this.subTitleTextView.getText() != null && this.subTitleTextView.getText().length() > 0) {
            return false;
        }
        if (this.descriptionTextView.getText() == null || this.descriptionTextView.getText().length() <= 0) {
            return true;
        }
        return false;
    }

    protected void onVisibilityChanged() {
        if (this.visibilityListener != null) {
            if (this.isVisible) {
                this.visibilityListener.onControlsShown();
            } else {
                this.visibilityListener.onControlsHidden();
            }
        }
    }

    protected void updateProgress() {
        if (this.videoView != null) {
            updateProgress(this.videoView.getCurrentPosition(), this.videoView.getDuration(), this.videoView.getBufferPercentage());
        }
    }
}
