package com.devbrackets.android.exomedia.core.exoplayer;

import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.Surface;
import com.devbrackets.android.exomedia.ExoMedia.Data;
import com.devbrackets.android.exomedia.ExoMedia.RendererType;
import com.devbrackets.android.exomedia.core.listener.CaptionListener;
import com.devbrackets.android.exomedia.core.listener.ExoPlayerListener;
import com.devbrackets.android.exomedia.core.listener.InternalErrorListener;
import com.devbrackets.android.exomedia.core.listener.MetadataListener;
import com.devbrackets.android.exomedia.core.renderer.RendererProvider;
import com.devbrackets.android.exomedia.core.source.MediaSourceProvider;
import com.devbrackets.android.exomedia.listener.OnBufferUpdateListener;
import com.devbrackets.android.exomedia.util.Repeater;
import com.devbrackets.android.exomedia.util.Repeater.RepeatListener;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayer.ExoPlayerMessage;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player.EventListener;
import com.google.android.exoplayer2.Renderer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes.Builder;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.ExoMediaDrm.KeyRequest;
import com.google.android.exoplayer2.drm.ExoMediaDrm.ProvisionRequest;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.MediaDrmCallback;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.MetadataRenderer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextRenderer.Output;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection.Factory;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.FixedTrackSelection;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.SelectionOverride;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ExoMediaPlayer implements EventListener {
    private static final int BUFFER_REPEAT_DELAY = 1000;
    private static final String TAG = "ExoMediaPlayer";
    private static final int WAKE_LOCK_TIMEOUT = 1000;
    @NonNull
    private final Factory adaptiveTrackSelectionFactory;
    private int audioSessionId = 0;
    @NonNull
    private DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
    @NonNull
    private Repeater bufferRepeater = new Repeater();
    @Nullable
    private OnBufferUpdateListener bufferUpdateListener;
    @NonNull
    private CapabilitiesListener capabilitiesListener = new CapabilitiesListener();
    @Nullable
    private CaptionListener captionListener;
    @NonNull
    private final Context context;
    @Nullable
    private MediaDrmCallback drmCallback;
    @Nullable
    private InternalErrorListener internalErrorListener;
    @NonNull
    private final CopyOnWriteArrayList<ExoPlayerListener> listeners = new CopyOnWriteArrayList();
    @NonNull
    private final Handler mainHandler;
    @Nullable
    private MediaSource mediaSource;
    @NonNull
    private MediaSourceProvider mediaSourceProvider = new MediaSourceProvider();
    @Nullable
    private MetadataListener metadataListener;
    @NonNull
    private final ExoPlayer player;
    private boolean prepared = false;
    @NonNull
    private List<Renderer> renderers = new LinkedList();
    @NonNull
    private StateStore stateStore = new StateStore();
    @NonNull
    private final AtomicBoolean stopped = new AtomicBoolean();
    @Nullable
    private Surface surface;
    @NonNull
    private final DefaultTrackSelector trackSelector;
    @Nullable
    private WakeLock wakeLock = null;

    private class BufferRepeatListener implements RepeatListener {
        private BufferRepeatListener() {
        }

        public void onRepeat() {
            if (ExoMediaPlayer.this.bufferUpdateListener != null) {
                ExoMediaPlayer.this.bufferUpdateListener.onBufferingUpdate(ExoMediaPlayer.this.getBufferedPercentage());
            }
        }
    }

    private class CapabilitiesListener implements DefaultDrmSessionManager.EventListener {
        private CapabilitiesListener() {
        }

        public void onDrmKeysLoaded() {
        }

        public void onDrmKeysRestored() {
        }

        public void onDrmKeysRemoved() {
        }

        public void onDrmSessionManagerError(Exception e) {
            if (ExoMediaPlayer.this.internalErrorListener != null) {
                ExoMediaPlayer.this.internalErrorListener.onDrmSessionManagerError(e);
            }
        }
    }

    private class ComponentListener implements VideoRendererEventListener, AudioRendererEventListener, Output, MetadataRenderer.Output {
        private ComponentListener() {
        }

        public void onAudioEnabled(DecoderCounters counters) {
        }

        public void onAudioDisabled(DecoderCounters counters) {
            ExoMediaPlayer.this.audioSessionId = 0;
        }

        public void onAudioSessionId(int sessionId) {
            ExoMediaPlayer.this.audioSessionId = sessionId;
        }

        public void onAudioDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {
        }

        public void onAudioInputFormatChanged(Format format) {
        }

        public void onAudioTrackUnderrun(int bufferSize, long bufferSizeMs, long elapsedSinceLastFeedMs) {
            if (ExoMediaPlayer.this.internalErrorListener != null) {
                ExoMediaPlayer.this.internalErrorListener.onAudioTrackUnderrun(bufferSize, bufferSizeMs, elapsedSinceLastFeedMs);
            }
        }

        public void onVideoEnabled(DecoderCounters counters) {
        }

        public void onVideoDisabled(DecoderCounters counters) {
        }

        public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {
        }

        public void onVideoInputFormatChanged(Format format) {
        }

        public void onDroppedFrames(int count, long elapsedMs) {
        }

        public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
            Iterator it = ExoMediaPlayer.this.listeners.iterator();
            while (it.hasNext()) {
                ((ExoPlayerListener) it.next()).onVideoSizeChanged(width, height, unappliedRotationDegrees, pixelWidthHeightRatio);
            }
        }

        public void onRenderedFirstFrame(Surface surface) {
        }

        public void onMetadata(Metadata metadata) {
            if (ExoMediaPlayer.this.metadataListener != null) {
                ExoMediaPlayer.this.metadataListener.onMetadata(metadata);
            }
        }

        public void onCues(List<Cue> cues) {
            if (ExoMediaPlayer.this.captionListener != null) {
                ExoMediaPlayer.this.captionListener.onCues(cues);
            }
        }
    }

    private class DelegatedMediaDrmCallback implements MediaDrmCallback {
        private DelegatedMediaDrmCallback() {
        }

        public byte[] executeProvisionRequest(UUID uuid, ProvisionRequest request) throws Exception {
            return ExoMediaPlayer.this.drmCallback != null ? ExoMediaPlayer.this.drmCallback.executeProvisionRequest(uuid, request) : new byte[0];
        }

        public byte[] executeKeyRequest(UUID uuid, KeyRequest request) throws Exception {
            return ExoMediaPlayer.this.drmCallback != null ? ExoMediaPlayer.this.drmCallback.executeKeyRequest(uuid, request) : new byte[0];
        }
    }

    private static class StateStore {
        public static final int FLAG_PLAY_WHEN_READY = -268435456;
        public static final int STATE_SEEKING = 100;
        private int[] prevStates;

        private StateStore() {
            this.prevStates = new int[]{1, 1, 1, 1};
        }

        public void reset() {
            for (int i = 0; i < this.prevStates.length; i++) {
                this.prevStates[i] = 1;
            }
        }

        public void setMostRecentState(boolean playWhenReady, int state) {
            if (this.prevStates[3] != getState(playWhenReady, state)) {
                this.prevStates[0] = this.prevStates[1];
                this.prevStates[1] = this.prevStates[2];
                this.prevStates[2] = this.prevStates[3];
                this.prevStates[3] = state;
            }
        }

        public int getState(boolean playWhenReady, int state) {
            return (playWhenReady ? FLAG_PLAY_WHEN_READY : 0) | state;
        }

        public int getMostRecentState() {
            return this.prevStates[3];
        }

        public boolean isLastReportedPlayWhenReady() {
            return (this.prevStates[3] & FLAG_PLAY_WHEN_READY) != 0;
        }

        public boolean matchesHistory(@Size(max = 4, min = 1) int[] states, boolean ignorePlayWhenReady) {
            boolean flag = true;
            int andFlag = ignorePlayWhenReady ? 268435455 : -1;
            int startIndex = this.prevStates.length - states.length;
            for (int i = startIndex; i < prevStates.length; i++) {
                flag &= (prevStates[i] & andFlag) == (states[i - startIndex] & andFlag);
            }

            return flag;
        }
    }

    public ExoMediaPlayer(@NonNull Context context) {
        this.context = context;
        this.bufferRepeater.setRepeaterDelay(1000);
        this.bufferRepeater.setRepeatListener(new BufferRepeatListener());
        this.mainHandler = new Handler();
        ComponentListener componentListener = new ComponentListener();
        RendererProvider rendererProvider = new RendererProvider(context, this.mainHandler, componentListener, componentListener, componentListener, componentListener);
        rendererProvider.setDrmSessionManager(generateDrmSessionManager());
        this.renderers = rendererProvider.generate();
        this.adaptiveTrackSelectionFactory = new Factory(this.bandwidthMeter);
        this.trackSelector = new DefaultTrackSelector(this.adaptiveTrackSelectionFactory);
        this.player = ExoPlayerFactory.newInstance((Renderer[]) this.renderers.toArray(new Renderer[this.renderers.size()]), this.trackSelector, Data.loadControl != null ? Data.loadControl : new DefaultLoadControl());
        this.player.addListener(this);
    }

    public void onTimelineChanged(Timeline timeline, Object manifest) {
    }

    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
    }

    public void onLoadingChanged(boolean isLoading) {
    }

    public void onRepeatModeChanged(int repeatMode) {
    }

    public void onPositionDiscontinuity() {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public void onPlayerStateChanged(boolean playWhenReady, int state) {
        reportPlayerState();
    }

    public void onPlayerError(ExoPlaybackException exception) {
        Iterator it = this.listeners.iterator();
        while (it.hasNext()) {
            ((ExoPlayerListener) it.next()).onError(this, exception);
        }
    }

    public void setDrmCallback(@Nullable MediaDrmCallback drmCallback) {
        this.drmCallback = drmCallback;
    }

    public void setUri(@Nullable Uri uri) {
        setMediaSource(uri != null ? this.mediaSourceProvider.generate(this.context, this.mainHandler, uri, this.bandwidthMeter) : null);
    }

    public void setMediaSource(@Nullable MediaSource source) {
        this.mediaSource = source;
        this.prepared = false;
        prepare();
    }

    public void addListener(ExoPlayerListener listener) {
        if (listener != null) {
            this.listeners.add(listener);
        }
    }

    public void removeListener(ExoPlayerListener listener) {
        if (listener != null) {
            this.listeners.remove(listener);
        }
    }

    public void setBufferUpdateListener(@Nullable OnBufferUpdateListener listener) {
        this.bufferUpdateListener = listener;
        setBufferRepeaterStarted(listener != null);
    }

    public void setInternalErrorListener(@Nullable InternalErrorListener listener) {
        this.internalErrorListener = listener;
    }

    public void setCaptionListener(@Nullable CaptionListener listener) {
        this.captionListener = listener;
    }

    public void setMetadataListener(@Nullable MetadataListener listener) {
        this.metadataListener = listener;
    }

    public void setSurface(@Nullable Surface surface) {
        this.surface = surface;
        sendMessage(2, 1, surface, false);
    }

    @Nullable
    public Surface getSurface() {
        return this.surface;
    }

    public void blockingClearSurface() {
        if (this.surface != null) {
            this.surface.release();
        }
        this.surface = null;
        sendMessage(2, 1, null, true);
    }

    @Nullable
    public Map<RendererType, TrackGroupArray> getAvailableTracks() {
        int i = 0;
        if (getPlaybackState() == 1) {
            return null;
        }
        Map<RendererType, TrackGroupArray> trackMap = new ArrayMap();
        MappedTrackInfo mappedTrackInfo = this.trackSelector.getCurrentMappedTrackInfo();
        if (mappedTrackInfo == null) {
            return trackMap;
        }
        RendererType[] types = new RendererType[]{RendererType.AUDIO, RendererType.VIDEO, RendererType.CLOSED_CAPTION, RendererType.METADATA};
        int length = types.length;
        while (i < length) {
            RendererType type = types[i];
            int exoPlayerTrackIndex = getExoPlayerTrackType(type);
            if (mappedTrackInfo.length > exoPlayerTrackIndex) {
                trackMap.put(type, mappedTrackInfo.getTrackGroups(exoPlayerTrackIndex));
            }
            i++;
        }
        return trackMap;
    }

    public int getSelectedTrackIndex(@NonNull RendererType type) {
        int exoPlayerTrackIndex = getExoPlayerTrackType(type);
        MappedTrackInfo mappedTrackInfo = this.trackSelector.getCurrentMappedTrackInfo();
        TrackGroupArray trackGroupArray = mappedTrackInfo == null ? null : mappedTrackInfo.getTrackGroups(exoPlayerTrackIndex);
        if (trackGroupArray == null || trackGroupArray.length == 0) {
            return -1;
        }
        SelectionOverride selectionOverride = this.trackSelector.getSelectionOverride(exoPlayerTrackIndex, trackGroupArray);
        if (selectionOverride == null || selectionOverride.groupIndex != exoPlayerTrackIndex || selectionOverride.length <= 0) {
            return -1;
        }
        return selectionOverride.tracks[0];
    }

    public void setSelectedTrack(@NonNull RendererType type, int index) {
        int exoPlayerTrackIndex = getExoPlayerTrackType(type);
        MappedTrackInfo mappedTrackInfo = this.trackSelector.getCurrentMappedTrackInfo();
        TrackGroupArray trackGroupArray = mappedTrackInfo == null ? null : mappedTrackInfo.getTrackGroups(exoPlayerTrackIndex);
        if (trackGroupArray != null && trackGroupArray.length != 0) {
            int[] tracks = new int[]{index};
            this.trackSelector.setSelectionOverride(exoPlayerTrackIndex, trackGroupArray, new SelectionOverride(tracks.length == 1 ? new FixedTrackSelection.Factory() : this.adaptiveTrackSelectionFactory, exoPlayerTrackIndex, tracks));
        }
    }

    public void setVolume(@FloatRange(from = 0.0d, to = 1.0d) float volume) {
        sendMessage(1, 2, Float.valueOf(volume));
    }

    public void setAudioStreamType(int streamType) {
        int usage = Util.getAudioUsageForStreamType(streamType);
        sendMessage(1, 3, new Builder().setUsage(usage).setContentType(Util.getAudioContentTypeForStreamType(streamType)).build());
    }

    public void forcePrepare() {
        this.prepared = false;
    }

    public void prepare() {
        if (!this.prepared && this.mediaSource != null) {
            if (!this.renderers.isEmpty()) {
                this.player.stop();
            }
            this.stateStore.reset();
            this.player.prepare(this.mediaSource);
            this.prepared = true;
            this.stopped.set(false);
        }
    }

    public void stop() {
        if (!this.stopped.getAndSet(true)) {
            this.player.setPlayWhenReady(false);
            this.player.stop();
        }
    }

    public void setPlayWhenReady(boolean playWhenReady) {
        this.player.setPlayWhenReady(playWhenReady);
        stayAwake(playWhenReady);
    }

    public void seekTo(long positionMs) {
        this.player.seekTo(positionMs);
        this.stateStore.setMostRecentState(this.stateStore.isLastReportedPlayWhenReady(), 100);
    }

    public boolean restart() {
        int playbackState = getPlaybackState();
        if (playbackState != 1 && playbackState != 4) {
            return false;
        }
        seekTo(0);
        setPlayWhenReady(true);
        forcePrepare();
        prepare();
        return true;
    }

    public void release() {
        setBufferRepeaterStarted(false);
        this.listeners.clear();
        this.surface = null;
        this.player.release();
        stayAwake(false);
    }

    public int getPlaybackState() {
        return this.player.getPlaybackState();
    }

    public int getAudioSessionId() {
        return this.audioSessionId;
    }

    public boolean setPlaybackSpeed(float speed) {
        this.player.setPlaybackParameters(new PlaybackParameters(speed, 1.0f));
        return true;
    }

    public long getCurrentPosition() {
        return this.player.getCurrentPosition();
    }

    public long getDuration() {
        return this.player.getDuration();
    }

    public int getBufferedPercentage() {
        return this.player.getBufferedPercentage();
    }

    public boolean getPlayWhenReady() {
        return this.player.getPlayWhenReady();
    }

    public void setWakeMode(Context context, int mode) {
        boolean wasHeld = false;
        if (this.wakeLock != null) {
            if (this.wakeLock.isHeld()) {
                wasHeld = true;
                this.wakeLock.release();
            }
            this.wakeLock = null;
        }
        if (context.getPackageManager().checkPermission("android.permission.WAKE_LOCK", context.getPackageName()) == 0) {
            this.wakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(536870912 | mode, ExoMediaPlayer.class.getName());
            this.wakeLock.setReferenceCounted(false);
        } else {
            Log.w(TAG, "Unable to acquire WAKE_LOCK due to missing manifest permission");
        }
        stayAwake(wasHeld);
    }

    protected int getExoPlayerTrackType(@NonNull RendererType type) {
        switch (type) {
            case AUDIO:
                return 1;
            case VIDEO:
                return 2;
            case CLOSED_CAPTION:
                return 3;
            case METADATA:
                return 4;
            default:
                return -1;
        }
    }

    protected void sendMessage(int renderType, int messageType, Object message) {
        sendMessage(renderType, messageType, message, false);
    }

    protected void sendMessage(int renderType, int messageType, Object message, boolean blocking) {
        if (!this.renderers.isEmpty()) {
            List<ExoPlayerMessage> messages = new ArrayList();
            for (Renderer renderer : this.renderers) {
                if (renderer.getTrackType() == renderType) {
                    messages.add(new ExoPlayerMessage(renderer, messageType, message));
                }
            }
            if (blocking) {
                this.player.blockingSendMessages((ExoPlayerMessage[]) messages.toArray(new ExoPlayerMessage[messages.size()]));
            } else {
                this.player.sendMessages((ExoPlayerMessage[]) messages.toArray(new ExoPlayerMessage[messages.size()]));
            }
        }
    }

    protected void stayAwake(boolean awake) {
        if (this.wakeLock != null) {
            if (awake && !this.wakeLock.isHeld()) {
                this.wakeLock.acquire(1000);
            } else if (!awake && this.wakeLock.isHeld()) {
                this.wakeLock.release();
            }
        }
    }

    @Nullable
    protected DrmSessionManager<FrameworkMediaCrypto> generateDrmSessionManager() {
        if (VERSION.SDK_INT < 18) {
            return null;
        }
        UUID uuid = C.WIDEVINE_UUID;
        try {
            return new DefaultDrmSessionManager(uuid, FrameworkMediaDrm.newInstance(uuid), new DelegatedMediaDrmCallback(), null, this.mainHandler, this.capabilitiesListener);
        } catch (Exception e) {
            Log.d(TAG, "Unable to create a DrmSessionManager due to an exception", e);
            return null;
        }
    }

    private void reportPlayerState() {
        boolean playWhenReady = this.player.getPlayWhenReady();
        int playbackState = getPlaybackState();
        int newState = this.stateStore.getState(playWhenReady, playbackState);
        if (newState != this.stateStore.getMostRecentState()) {
            this.stateStore.setMostRecentState(playWhenReady, playbackState);
            if (newState == 3) {
                setBufferRepeaterStarted(true);
            } else if (newState == 1 || newState == 4) {
                setBufferRepeaterStarted(false);
            }
            boolean informSeekCompletion = (this.stateStore.matchesHistory(new int[]{100, 2, 3}, true) | this.stateStore.matchesHistory(new int[]{2, 100, 3}, true)) | this.stateStore.matchesHistory(new int[]{100, 3, 2, 3}, true);
            Iterator it = this.listeners.iterator();
            while (it.hasNext()) {
                ExoPlayerListener listener = (ExoPlayerListener) it.next();
                listener.onStateChanged(playWhenReady, playbackState);
                if (informSeekCompletion) {
                    listener.onSeekComplete();
                }
            }
        }
    }

    private void setBufferRepeaterStarted(boolean start) {
        if (!start || this.bufferUpdateListener == null) {
            this.bufferRepeater.stop();
        } else {
            this.bufferRepeater.start();
        }
    }
}
