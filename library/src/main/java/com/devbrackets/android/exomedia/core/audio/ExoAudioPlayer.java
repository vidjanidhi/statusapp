package com.devbrackets.android.exomedia.core.audio;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.FloatRange;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.devbrackets.android.exomedia.ExoMedia.RendererType;
import com.devbrackets.android.exomedia.core.ListenerMux;
import com.devbrackets.android.exomedia.core.api.AudioPlayerApi;
import com.devbrackets.android.exomedia.core.exoplayer.ExoMediaPlayer;
import com.devbrackets.android.exomedia.core.listener.MetadataListener;
import com.devbrackets.android.exomedia.listener.OnBufferUpdateListener;
import com.google.android.exoplayer2.drm.MediaDrmCallback;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import java.util.Map;

@TargetApi(16)
public class ExoAudioPlayer implements AudioPlayerApi {
    @NonNull
    protected final Context context;
    @NonNull
    protected final ExoMediaPlayer exoMediaPlayer;
    @NonNull
    protected InternalListeners internalListeners = new InternalListeners();
    protected ListenerMux listenerMux;
    protected boolean playRequested = false;

    protected class InternalListeners implements MetadataListener, OnBufferUpdateListener {
        protected InternalListeners() {
        }

        public void onMetadata(Metadata metadata) {
            ExoAudioPlayer.this.listenerMux.onMetadata(metadata);
        }

        public void onBufferingUpdate(@IntRange(from = 0, to = 100) int percent) {
            ExoAudioPlayer.this.listenerMux.onBufferingUpdate(percent);
        }
    }

    public ExoAudioPlayer(@NonNull Context context) {
        this.context = context;
        this.exoMediaPlayer = new ExoMediaPlayer(context);
        this.exoMediaPlayer.setMetadataListener(this.internalListeners);
        this.exoMediaPlayer.setBufferUpdateListener(this.internalListeners);
    }

    public void setDataSource(@Nullable Uri uri) {
        setDataSource(uri, null);
    }

    public void setDataSource(@Nullable Uri uri, @Nullable MediaSource mediaSource) {
        this.listenerMux.setNotifiedPrepared(false);
        this.exoMediaPlayer.seekTo(0);
        if (mediaSource != null) {
            this.exoMediaPlayer.setMediaSource(mediaSource);
            this.listenerMux.setNotifiedCompleted(false);
        } else if (uri != null) {
            this.exoMediaPlayer.setUri(uri);
            this.listenerMux.setNotifiedCompleted(false);
        } else {
            this.exoMediaPlayer.setMediaSource(null);
        }
    }

    public void setDrmCallback(@Nullable MediaDrmCallback drmCallback) {
        this.exoMediaPlayer.setDrmCallback(drmCallback);
    }

    public void prepareAsync() {
        this.exoMediaPlayer.prepare();
    }

    public void reset() {
    }

    public void setVolume(@FloatRange(from = 0.0d, to = 1.0d) float left, @FloatRange(from = 0.0d, to = 1.0d) float right) {
        this.exoMediaPlayer.setVolume((left + right) / 2.0f);
    }

    public void seekTo(@IntRange(from = 0) long milliseconds) {
        this.exoMediaPlayer.seekTo(milliseconds);
    }

    public boolean isPlaying() {
        return this.exoMediaPlayer.getPlayWhenReady();
    }

    public void start() {
        this.exoMediaPlayer.setPlayWhenReady(true);
        this.listenerMux.setNotifiedCompleted(false);
        this.playRequested = true;
    }

    public void pause() {
        this.exoMediaPlayer.setPlayWhenReady(false);
        this.playRequested = false;
    }

    public void stopPlayback() {
        this.exoMediaPlayer.stop();
        this.playRequested = false;
    }

    public boolean restart() {
        if (!this.exoMediaPlayer.restart()) {
            return false;
        }
        this.listenerMux.setNotifiedCompleted(false);
        this.listenerMux.setNotifiedPrepared(false);
        return true;
    }

    public long getDuration() {
        if (this.listenerMux.isPrepared()) {
            return this.exoMediaPlayer.getDuration();
        }
        return 0;
    }

    public long getCurrentPosition() {
        if (this.listenerMux.isPrepared()) {
            return this.exoMediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    public int getBufferedPercent() {
        return this.exoMediaPlayer.getBufferedPercentage();
    }

    public void release() {
        this.exoMediaPlayer.release();
    }

    public int getAudioSessionId() {
        return this.exoMediaPlayer.getAudioSessionId();
    }

    public boolean setPlaybackSpeed(float speed) {
        return this.exoMediaPlayer.setPlaybackSpeed(speed);
    }

    public void setAudioStreamType(int streamType) {
        this.exoMediaPlayer.setAudioStreamType(streamType);
    }

    public void setWakeMode(Context context, int mode) {
        this.exoMediaPlayer.setWakeMode(context, mode);
    }

    public boolean trackSelectionAvailable() {
        return true;
    }

    public void setTrack(RendererType type, int trackIndex) {
        this.exoMediaPlayer.setSelectedTrack(type, trackIndex);
    }

    @Nullable
    public Map<RendererType, TrackGroupArray> getAvailableTracks() {
        return this.exoMediaPlayer.getAvailableTracks();
    }

    public void setListenerMux(ListenerMux listenerMux) {
        if (this.listenerMux != null) {
            this.exoMediaPlayer.removeListener(this.listenerMux);
        }
        this.listenerMux = listenerMux;
        this.exoMediaPlayer.addListener(listenerMux);
    }

    public void onMediaPrepared() {
    }
}
