package com.devbrackets.android.exomedia.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.FloatRange;
import android.support.annotation.IntRange;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.devbrackets.android.exomedia.ExoMedia.RendererType;
import com.devbrackets.android.exomedia.R;
import com.devbrackets.android.exomedia.core.ListenerMux;
import com.devbrackets.android.exomedia.core.ListenerMux.Notifier;
import com.devbrackets.android.exomedia.core.api.VideoViewApi;
import com.devbrackets.android.exomedia.core.exoplayer.ExoMediaPlayer;
import com.devbrackets.android.exomedia.core.listener.MetadataListener;
import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import com.devbrackets.android.exomedia.listener.OnBufferUpdateListener;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.listener.OnSeekCompletionListener;
import com.devbrackets.android.exomedia.listener.OnVideoSizeChangedListener;
import com.devbrackets.android.exomedia.util.DeviceUtil;
import com.devbrackets.android.exomedia.util.StopWatch;
import com.google.android.exoplayer2.drm.MediaDrmCallback;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.util.MimeTypes;

import java.util.Map;

public class VideoView extends RelativeLayout {
    private static final String TAG = VideoView.class.getSimpleName();
    @NonNull
    protected AudioFocusHelper audioFocusHelper = new AudioFocusHelper();
    protected AudioManager audioManager;
    protected DeviceUtil deviceUtil = new DeviceUtil();
    protected boolean handleAudioFocus = true;
    protected ListenerMux listenerMux;
    protected MuxNotifier muxNotifier = new MuxNotifier();
    protected long overriddenDuration = -1;
    protected StopWatch overriddenPositionStopWatch = new StopWatch();
    protected boolean overridePosition = false;
    protected long positionOffset = 0;
    protected ImageView previewImageView;
    protected boolean releaseOnDetachFromWindow = true;
    @Nullable
    protected VideoControls videoControls;
    protected Uri videoUri;
    protected VideoViewApi videoViewImpl;

    protected class AttributeContainer {
        public int apiImplLegacyResourceId = R.layout.exomedia_default_native_texture_video_view;
        public int apiImplResourceId = R.layout.exomedia_default_exo_texture_video_view;
        @Nullable
        public Boolean measureBasedOnAspectRatio;
        @Nullable
        public ScaleType scaleType;
        public boolean useDefaultControls = false;
        public boolean useTextureViewBacking = false;

        public AttributeContainer(@NonNull Context context, @Nullable AttributeSet attrs) {
            if (attrs != null) {
                TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.VideoView);
                if (typedArray != null) {
                    this.useDefaultControls = typedArray.getBoolean(R.styleable.VideoView_useDefaultControls, this.useDefaultControls);
                    this.useTextureViewBacking = typedArray.getBoolean(R.styleable.VideoView_useTextureViewBacking, this.useTextureViewBacking);
                    if (typedArray.hasValue(R.styleable.VideoView_videoScale)) {
                        this.scaleType = ScaleType.fromOrdinal(typedArray.getInt(R.styleable.VideoView_videoScale, -1));
                    }
                    if (typedArray.hasValue(R.styleable.VideoView_measureBasedOnAspectRatio)) {
                        this.measureBasedOnAspectRatio = Boolean.valueOf(typedArray.getBoolean(R.styleable.VideoView_measureBasedOnAspectRatio, false));
                    }
                    this.apiImplResourceId = this.useTextureViewBacking ? R.layout.exomedia_default_exo_texture_video_view : R.layout.exomedia_default_exo_surface_video_view;
                    this.apiImplLegacyResourceId = this.useTextureViewBacking ? R.layout.exomedia_default_native_texture_video_view : R.layout.exomedia_default_native_surface_video_view;
                    this.apiImplResourceId = typedArray.getResourceId(R.styleable.VideoView_videoViewApiImpl, this.apiImplResourceId);
                    this.apiImplLegacyResourceId = typedArray.getResourceId(R.styleable.VideoView_videoViewApiImplLegacy, this.apiImplLegacyResourceId);
                    typedArray.recycle();
                }
            }
        }
    }

    protected class AudioFocusHelper implements OnAudioFocusChangeListener {
        protected int currentFocus = 0;
        protected boolean pausedForLoss = false;
        protected boolean startRequested = false;

        protected AudioFocusHelper() {
        }

        public void onAudioFocusChange(int focusChange) {
            if (VideoView.this.handleAudioFocus && this.currentFocus != focusChange) {
                this.currentFocus = focusChange;
                switch (focusChange) {
                    case -3:
                    case -2:
                        if (VideoView.this.isPlaying()) {
                            this.pausedForLoss = true;
                            VideoView.this.pause();
                            return;
                        }
                        return;
                    case -1:
                        if (VideoView.this.isPlaying()) {
                            this.pausedForLoss = true;
                            VideoView.this.pause();
                            return;
                        }
                        return;
                    case 1:
                    case 2:
                        if (this.startRequested || this.pausedForLoss) {
                            VideoView.this.start();
                            this.startRequested = false;
                            this.pausedForLoss = false;
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public boolean requestFocus() {
            if (!VideoView.this.handleAudioFocus || this.currentFocus == 1) {
                return true;
            }
            if (VideoView.this.audioManager == null) {
                return false;
            }
            if (1 == VideoView.this.audioManager.requestAudioFocus(this, 3, 1)) {
                this.currentFocus = 1;
                return true;
            }
            this.startRequested = true;
            return false;
        }

        public boolean abandonFocus() {
            if (!VideoView.this.handleAudioFocus) {
                return true;
            }
            if (VideoView.this.audioManager == null) {
                return false;
            }
            this.startRequested = false;
            if (1 != VideoView.this.audioManager.abandonAudioFocus(this)) {
                return false;
            }
            return true;
        }
    }

    protected class MuxNotifier extends Notifier {
        @Nullable
        public OnVideoSizeChangedListener videoSizeChangedListener;

        protected MuxNotifier() {
        }

        public boolean shouldNotifyCompletion(long endLeeway) {
            return VideoView.this.getCurrentPosition() + endLeeway >= VideoView.this.getDuration();
        }

        public void onExoPlayerError(ExoMediaPlayer exoMediaPlayer, Exception e) {
            VideoView.this.stopPlayback();
            if (exoMediaPlayer != null) {
                exoMediaPlayer.forcePrepare();
            }
        }

        public void onMediaPlaybackEnded() {
            VideoView.this.setKeepScreenOn(false);
            VideoView.this.onPlaybackEnded();
        }

        public void onSeekComplete() {
            if (VideoView.this.videoControls != null) {
                VideoView.this.videoControls.finishLoading();
            }
        }

        public void onVideoSizeChanged(int width, int height, int unAppliedRotationDegrees, float pixelWidthHeightRatio) {
            VideoView.this.videoViewImpl.setVideoRotation(unAppliedRotationDegrees, false);
            VideoView.this.videoViewImpl.onVideoSizeChanged(width, height);
            if (this.videoSizeChangedListener != null) {
                this.videoSizeChangedListener.onVideoSizeChanged(width, height);
            }
        }

        public void onPrepared() {
            if (VideoView.this.videoControls != null) {
                VideoView.this.videoControls.setDuration(VideoView.this.getDuration());
                VideoView.this.videoControls.finishLoading();
            }
        }

        public void onPreviewImageStateChanged(boolean toVisible) {
            if (VideoView.this.previewImageView != null) {
                VideoView.this.previewImageView.setVisibility(toVisible ? 0 : 8);
            }
        }
    }

    protected class TouchListener extends SimpleOnGestureListener implements OnTouchListener {
        protected GestureDetector gestureDetector;

        public TouchListener(Context context) {
            this.gestureDetector = new GestureDetector(context, this);
        }

        public boolean onTouch(View view, MotionEvent event) {
            this.gestureDetector.onTouchEvent(event);
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent event) {
            if (VideoView.this.videoControls == null || !VideoView.this.videoControls.isVisible()) {
                VideoView.this.showControls();
            } else {
                VideoView.this.videoControls.hide();
            }
            return true;
        }
    }

    public VideoView(Context context) {
        super(context);
        setup(context, null);
    }

    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 1 || newConfig.orientation != 2) {
        }
    }

    public VideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context, attrs);
    }

    @TargetApi(11)
    public VideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup(context, attrs);
    }

    @TargetApi(21)
    public VideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setup(context, attrs);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode() && this.releaseOnDetachFromWindow) {
            release();
        }
    }

    public void setOnTouchListener(OnTouchListener listener) {
        this.videoViewImpl.setOnTouchListener(listener);
        super.setOnTouchListener(listener);
    }

    public void setReleaseOnDetachFromWindow(boolean releaseOnDetach) {
        this.releaseOnDetachFromWindow = releaseOnDetach;
    }

    public void release() {
        this.videoControls = null;
        stopPlayback();
        this.overriddenPositionStopWatch.stop();
        this.videoViewImpl.release();
    }

    public void setPreviewImage(@Nullable Drawable drawable) {
        if (this.previewImageView != null) {
            this.previewImageView.setImageDrawable(drawable);
        }
    }

    public void setPreviewImage(@DrawableRes int resourceId) {
        if (this.previewImageView != null) {
            this.previewImageView.setImageResource(resourceId);
        }
    }

    public void setPreviewImage(@Nullable Bitmap bitmap) {
        if (this.previewImageView != null) {
            this.previewImageView.setImageBitmap(bitmap);
        }
    }

    public void setPreviewImage(@Nullable Uri uri) {
        if (this.previewImageView != null) {
            this.previewImageView.setImageURI(uri);
        }
    }

    public ImageView getPreviewImageView() {
        return this.previewImageView;
    }

    public void setControls(@Nullable VideoControls controls) {
        if (!(this.videoControls == null || this.videoControls == controls)) {
            removeView(this.videoControls);
        }
        if (controls != null) {
            this.videoControls = controls;
            controls.setVideoView(this);
            addView(controls);
        }
        TouchListener listener = new TouchListener(getContext());
        if (this.videoControls == null) {
            listener = null;
        }
        setOnTouchListener(listener);
    }

    public void showControls() {
        if (this.videoControls != null) {
            this.videoControls.show();
            if (isPlaying()) {
                this.videoControls.hideDelayed();
            }
        }
    }

    @Nullable
    public VideoControls getVideoControls() {
        return this.videoControls;
    }

    public void setVideoURI(@Nullable Uri uri) {
        this.videoUri = uri;
        this.videoViewImpl.setVideoUri(uri);
        if (this.videoControls != null) {
            this.videoControls.showLoading(true);
        }
    }

    public void setVideoURI(@Nullable Uri uri, @Nullable MediaSource mediaSource) {
        this.videoUri = uri;
        this.videoViewImpl.setVideoUri(uri, mediaSource);
        if (this.videoControls != null) {
            this.videoControls.showLoading(true);
        }
    }

    public void setVideoPath(String path) {
        setVideoURI(Uri.parse(path));
    }

    @Nullable
    public Uri getVideoUri() {
        return this.videoUri;
    }

    public void setDrmCallback(@Nullable MediaDrmCallback drmCallback) {
        this.videoViewImpl.setDrmCallback(drmCallback);
    }

    public boolean setVolume(@FloatRange(from = 0.0d, to = 1.0d) float volume) {
        return this.videoViewImpl.setVolume(volume);
    }

    public void setHandleAudioFocus(boolean handleAudioFocus) {
        this.audioFocusHelper.abandonFocus();
        this.handleAudioFocus = handleAudioFocus;
    }

    public void reset() {
        stopPlayback();
        setVideoURI(null);
    }

    public void seekTo(long milliSeconds) {
        if (this.videoControls != null) {
            this.videoControls.showLoading(false);
        }
        this.videoViewImpl.seekTo(milliSeconds);
    }

    public boolean isPlaying() {
        return this.videoViewImpl.isPlaying();
    }

    public void start() {
        if (this.audioFocusHelper.requestFocus()) {
            this.videoViewImpl.start();
            setKeepScreenOn(true);
            if (this.videoControls != null) {
                this.videoControls.updatePlaybackState(true);
            }
        }
    }

    public void pause() {
        this.audioFocusHelper.abandonFocus();
        this.videoViewImpl.pause();
        setKeepScreenOn(false);
        if (this.videoControls != null) {
            this.videoControls.updatePlaybackState(false);
        }
    }

    public void stopPlayback() {
        stopPlayback(true);
    }

    public boolean restart() {
        if (this.videoUri == null || !this.videoViewImpl.restart()) {
            return false;
        }
        if (this.videoControls != null) {
            this.videoControls.showLoading(true);
        }
        return true;
    }

    public void suspend() {
        this.audioFocusHelper.abandonFocus();
        this.videoViewImpl.suspend();
        setKeepScreenOn(false);
        if (this.videoControls != null) {
            this.videoControls.updatePlaybackState(false);
        }
    }

    public long getDuration() {
        if (this.overriddenDuration >= 0) {
            return this.overriddenDuration;
        }
        return this.videoViewImpl.getDuration();
    }

    public void overrideDuration(long duration) {
        this.overriddenDuration = duration;
    }

    public long getCurrentPosition() {
        if (this.overridePosition) {
            return this.positionOffset + ((long) this.overriddenPositionStopWatch.getTimeInt());
        }
        return this.positionOffset + this.videoViewImpl.getCurrentPosition();
    }

    public void setPositionOffset(long offset) {
        this.positionOffset = offset;
    }

    public void restartOverridePosition() {
        this.overriddenPositionStopWatch.reset();
    }

    public void overridePosition(boolean override) {
        if (override) {
            this.overriddenPositionStopWatch.start();
        } else {
            this.overriddenPositionStopWatch.stop();
        }
        this.overridePosition = override;
    }

    public int getBufferPercentage() {
        return this.videoViewImpl.getBufferedPercent();
    }

    public boolean setPlaybackSpeed(float speed) {
        return this.videoViewImpl.setPlaybackSpeed(speed);
    }

    public boolean trackSelectionAvailable() {
        return this.videoViewImpl.trackSelectionAvailable();
    }

    public void setTrack(RendererType trackType, int trackIndex) {
        this.videoViewImpl.setTrack(trackType, trackIndex);
    }

    @Nullable
    public Map<RendererType, TrackGroupArray> getAvailableTracks() {
        return this.videoViewImpl.getAvailableTracks();
    }

    public void setScaleType(@NonNull ScaleType scaleType) {
        this.videoViewImpl.setScaleType(scaleType);
    }

    public void setMeasureBasedOnAspectRatioEnabled(boolean measureBasedOnAspectRatioEnabled) {
        this.videoViewImpl.setMeasureBasedOnAspectRatioEnabled(measureBasedOnAspectRatioEnabled);
    }

    public void setVideoRotation(@IntRange(from = 0, to = 359) int rotation) {
        this.videoViewImpl.setVideoRotation(rotation, true);
    }

    public void setOnPreparedListener(@Nullable OnPreparedListener listener) {
        this.listenerMux.setOnPreparedListener(listener);
    }

    public void setOnCompletionListener(@Nullable OnCompletionListener listener) {
        this.listenerMux.setOnCompletionListener(listener);
    }

    public void setOnBufferUpdateListener(@Nullable OnBufferUpdateListener listener) {
        this.listenerMux.setOnBufferUpdateListener(listener);
    }

    public void setOnSeekCompletionListener(@Nullable OnSeekCompletionListener listener) {
        this.listenerMux.setOnSeekCompletionListener(listener);
    }

    public void setOnErrorListener(@Nullable OnErrorListener listener) {
        this.listenerMux.setOnErrorListener(listener);
    }

    public void setId3MetadataListener(@Nullable MetadataListener listener) {
        this.listenerMux.setMetadataListener(listener);
    }

    public void setOnVideoSizedChangedListener(@Nullable OnVideoSizeChangedListener listener) {
        this.muxNotifier.videoSizeChangedListener = listener;
    }

    protected void setup(Context context, @Nullable AttributeSet attrs) {
        if (!isInEditMode()) {
            this.audioManager = (AudioManager) context.getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
            AttributeContainer attributeContainer = new AttributeContainer(context, attrs);
            initView(context, attributeContainer);
            postInit(attributeContainer);
        }
    }

    protected void initView(Context context, @NonNull AttributeContainer attributeContainer) {
        inflateVideoView(context, attributeContainer);
        this.previewImageView = (ImageView) findViewById(R.id.exomedia_video_preview_image);
        this.videoViewImpl = (VideoViewApi) findViewById(R.id.exomedia_video_view);
        this.muxNotifier = new MuxNotifier();
        this.listenerMux = new ListenerMux(this.muxNotifier);
        this.videoViewImpl.setListenerMux(this.listenerMux);
    }

    protected void postInit(@NonNull AttributeContainer attributeContainer) {
        if (attributeContainer.useDefaultControls) {
            setControls(this.deviceUtil.isDeviceTV(getContext()) ? new VideoControlsLeanback(getContext()) : new VideoControlsMobile(getContext()));
        }
        if (attributeContainer.scaleType != null) {
            setScaleType(attributeContainer.scaleType);
        }
        if (attributeContainer.measureBasedOnAspectRatio != null) {
            setMeasureBasedOnAspectRatioEnabled(attributeContainer.measureBasedOnAspectRatio.booleanValue());
        }
    }

    protected void inflateVideoView(@NonNull Context context, @NonNull AttributeContainer attributeContainer) {
        View.inflate(context, R.layout.exomedia_video_view_layout, this);
        ViewStub videoViewStub = (ViewStub) findViewById(R.id.video_view_api_impl_stub);
        videoViewStub.setLayoutResource(getVideoViewApiImplementation(context, attributeContainer));
        videoViewStub.inflate();
    }

    @LayoutRes
    protected int getVideoViewApiImplementation(@NonNull Context context, @NonNull AttributeContainer attributeContainer) {
        return !this.deviceUtil.supportsExoPlayer(context) ? attributeContainer.apiImplLegacyResourceId : attributeContainer.apiImplResourceId;
    }

    protected void onPlaybackEnded() {
        stopPlayback(false);
    }

    protected void stopPlayback(boolean clearSurface) {
        this.audioFocusHelper.abandonFocus();
        this.videoViewImpl.stopPlayback(clearSurface);
        setKeepScreenOn(false);
        if (this.videoControls != null) {
            this.videoControls.updatePlaybackState(false);
        }
    }
}
